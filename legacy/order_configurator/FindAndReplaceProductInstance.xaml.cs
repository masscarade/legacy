﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace wpfTest
{
    /// <summary>
    /// Interaction logic for RenameRootNode.xaml
    /// </summary>
    public partial class FindAndReplaceProductInstanceDialog : Window
    {
        Product node;

        public FindAndReplaceProductInstanceDialog(Product product)
        {
            node = product;

            InitializeComponent();
           
        }

        public string findText
        {
            get { return txtFind.Text; }
            set { txtFind.Text = value; }
        }
        public string replaceText
        {
            get { return txtReplace.Text; }
            set { txtReplace.Text = value; }
        }

        private void btnDialogOk_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            node.element.properties.findAndReplace(findText, replaceText);
            DialogResult = true;
        }
    }
}
