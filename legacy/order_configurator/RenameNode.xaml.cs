﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace wpfTest
{
    /// <summary>
    /// Interaction logic for RenameRootNode.xaml
    /// </summary>
    public partial class RenameNode : Window
    {
        public RenameNode(string oldName)
        {
            InitializeComponent();

            txtAnswer.Text = oldName;

            Loaded += RenameNode_Loaded;
           
        }

        private void RenameNode_Loaded(object sender, RoutedEventArgs e)
        {
            txtAnswer.Focus();
        }

        public string responseText
        {
            get { return txtAnswer.Text; }
            set { txtAnswer.Text = value; }
        }

        private void btnDialogOk_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
