﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Microsoft.Win32;
using System.Xml;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace wpfTest
{
    //stupid comment

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public static string _currentOrder = "Order.xml";
        MainWindwoViewModel viewModel = new MainWindwoViewModel();

        public String currentOrder
        {
            get { return _currentOrder; }
            set
            {
                _currentOrder = value;
                OnPropertyChanged("currentOrder");
            }
        }

        public MainWindow()
        {
            InitializeComponent();

            viewModel.machine = new MachineViewModel(machine, this);
            viewModel.productCatalog = new ProductCatalogViewModel();
            viewModel.properties = new PropertiesViewModel(propertyPanel.Children);

            machine.ItemsSource = viewModel.machine.tree;
            machine.SelectedItemChanged += viewModel.machine.selectedItemChanged;

            viewModel.machine.SlotSelectedEvent += viewModel.productCatalog.updateAvailableProducts;
            viewModel.machine.ProductSelectedEvent += viewModel.properties.updateProperties;
            viewModel.productCatalog.ProductionTriggeredEvent += viewModel.machine.ProductionTriggered;

            DataContext = this;

            productCatalog.DataContext = viewModel.productCatalog;
            productCatalog.MouseDoubleClick += viewModel.productCatalog.triggerProduction;

            menuSave.Click += viewModel.machine.saveOrder;
            menuSaveAs.Click += viewModel.machine.saveOrderAs;
            menuLoad.Click += loadOrder;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Exit(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        public void loadOrder(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();

            if (fileDialog.ShowDialog() == true)
            {
                using (XmlReader reade = XmlReader.Create(fileDialog.FileName))
                {
                    //deregister events of old object             
                    machine.SelectedItemChanged -= viewModel.machine.selectedItemChanged;

                    viewModel.machine.SlotSelectedEvent -= viewModel.productCatalog.updateAvailableProducts;
                    viewModel.machine.ProductSelectedEvent -= viewModel.properties.updateProperties;
                    viewModel.productCatalog.ProductionTriggeredEvent -= viewModel.machine.ProductionTriggered;


                    productCatalog.DataContext = viewModel.productCatalog;
                    productCatalog.MouseDoubleClick -= viewModel.productCatalog.triggerProduction;

                    menuSave.Click -= viewModel.machine.saveOrder;
                    menuSaveAs.Click -= viewModel.machine.saveOrderAs;
                    menuLoad.Click -= loadOrder;

                    DataContractSerializer serializer = new DataContractSerializer(typeof(codegen.Order));
                    codegen.Order order = (codegen.Order)serializer.ReadObject(reade);                   

                    //register events and data context
                    viewModel.machine = new MachineViewModel(machine, this, order);                  

                    viewModel.productCatalog = new ProductCatalogViewModel();
                    viewModel.properties = new PropertiesViewModel(propertyPanel.Children);

                    //reinitialitze factory
                    viewModel.machine.factory.reinitialitze(order, viewModel.productCatalog.factory);

                    machine.ItemsSource = viewModel.machine.tree;
                    machine.SelectedItemChanged += viewModel.machine.selectedItemChanged;

                    viewModel.machine.SlotSelectedEvent += viewModel.productCatalog.updateAvailableProducts;
                    viewModel.machine.ProductSelectedEvent += viewModel.properties.updateProperties;
                    viewModel.productCatalog.ProductionTriggeredEvent += viewModel.machine.ProductionTriggered;

                    DataContext = this;

                    productCatalog.DataContext = viewModel.productCatalog;
                    productCatalog.MouseDoubleClick += viewModel.productCatalog.triggerProduction;

                    menuSave.Click += viewModel.machine.saveOrder;
                    menuSaveAs.Click += viewModel.machine.saveOrderAs;
                    menuLoad.Click += loadOrder;

                    currentOrder = fileDialog.FileName;
                }
            }
        }

        private void deleteProductInstance(object sender, RoutedEventArgs e)
        {
            if (machine.SelectedItem.GetType() == typeof(Product))
            {
                Product node = (Product)machine.SelectedItem;
                List<string> deleted = node.delete();

                viewModel.machine.tree.Remove(node);            

                MessageBox.Show(string.Format("deleted elements: {0}", string.Join(" ",deleted.ToArray())));                
            }            
        }

        private void machine_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            Type selector = machine.SelectedItem.GetType();

            if (selector == typeof(Product))
            {

                machine.ContextMenu = machine.Resources["ProductInstanceContext"] as System.Windows.Controls.ContextMenu;
            }
            else if (selector == typeof(Slot))
            {

                machine.ContextMenu = machine.Resources["SlotContext"] as System.Windows.Controls.ContextMenu;
            }
            else if (selector == typeof(RootNode))
            {

                machine.ContextMenu = machine.Resources["RootNodeContext"] as System.Windows.Controls.ContextMenu;
            }

        }

        private void renameRootNode(object sender, RoutedEventArgs e)
        {
            if (machine.SelectedItem.GetType() == typeof(RootNode))
            {
                RootNode node = (RootNode)machine.SelectedItem;
                RenameNode dialog = new RenameNode(node.name);

                if (dialog.ShowDialog() == true)
                {
                    string oldName = node.name;
                    node.changeName(dialog.responseText);

                    MessageBox.Show(string.Format("Changed name from {0} to {1}",oldName, dialog.responseText));

                }
            }
        }

        private void renameProductInstance(object sender, RoutedEventArgs e)
        {
            if (machine.SelectedItem.GetType() == typeof(Product))
            {
                Product node = (Product)machine.SelectedItem;
                RenameNode dialog = new RenameNode(node.name);

                if (dialog.ShowDialog() == true)
                {
                    string oldName = node.name;
                    node.changeName(dialog.responseText);

                    MessageBox.Show(string.Format("Changed name from {0} to {1}", oldName, dialog.responseText));
                }
            }
        }

        private void findAndReplaceProductInstance(object sender, RoutedEventArgs e)
        {
            if (machine.SelectedItem.GetType() == typeof(Product))
            {
                Product node = (Product)machine.SelectedItem;
                FindAndReplaceProductInstanceDialog dialog = new FindAndReplaceProductInstanceDialog(node);

                if (dialog.ShowDialog() == true)
                {

                    //MessageBox.Show(string.Format("Changed name from {0} to {1}", oldName, dialog.responseText));
                }
            }
        }

        private void slotDetails(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
