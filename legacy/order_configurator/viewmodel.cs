﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Xml;
using System.Runtime.Serialization;
using System.IO;
using Microsoft.Win32;

namespace wpfTest
{
    public class MainWindwoViewModel
    {
        public MachineViewModel machine;// = new MachineViewModel();
        public ProductCatalogViewModel productCatalog;// = new ProductCatalogViewModel();
        public PropertiesViewModel properties;// = new PropertiesViewModel();

        public MainWindwoViewModel()
        {
            XmlWriterSettings xmlSettings = new XmlWriterSettings()
            {
                Indent = true,
            };

            using (XmlWriter write = XmlWriter.Create("SettingsSample.xml", xmlSettings))
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(Settings));

                serializer.WriteObject(write, Settings.getSample());
            }

            if (File.Exists("Settings.xml") == false)
            {
                settings = Settings.getSample();
            }
            else
            {
                using (XmlReader reade = XmlReader.Create("Settings.xml"))
                {
                    DataContractSerializer serializer = new DataContractSerializer(typeof(Settings));

                    settings = (Settings)serializer.ReadObject(reade);
                }
            }
        }

        public static Settings settings { get; private set; }
    }

    public class MachineViewModel :INotifyPropertyChanged
    {
        public codegen.Order order { get; private set; }
        public codegen.ProductInstance.Factory factory;

        public MainWindow windowRef;

        public TreeView treeview { get; private set; }

        public TreeNode root;

        private ObservableCollection<TreeNode> _tree = new ObservableCollection<TreeNode>();
        public ObservableCollection<TreeNode> tree
        {
            get
            {
                return _tree;
            }
            set
            {
                _tree = value;
                onPropertyChanged(new PropertyChangedEventArgs("tree"));
            }
        }

        public delegate void SlotSelected(Slot element);
        public event SlotSelected SlotSelectedEvent;

        public delegate void ProductSelected(Product element);
        public event ProductSelected ProductSelectedEvent;
        public event PropertyChangedEventHandler PropertyChanged;

        public MachineViewModel(TreeView treeview, MainWindow window, codegen.Order order = null)
        {
            this.windowRef = window;
            this.order = order;

            if (this.order == null)
            {
                this.order = new codegen.Order();
            }

            factory = new codegen.ProductInstance.Factory();
            this.treeview = treeview;

            root = new RootNode(this.order);
            tree.Add(root);
        }

        public void saveOrder(object sender, RoutedEventArgs e)
        {
            XmlWriterSettings settings = new XmlWriterSettings()
            {
                Indent = true,
            };

            using (XmlWriter write = XmlWriter.Create(windowRef.currentOrder, settings))
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(codegen.Order));

                serializer.WriteObject(write, order);

                MessageBox.Show(string.Format("Saved order to {0}", windowRef.currentOrder));
            }
        }

        public void saveOrderAs(object sender, RoutedEventArgs e)
        {
            SaveFileDialog fileDialog = new SaveFileDialog()
            {
                Filter = "xml files (*.xml)|*.xml",
            };

            if (fileDialog.ShowDialog() == true)
            {
                XmlWriterSettings settings = new XmlWriterSettings()
                {
                    Indent = true,
                };

                using (XmlWriter write = XmlWriter.Create(fileDialog.FileName, settings))
                {
                    DataContractSerializer serializer = new DataContractSerializer(typeof(codegen.Order));

                    serializer.WriteObject(write, order);

                    windowRef.currentOrder = fileDialog.FileName;

                    MessageBox.Show(string.Format("Saved order to {0}", windowRef.currentOrder));
                }
            }
        }

        public void onPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, e);
        }

        public void ProductionTriggered(codegen.ProductDefinition element)
        {
            if (treeview.SelectedItem == null)
            {
                return;
            }
            if (treeview.SelectedItem.GetType() == typeof(Slot))
            {
                Slot item = (Slot)treeview.SelectedItem;

                if (item.element.productDefinitionValid(element))
                {
                    if (item.element.addAllowed == true)
                    {
                        codegen.ProductInstance productInstance = factory.produce(element);
                        item.element.Add(productInstance);
                        item.children.Add(new Product(item, productInstance));

                        order.orderDate = DateTime.Now;
                        root.onPropertyChanged(new PropertyChangedEventArgs("description"));
                        item.onPropertyChanged(new PropertyChangedEventArgs("name"));
                        onPropertyChanged(new PropertyChangedEventArgs("children"));
                    }
                    else
                    {
                        //Popup or something
                    }
                }                
            }
            else if (treeview.SelectedItem.GetType() == typeof(RootNode))
            {
                RootNode item = (RootNode)treeview.SelectedItem;

                codegen.Condition.ContainsTag condition = new codegen.Condition.ContainsTag(new codegen.Tag("root"));

                //allow infinite root elements
                if (condition.fulfillsCondition(element)) //&& item.children.Count < 1)
                {
                    codegen.ProductInstance productInstance = factory.produce(element);
                    item.element.products.Add(productInstance);
                    item.children.Add(new Product(item, productInstance));

                    order.orderDate = DateTime.Now;
                    item.onPropertyChanged(new PropertyChangedEventArgs("name"));
                    onPropertyChanged(new PropertyChangedEventArgs("tree"));
                }
                else
                {
                    //Popup or something
                }
            }
        }

        public void selectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (e.NewValue != null)
            {
                if (e.NewValue.GetType() == typeof(Slot))
                {
                    SlotSelectedEvent((Slot)e.NewValue);
                }
                else if (e.NewValue.GetType() == typeof(Product))
                {
                    ProductSelectedEvent((Product)e.NewValue);
                }

                else if (e.NewValue.GetType() == typeof(RootNode))
                {
                    codegen.SlotDefinition slotDefinition = new codegen.SlotDefinition(1);
                    slotDefinition.conditions.Add(new codegen.Condition.ContainsTag(new codegen.Tag("root")));                  
                    Slot slot = new Slot(null, new codegen.SlotElement(slotDefinition));

                    SlotSelectedEvent(slot);
                }
            }     
        }
    }

    public abstract class TreeNode : INotifyPropertyChanged
    {
        protected TreeNode parent;

        public ObservableCollection<TreeNode> children { get; private set; } = new ObservableCollection<TreeNode>();

        public abstract string name
        {
            get;
        }

        private bool _isExpanded;
        public bool isExpanded
        {
            get { return _isExpanded; }
            set
            {
                if (value != _isExpanded)
                {
                    _isExpanded = value;
                    this.onPropertyChanged(new PropertyChangedEventArgs("isExpanded"));
                }

                // Expand all the way up to the root.
                if (_isExpanded && parent != null)
                {
                    parent.isExpanded = true;
                }
            }
        }

        public abstract string description
        {
            get;
        }

        public TreeNode(TreeNode parent)
        {
            this.parent = parent;
            isExpanded = true;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void onPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, e);
        }

        internal abstract List<string> delete();
        internal abstract void removeChild(Product product);
    }

    public class Product : TreeNode
    {
        public codegen.ProductInstance element { get; private set; }

        public override string name
        {
            get
            {
                return element.id.ToString();
            }   
        }

        public override string description
        {
            get
            {
               return element.definition.id.ToString();
            }     
        }

        public Product(TreeNode parent, codegen.ProductInstance element) :base(parent)
        {
            this.element = element;

            if (element.slots != null)
            {
                int i = 0;
                foreach (codegen.SlotElement slot in element.slots)
                {
                    Slot elslot = new Slot(this, slot, element.definition.rules.slots[i++]);
                    children.Add(elslot);
                }

                foreach (codegen.Property property in element.properties)
                {
                    property.initUiElements();
                }
            }
        }

        internal void changeName(string responseText)
        {
            element.rename(responseText);
            onPropertyChanged(new PropertyChangedEventArgs("name"));
        }

        override internal List<string> delete()
        {
            List<string> result = new List<string>();

            TreeNode child;
            while (children.Count > 0)
            {
                child = children.First();
                result.AddRange(child.delete());
            }
            element.delete();

            //parent.children.Remove(this);
            parent.removeChild(this);

            result.Add(element.id.ToString());

            return result;
        }

        internal override void removeChild(Product product)
        {
            children.Remove(product);
        }
    }

    public class RootNode : TreeNode, INotifyPropertyChanged
    {
        public codegen.Order element
        {
            get
            {
                return _element;
            }
            private set
            {
                _element = value;
                onPropertyChanged(new PropertyChangedEventArgs("name"));
                onPropertyChanged(new PropertyChangedEventArgs("description"));
            }
        }

        private codegen.Order _element;

        public override string name
        {
            get
            {
                return element.id.ToString();
            }          
        }

        public void changeName(string name)
        {
            element.setId(name);
            onPropertyChanged(new PropertyChangedEventArgs("name"));
        }

        internal override List<string> delete()
        {
            throw new NotImplementedException();
        }

        internal override void removeChild(Product product)
        {
            element.remove(product.element.id);
            children.Remove(product);
        }

        public override string description
        {
            get
            {
                return element.orderDate.ToString();
            }
        }

        public RootNode(codegen.Order element) : base(null)
        {
            this.element = element;

            foreach (KeyValuePair<codegen.ProductInstance.ID, codegen.ProductInstance> pair in element.products)
            {
                children.Add(new Product(this, pair.Value));
            }

        }
    }

    public class Slot : TreeNode
    {
        public codegen.SlotElement element { get; private set; }

        public override string name
        {
            get
            {
                return "slot";
            }
        }

        public override string description
        {
            get
            {
                return element.definition.ToString(); ;
            }
        }

        public Slot(TreeNode parent, codegen.SlotElement element, codegen.SlotDefinition definition = null) : base(parent)
        {
            this.element = element;

            if (definition != null)
            {
                this.element.definition = definition;
            }

            foreach (codegen.ProductInstance product in element)
            {
                children.Add(new Product(this, product));
            }
        }

        internal override List<string> delete()
        {
            List<string> result = new List<string>();

            TreeNode child;
            while (children.Count > 0)
            {
                child = children.First();
                result.AddRange(child.delete());
            }

            parent.children.Remove(this);

            return result;
        }

        internal override void removeChild(Product product)
        {
            children.Remove(product);
        }
    }

    public class ProductCatalogViewModel : INotifyPropertyChanged
    {
        public codegen.ProductDefinition.Factory factory { get; private set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public delegate void ProductionTriggered(codegen.ProductDefinition element);
        public event ProductionTriggered ProductionTriggeredEvent;

        private ObservableCollection<codegen.ProductDefinition> _availableProducts;

        public ObservableCollection<codegen.ProductDefinition> availableProducts
        {
            get
            {
                return _availableProducts;
            }
            private set
            {
                _availableProducts = value;
                onPropertyChanged(new PropertyChangedEventArgs("availableProducts"));
            }
        }

        public void triggerProduction(object sender, MouseButtonEventArgs e)
        {
            if (sender.GetType() == typeof(ListView))
            {
                ListView source = (ListView)sender;

                if (source.SelectedItem != null)
                {
                    if (source.SelectedItem.GetType() == typeof(codegen.ProductDefinition))
                    {
                        ProductionTriggeredEvent((codegen.ProductDefinition)source.SelectedItem);
                    }
                }
            }
        }

        public void updateAvailableProducts(Slot element)
        {
            availableProducts = new ObservableCollection<codegen.ProductDefinition>(factory.produced[element.element.definition.conditions].Values);
        }       
        public ProductCatalogViewModel()
        {
            factory = new codegen.ProductDefinition.Factory(new System.IO.DirectoryInfo(MainWindwoViewModel.settings.productsPath));
                     
            availableProducts = new ObservableCollection<codegen.ProductDefinition>(factory.produced.Values);
        }

        public void onPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, e);
        }
    }

    public class PropertiesViewModel
    {
        private List<codegen.ProductInstance.ID> registered = new List<codegen.ProductInstance.ID>();
        public codegen.ProductInstance selectedItem { get; private set; }

        public UIElementCollection elements { get; private set; }

        public PropertiesViewModel(UIElementCollection elements)
        {
            this.elements = elements;
        }
        public void updateProperties(Product element)
        {            

            selectedItem = element.element;           

            if (registered.Contains(selectedItem.id) == false)
            {
                foreach (codegen.Property property in selectedItem.properties)
                {
                    property.onUiElementsChange += Property_onUiElementsChange;
                    
                }

                registered.Add(selectedItem.id);
            }

            elements.Clear();
            //some comment
            foreach (codegen.Property property in selectedItem.properties)
            {               
                foreach (UIElement uiElement in property.uiElements)
                {
                    elements.Add(uiElement);
                }
            }
        }

        private void Property_onUiElementsChange()
        {
            elements.Clear();

            foreach (codegen.Property property in selectedItem.properties)
            {
                foreach (UIElement uiElement in property.uiElements)
                {
                    elements.Add(uiElement);
                }
            }
        }
    }
}
