﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

namespace codegen
{

    [DataContract]
    public class ProductInstance : IEquatable<ProductInstance>
    {
        public Factory producer { get; private set; }

        /// <summary>
        /// is used to identify the product instance
        /// </summary>
        [DataMember]
        public ID id { get; private set; }

        [DataMember]
        public PropertyCollection properties { get; private set; }

        [DataMember]
        public List<SlotElement> slots { get; private set; }

        [DataMember]
        public ProductDefinition definition { get; private set; }

        public List<ProductInstance> getChildren()
        {
            List<ProductInstance> children = new List<ProductInstance>();

            if (this.slots != null || slots.Count == 0)
            {
                return children;
            }

            foreach (SlotElement slot in this.slots)
            {
                foreach (ProductInstance inst in slot)
                {
                    children.Add(inst);
                    children.AddRange(inst.getChildren());
                }
            }

            return children;
        }

        public override string ToString()
        {
            return id.ToString();
        }

        private ProductInstance()
        {

        }

        public bool Equals(ProductInstance other)
        {
            return id.Equals(other.id);
        }

        public class Factory
        {
            public Dictionary<ProductDefinition.ID, int> producedProducts { get; private set; } = new Dictionary<ProductDefinition.ID, int>();
            public Dictionary<Feature.Type, int> producedTypes { get; private set; }  = new Dictionary<Feature.Type, int>();

            public ProductInstanceDictionary produced { get; private set; } = new ProductInstanceDictionary();

            public void reinitialitze(Order order, ProductDefinition.Factory defFactory)
            {
                //create new empty
                produced = new ProductInstanceDictionary();
                producedProducts = new Dictionary<ProductDefinition.ID, int>();
                producedTypes = new Dictionary<Feature.Type, int>();

                IEnumerator<ProductInstance> enumerable = order.GetEnumerator();

                while (enumerable.MoveNext())
                {
                    ProductInstance curr = enumerable.Current;
                    ProductDefinition def = defFactory.produced[curr.definition.id];

                    curr.definition = def;

                    enumerable.Current.producer = this;
                    incrementDictionary<ProductDefinition.ID>(producedProducts, curr.definition.id);
                    incrementDictionary<Feature.Type>(producedTypes, curr.definition.feature.type);

                    produced.Add(curr);
                }
            }

            private ID gernerateID(ProductDefinition definition)
            {
                int i = 1;
                ProductInstance.ID id;
                do
                {
                    id = new ID(String.Format("{0}{1}", definition.feature.type, i++), definition.id);
                }
                while (produced.ContainsKey(id) == true);


                return id;
            }
            public ProductInstance produce(ProductDefinition definition)
            {

                incrementDictionary<ProductDefinition.ID>(producedProducts, definition.id);
                incrementDictionary<Feature.Type>(producedTypes, definition.feature.type);

                ProductInstance inst = new ProductInstance()
                {
                    producer = this,
                    id = gernerateID(definition),
                    definition = definition,
                    properties = definition.properties.deepClone(),
                    slots = new List<SlotElement>(),
                };

                if (definition.rules != null)
                {
                    if (definition.rules.slots != null)
                    {
                        foreach (SlotDefinition def in definition.rules.slots)
                        {
                            inst.slots.Add(new SlotElement(def));
                        }
                    }
                }

                produced.Add(inst);            

                return inst;
            }

            private static void incrementDictionary<T>(Dictionary<T, int> dictionary, T element)
            {
                if (dictionary.ContainsKey(element) == false)
                {
                    dictionary.Add(element, 0);                    
                }               

                dictionary[element]++;
            }
            private static void decrementDictionary<T>(Dictionary<T, int> dictionary, T element)
            {
                if (dictionary.ContainsKey(element) == false)
                {
                    throw new Exception("decrementing counter for non existing entry -> check logic!");
                    //dictionary.Add(element, 0);
                }
                if (dictionary[element] <= 0)
                {
                    throw new Exception(
                        string.Format("decrementing counter which is already {0} -> check logic!", dictionary[element]));
                }

                dictionary[element]--;
            }

            internal void remove(ID id)
            {
                //for nicer code
                ProductInstance inst = produced[id];

                //make sure counting indecies are right
                decrementDictionary<ProductDefinition.ID>(producedProducts, inst.definition.id);
                decrementDictionary<Feature.Type>(producedTypes, inst.definition.feature.type);

                //remove id from dictionary
                produced.Remove(id);              
            }

            internal void add(ProductInstance productInstance)
            {
                produced.Add(productInstance);
            }
        }

        [DataContract]
        public class ID : IEquatable<ID>
        {
            [DataMember]
            public string name { get; set; }
            public ProductDefinition.ID productSource { get; private set; }

            public ID(string name, ProductDefinition.ID productSource)
            {
                this.name = name;
                this.productSource = productSource;
            }

            public override string ToString()
            {
                return name;
            }

            public override int GetHashCode()
            {
                return name.GetHashCode();
            }

            public bool Equals(ID other)
            {
                return this.name.Equals(other.name);
            }
        }

        public void rename(string responseText)
        {            
            producer.remove(this.id);

            this.id.name = responseText;

            producer.add(this);

            //throw new NotImplementedException();
        }

        public void delete()
        {
            //remove first all chil elements from the producer
            foreach (ProductInstance inst in this.getChildren())
            {
                producer.remove(inst.id);
            }
            producer.remove(this.id);
        }
    }

    [DataContract]
    public class ProductDefinition : IEquatable<ProductDefinition>
    {
        /// <summary>
        /// is used to identify the product definition instance
        /// </summary>
        [DataMember]
        public ID id { get; private set; }

        public Feature feature { get; private set; }

        public PropertyCollection properties { get; private set; }

        [DataMember]
        public Rules rules { get; private set; }

        public Factory producer { get; private set; }

        private ProductDefinition()
        {
        }

        public override string ToString()
        {
            return String.Format("ID {0}", id);
        }

        public bool Equals(ProductDefinition other)
        {
            //the id is the unique idintiefier for a product
            return this.id.Equals(other.id);
        }

        /// <summary>
        /// returns true if the directory is a valid product definition.
        /// a directory is considered a product definition, when it contains a valid Feature.xml
        /// currently it is only checked, if the file is existing, not if it is valid
        /// </summary>
        /// <param name="dir">directory that should be checked</param>
        /// <returns></returns>
        public static bool isProductDefinition(DirectoryInfo dir)
        {
            return dir.GetFiles("Definition.xml", SearchOption.TopDirectoryOnly).Length == 1;
        }

        public class Factory
        {
            /// <summary>
            /// contains all produced products
            /// </summary>
            public ProductDefinitionDictionary produced { get; private set; } = new ProductDefinitionDictionary();

            private HashSet<string> producedNames = new HashSet<string>();

            /// <summary>
            /// counter for produced samples; used to generate samle IDs
            /// </summary>
            private static int cSamples = 0;

            public DirectoryInfo source { get; private set; }

            public ProductDefinition this[ID id]
            {
                get
                {
                    return produced[id];
                }
            }

            public Factory(DirectoryInfo source)
            {
                this.source = source;

                foreach (DirectoryInfo dir in source.GetDirectories())
                {
                    if (ProductDefinition.isProductDefinition(dir))
                    {
                        produce(new ID(dir.Name, dir));
                    }
                }
            }

            /// <summary>
            /// creates a sample instance of Product, containing sample values for all properties
            /// </summary>
            /// <returns></returns>
            public static ProductDefinition getSample()
            {
                ProductDefinition inst = new ProductDefinition()
                {
                    id = new ID(String.Format("SampleProduct{0}", ++cSamples), new DirectoryInfo(String.Format("sample_path_{0}", cSamples))),
                    feature = Feature.Factory.getSample(),
                    properties = PropertyCollection.getSample(),
                };

                return inst;
            }

            public ProductDefinition produce(ID item)
            {

                if (produced.ContainsKey(item) == true)
                {
                    return produced[item];
                }

                ProductDefinition inst = new ProductDefinition()
                {
                    id = item,
                    producer = this,
                };

                //read properties if existing
                FileInfo[] propertiesPath = item.productSource.GetFiles("Properties.xml", SearchOption.TopDirectoryOnly);

                if (propertiesPath.Length == 1)
                {
                    if (propertiesPath[0].Exists)
                    {
                        using (XmlReader reader = XmlReader.Create(propertiesPath[0].FullName))
                        {
                            DataContractSerializer serializer = new DataContractSerializer(typeof(PropertyCollection));
                            inst.properties = (PropertyCollection)serializer.ReadObject(reader);
                        }
                    }
                }

                //read rules if existing
                FileInfo[] rulesPath = item.productSource.GetFiles("Rules.xml", SearchOption.TopDirectoryOnly);

                if (rulesPath.Length == 1)
                {
                    if (rulesPath[0].Exists)
                    {
                        using (XmlReader reader = XmlReader.Create(rulesPath[0].FullName))
                        {
                            DataContractSerializer serializer = new DataContractSerializer(typeof(Rules));
                            inst.rules = (Rules)serializer.ReadObject(reader);
                        }
                    }
                }

                //read feature; feature mustexist
                FileInfo[] featurePath = item.productSource.GetFiles("Definition.xml", SearchOption.TopDirectoryOnly);

                if (featurePath.Length == 1)
                {
                    if (featurePath[0].Exists)
                    {
                        using (XmlReader reader = XmlReader.Create(featurePath[0].FullName))
                        {
                            DataContractSerializer serializer = new DataContractSerializer(typeof(Feature));
                            inst.feature = (Feature)serializer.ReadObject(reader);
                        }
                    }
                }
                else
                {
                    throw new FeatureInvalidException(item);
                }

                //products must be unique
                produced.Add(inst);

                return inst;
            }

            public class FeatureInvalidException : Exception
            {
                private ID id;

                public FeatureInvalidException(ID id) : base()
                {
                    this.id = id;
                }

                public override string Message
                {
                    get
                    {
                        return String.Format("Product definition with ID <<{0}>> contains no or multible Feature.xml definitions", id);
                    }
                }
            }      
        }

        [DataContract]
        public class ID : IEquatable<ID>
        {
            [DataMember]
            public string name { get; private set; }
            public DirectoryInfo productSource { get; private set; }

            public ID(string name, DirectoryInfo productSource)
            {
                this.name = name;
                this.productSource = productSource;
            }

            public override string ToString()
            {
                return name;
            }

            public override int GetHashCode()
            {
                return name.GetHashCode();
            }

            public bool Equals(ID other)
            {
                return this.name.Equals(other.name);
            }
        }
    }
    public class ProductDefinitionDictionary : Dictionary<ProductDefinition.ID, ProductDefinition>
    {
        public void Add(ProductDefinition value)
        {
            this.Add(value.id, value);
        }

        public void Add(ProductDefinitionDictionary value)
        {
            foreach (KeyValuePair<ProductDefinition.ID, ProductDefinition> pair in this)
            {
                this.Add(pair.Value);
            }
        }

        public static ProductDefinitionDictionary getSample()
        {
            ProductDefinitionDictionary sample = new ProductDefinitionDictionary();
            sample.Add(ProductDefinition.Factory.getSample());
            sample.Add(ProductDefinition.Factory.getSample());

            return sample;
        }

        public ProductDefinitionDictionary this[ICollection<Tag> key]
        {
            get
            {
                ProductDefinitionDictionary items = new ProductDefinitionDictionary();

                foreach (Tag tag in key)
                {
                    items.Add(this[tag]);
                }

                return items;
            }   
        }

        public ProductDefinitionDictionary this[Condition condition]
        {
            get
            {
                ProductDefinitionDictionary items = new ProductDefinitionDictionary();

                foreach (KeyValuePair<ProductDefinition.ID, ProductDefinition> pair in this)
                {
                    if (condition.fulfillsCondition(pair.Value))
                    {
                        items.Add(pair.Value);
                    }
                }

                return items;
            }
        }

        public ProductDefinitionDictionary this[ICollection<Condition> conditions]
        {
            get
            {
                ProductDefinitionDictionary items = this;

                foreach (Condition condition in conditions)
                {
                    items = items[condition];
                }

                return items;
            }
        }

        public ProductDefinitionDictionary this[Tag key]
        {
            get
            {
                ProductDefinitionDictionary items = new ProductDefinitionDictionary();

                foreach (KeyValuePair<ProductDefinition.ID, ProductDefinition> pair in this)
                {
                    if (pair.Value.feature.tags.Contains(key))
                    {
                        items.Add(pair.Value);
                    }
                }

                return items;
            }
        }
    }

    [CollectionDataContract]
    public class ProductInstanceDictionary : Dictionary<ProductInstance.ID, ProductInstance>
    {
        public void Add(ProductInstance value)
        {
            this.Add(value.id, value);
        }

        public void Add(ProductInstanceDictionary value)
        {
            foreach (KeyValuePair<ProductInstance.ID, ProductInstance> pair in this)
            {
                this.Add(pair.Value);
            }
        }

        public static ProductInstanceDictionary getSample()
        {
            ProductInstanceDictionary inst = new ProductInstanceDictionary();

            ProductInstance.Factory factory = new ProductInstance.Factory();

            inst.Add(factory.produce(ProductDefinition.Factory.getSample()));
            inst.Add(factory.produce(ProductDefinition.Factory.getSample()));
            inst.Add(factory.produce(ProductDefinition.Factory.getSample()));

            return inst;
        }

        /// <summary>
        /// get a collection containing all product instances with a one or more tags of the collection
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public ProductInstanceDictionary this[ICollection<Tag> key]
        {
            get
            {
                ProductInstanceDictionary items = new ProductInstanceDictionary();

                foreach (Tag tag in key)
                {
                    items.Add(this[tag]);
                }

                return items;
            }
        }

        /// <summary>
        /// get a collection containing all product instances with a tag
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public ProductInstanceDictionary this[Tag key]
        {
            get
            {
                ProductInstanceDictionary items = new ProductInstanceDictionary();

                foreach (KeyValuePair<ProductInstance.ID, ProductInstance> pair in this)
                {
                    if (pair.Value.definition.feature.tags.Contains(key))
                    {
                        items.Add(pair.Value);
                    }
                }

                return items;
            }
        }
    }
}
