﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace codegenLog
{

    /// <summary>
    /// definition of the different message levels; note that the order is important, 
    /// because the (numerically) higher levels, such as WARNING, also include the lower levels, such as ERROR
    /// </summary>
    public enum LEVEL
    {
        /// <summary>
        /// showing only error messages
        /// </summary>
        ERROR,
        /// <summary>
        /// showing errors + warnings
        /// </summary>
        WARNING,
        /// <summary>
        /// showing errors + warnings + informations
        /// </summary>
        INFO,
    }

    /// <summary>
    /// static class for logging trace messages
    /// </summary>
    public static class Logging
    {
        /// <summary>
        /// text writer where the logging messages are written; console on default
        /// </summary>
        static private TextWriter writer = Console.Out;

        private static List<object> contextList = new List<object>();

        private static Dictionary<object, Queue<Log>> contextBuffer = new Dictionary<object, Queue<Log>>();

        private static Dictionary<LEVEL, int> logStatistics = new Dictionary<LEVEL, int>();

        private static int indicationIndex = 0;

        public static event LogEventHandler errorLogEvent;

        public delegate void LogEventHandler(object context);

        static Logging()
        {
            string context = "";
            contextList.Add(context);
            contextBuffer.Add(context, new Queue<Log>());
        }

        public class Log
        {
            public string message { get; private set; }
            public LEVEL level { get; private set; }

            public int offset { get; private set; }

            public Log(string msg, LEVEL lvl, int off = 0)
            {
                level = lvl;
                message = msg;
                offset = off;
            }
        }

        public static char contextIndicator { get; private set; } = '\t';

        private static readonly Dictionary<LEVEL, ConsoleColor> colors = new Dictionary<LEVEL, ConsoleColor>
        {
            {LEVEL.ERROR, ConsoleColor.Red },
            {LEVEL.INFO, ConsoleColor.Green },
            {LEVEL.WARNING, ConsoleColor.Yellow },
        };
        /// <summary>
        /// current set level for output messages; all messages with a level lower or equal outputMessage Level will be written
        /// </summary>
        public static LEVEL outputMessageLevel { get; set; } = LEVEL.INFO;      

        public static void enterContext(object context)
        {
            object current = contextList.Last();
            //clear buff
            contextBuffer[current].Clear();

            contextList.Add(context);
            contextBuffer.Add(context, new Queue<Log>());
            log(string.Format("enter context {0}", context), LEVEL.INFO);

            indicationIndex++;
        }
        public static void leaveContext()
        {
            if (contextList.Count > 1)
            {                
                object current = contextList.Last();                

                log(string.Format("leave context {0}", current), LEVEL.INFO);
                log();

                contextList.Remove(current);
                contextBuffer.Remove(current);

                if (indicationIndex > 0)
                {
                    indicationIndex--;
                }
            }
        }

        public static void  log(Log inst = null, bool force = false)
        {
            if (inst == null)
            {
                writer.WriteLine();
                return;
            }
            log(inst.message, inst.level, force, inst.offset);
        }

        /// <summary>
        /// logs a message string according to its level and the currently configured outputMessageLevel
        /// </summary>
        /// <param name="message"></param>
        /// <param name="level"></param>
        public static void log(string message, LEVEL level = 0, bool force = false, int offset = 0)
        {
            object current = contextList.Last();

            //always empty context buffer if an error is happening, before writing the error
            if (level == LEVEL.ERROR)
            {                
                if (contextBuffer.ContainsKey(current))
                {
                    foreach (Log msg in contextBuffer[current])
                    {
                        log(msg, true);
                    }
                }
            }
            //write message if level is right
            if (level <= outputMessageLevel || force == true)
            {
                Console.ForegroundColor = colors[level];
                writer.WriteLine("{0}{1} {2}", generateContextIndication(offset), level, message);
                Console.ResetColor();

                switch (level)
                {
                    case LEVEL.ERROR:
                        if (errorLogEvent != null)
                        {
                            errorLogEvent(current);
                        }
                        break;
                }

            }
            //buffer message in case level is not right
            else
            {
                int off = 0;

                if (contextBuffer[current].Count == 0)
                {
                    off = -1;
                }

                contextBuffer[current].Enqueue(new Log(message, level, off));
            }

            //create statistic for level if not existing
            if (logStatistics.ContainsKey(level) == false)
            {
                logStatistics.Add(level, 0);
            }

            //increment statistic for level
            logStatistics[level]++;
        }

        public static void status(string message, LEVEL level = 0, ConsoleColor ?colorOverride = null)
        {

            ConsoleColor? displayColor = colorOverride != null ? colorOverride : colors[level];

            if (level <= outputMessageLevel)
            {            
                Console.ForegroundColor = (ConsoleColor)displayColor;
                writer.Write(string.Format("\r{0}{1}", generateContextIndication(), message).PadRight(Console.BufferWidth - 1));
                Console.ResetColor();
            }
        }
        public static void endStatus()
        {
            writer.Write(writer.NewLine);
        }

        public static void writeLogStatistics()
        {
            writer.WriteLine("<<logging statistics>>");
            foreach (LEVEL level in logStatistics.Keys)
            {
                Console.ForegroundColor = colors[level];
                writer.WriteLine("{0}: {1}", level, logStatistics[level]);
                Console.ResetColor();
            }
        }

        /// <summary>
        /// generates context indicator depending on how far the context is nested
        /// </summary>
        /// <returns></returns>
        private static string generateContextIndication(int offset = 0)
        {
            int value = indicationIndex + offset;

            if (value < 0)
            {
                value = 0;
            }

            return new string(contextIndicator, value);
        }
    }
}
