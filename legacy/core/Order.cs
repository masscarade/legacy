﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Xml;
using System.ComponentModel;
using System.Collections;

namespace codegen
{
    [DataContract]
    public class Order : INotifyPropertyChanged, IEnumerable<ProductInstance>
    {
        private ID _id = new ID("testID");

        [DataMember]
        public ID id
        {
            get
            {
                return _id;
            }
                
            set
            {
                _id = value;
                onPropertyChanged(new PropertyChangedEventArgs("id"));
            }
        }

        private DateTime _orderDate;    

        [DataMember]
        public DateTime orderDate
        {
            get
            {
                return _orderDate;
            }

            set
            {
                _orderDate = value;
                onPropertyChanged(new PropertyChangedEventArgs("orderDate"));
            }
        }

        [DataMember]
        public ProductInstanceDictionary products { get; private set; } = new ProductInstanceDictionary();

        public event PropertyChangedEventHandler PropertyChanged;

        public static Order getSample()
        {
            Order inst = new Order()
            {
                products = ProductInstanceDictionary.getSample(),
                id = ID.getSample(),
                orderDate = new DateTime(),
            };
            

            return inst;
        }

        public void onPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, e);
        }

        public IEnumerator<ProductInstance> GetEnumerator()
        {
            foreach (ProductInstance inst in products.Values)
            {
                yield return inst;
                foreach (ProductInstance child in inst.getChildren())
                {
                    yield return child;
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }

        [DataContract]
        public class ID
        {
            [DataMember]
            public string name { get; set; }

            public ID(string name)
            {
                this.name = name;
            }

            public override string ToString()
            {
                return name;
            }

            public static ID getSample()
            {
                return new ID("sample_id")
                {
                };
            }
        }

        public void setId(string value)
        {
            id.name = value;
        }

        public void remove(ProductInstance.ID id)
        {
            products.Remove(id);
        }
    }
}
