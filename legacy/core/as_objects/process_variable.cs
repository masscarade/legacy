﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace codegen
{
    interface IProcessVariable
    {
        string path { get;}
    }

    //template class for datatype specific PV -> used later
    class PV<T> : IProcessVariable
    {
        public string path { get; private set; }

        public T value { get; set; }
    }

    //generic class, which treats every value as a string, used for now
    class PV : IProcessVariable
    {
        public string path { get; private set; }
        public string value { get; set; }
    }
}
