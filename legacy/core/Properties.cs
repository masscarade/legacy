﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using System.Windows.Data;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace codegen
{

    [DataContract]
    [KnownType(typeof(FixDataInput))]
    [KnownType(typeof(FixDataOutput))]
    [KnownType(typeof(StaticInterlock))]
    [KnownType(typeof(DynamicInterlock))]
    [KnownType(typeof(DynamicInterlockException))]
    [KnownType(typeof(InstanceName))]
    [KnownType(typeof(CommandProperty))]
    public abstract class Property : IEquatable<Property>, IDeepCloneable<Property>, INotifyPropertyChanged
    {
        public abstract ObservableCollection<UIElement> uiElements { get; protected set; }

        public delegate void UiElementsChanged();
        public event UiElementsChanged onUiElementsChange;

        public virtual event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public abstract void initUiElements();

        [DataMember]
        public abstract string description { get; protected set; }

        public abstract bool Equals(Property other);
        public abstract override int GetHashCode();

        public Property(string description)
        {
            this.description = description;
        }

        protected Property()
        {
        }

        public static string loadValue()
        {
            return "notImplemented!!";
        }

        public abstract Property deepClone();

        [DataContract]
        public class FixDataOutput : Property, IOutput
        {

            [DataMember]
            public string source { get; private set; }

            private FixDataOutput() :base()
            {
            }

            public override void initUiElements()
            {
                uiElements = new ObservableCollection<UIElement>();

                Grid grid = new Grid()
                {
                    Name = "_grid",
                    HorizontalAlignment = HorizontalAlignment.Stretch,
                };

                //first column containing static label and description text
                grid.ColumnDefinitions.Add(new ColumnDefinition()
                {
                    Width = GridLength.Auto,
                });

                //second column containing destination and value
                grid.ColumnDefinitions.Add(new ColumnDefinition()
                {
                    Width = GridLength.Auto,
                });

                grid.RowDefinitions.Add(new RowDefinition()
                {
                    Height = GridLength.Auto,
                });

                grid.RowDefinitions.Add(new RowDefinition()
                {
                    Height = GridLength.Auto,
                });

                Label label = new Label()
                {
                    Name = "_label",
                    Content = "Output of FixData",
                    HorizontalAlignment = HorizontalAlignment.Left,
                };

                label.SetValue(Grid.RowProperty, 0);
                label.SetValue(Grid.ColumnProperty, 0);

                grid.Children.Add(label);

                Label box1 = new Label()
                {
                    Name = "_description",
                    HorizontalAlignment = HorizontalAlignment.Right,
                };

                box1.SetValue(Grid.RowProperty, 0);
                box1.SetValue(Grid.ColumnProperty, 1);

                Binding box1Binding = new Binding("description")
                {
                    Mode = BindingMode.OneWay,
                };
                box1.SetBinding(Label.ContentProperty, box1Binding);
                grid.Children.Add(box1);

                Label box2 = new Label()
                {
                    Name = "_destination",
                    HorizontalAlignment = HorizontalAlignment.Left,
                };

                box2.SetValue(Grid.RowProperty, 1);
                box2.SetValue(Grid.ColumnProperty, 0);

                Binding box2Binding = new Binding("source")
                {
                    Mode = BindingMode.OneWay,
                };
                box2.SetBinding(Label.ContentProperty, box2Binding);
                grid.Children.Add(box2);

                Label box3 = new Label()
                {
                    Name = "_value",
                    HorizontalAlignment = HorizontalAlignment.Right,
                };

                box3.SetValue(Grid.RowProperty, 1);
                box3.SetValue(Grid.ColumnProperty, 1);

                Binding box3Binding = new Binding("value")
                {
                    Mode = BindingMode.OneWay,
                };
                box3.SetBinding(Label.ContentProperty, box3Binding);
                grid.Children.Add(box3);

                uiElements.Add(new GroupBox()
                {
                    DataContext = this,
                    Content = grid,
                    Header = "Fix data " + description,
                    HorizontalAlignment = HorizontalAlignment.Stretch,
                });
            }

            public FixDataOutput(string description, string source, bool isSample = false) : base(description)
            {
                this.source = source;
                //value = loadValue();

                if (isSample == false)
                {
                    initUiElements();
                }
            }

            [DataMember]
            public override string description { get; protected set; }

            public override ObservableCollection<UIElement> uiElements { get; protected set; }

            private string _value;

            [DataMember]
            public string value
            {
                get
                {
                    return _value;
                }
                private set
                {
                    value = _value;
                    OnPropertyChanged("value");
                }
            }

            public override bool Equals(Property other)
            {
                if (other.GetType() == this.GetType())
                {
                    if (this.source == ((FixDataOutput)other).source)
                    {
                        return true;
                    }
                }
                return false;
            }

            public override int GetHashCode()
            {
                return source.GetHashCode();
            }

            public static FixDataOutput getSample()
            {
                return new FixDataOutput("emptyDescription", "emptySource", true)
                {
                };
            }

            public override Property deepClone()
            {
                return new FixDataOutput(this.description, this.source)
                {
                    value = this.value,
                };
            }

            public override void findAndReplace(string find, string replace)
            {
                //does nothing, because it is only displaying data
                //throw new NotImplementedException();
            }
        }

        [DataContract]
        public class DynamicInterlockException : DynamicInterlock
        {
            protected override string header
            {
                get
                {
                    return base.header + " Exception";
                }
            }

            public DynamicInterlockException(string description, bool isSample = false) : base(description, isSample)
            {
            }
        }

        [DataContract]
        [KnownType(typeof(DynamicInterlockException))]
        public class DynamicInterlock : Property
        {          
            [DataMember]
            public ObservableCollection<Axis> axes { get; set; }

            protected virtual string header
            {
                get
                {
                    return "Dynamic Interlock";
                }
            }

            public override ObservableCollection<UIElement> uiElements { get; protected set; }

            private void init()
            {
                if (uiElements == null)
                {
                    uiElements = new ObservableCollection<UIElement>();
                }

                Grid grid = new Grid()
                {
                    Name = "_grid",
                };

                //first column containing static label and description text
                grid.ColumnDefinitions.Add(new ColumnDefinition()
                {
                    Width = GridLength.Auto,
                });

                //second column containing destination and value
                grid.ColumnDefinitions.Add(new ColumnDefinition()
                {
                    Width = GridLength.Auto,
                });

                grid.RowDefinitions.Add(new RowDefinition()
                {
                    Height = GridLength.Auto,
                });

                grid.RowDefinitions.Add(new RowDefinition()
                {
                    Height = GridLength.Auto,
                });

                Label box1 = new Label()
                {
                    Name = "_command",
                    HorizontalAlignment = HorizontalAlignment.Left,
                };

                box1.SetValue(Grid.RowProperty, 0);
                box1.SetValue(Grid.ColumnProperty, 0);

                Binding box1Binding = new Binding("command")
                {
                    Mode = BindingMode.OneWay,
                };
                box1.SetBinding(Label.ContentProperty, box1Binding);
                grid.Children.Add(box1);

                Label box2 = new Label()
                {
                    Name = "_command",
                    HorizontalAlignment = HorizontalAlignment.Left,
                };

                box2.SetValue(Grid.RowProperty, 0);
                box2.SetValue(Grid.ColumnProperty, 0);

                Binding box2Binding = new Binding("description")
                {
                    Mode = BindingMode.OneWay,
                };
                box2.SetBinding(Label.ContentProperty, box2Binding);
                grid.Children.Add(box2);

                Button addButton = new Button()
                {
                    Name = "_addButton",
                    Content = "Add axis",
                };

                addButton.Click += AddButton_Click;
                addButton.SetValue(Grid.RowProperty, 1);
                addButton.SetValue(Grid.ColumnProperty, 0);
                grid.Children.Add(addButton);


                uiElements.Add(new GroupBox()
                {
                    DataContext = this,
                    Content = grid,
                    Header = header,
                    HorizontalAlignment = HorizontalAlignment.Stretch,
                });

                if (axes == null)
                {
                    axes = new ObservableCollection<Axis>();
                    axes.CollectionChanged += this.OnCollectionChanged;
                }
                else
                {
                    Axis[] temp = new Axis[axes.Count];
                    axes.CopyTo(temp, 0);
                    axes.Clear();

                    foreach (Axis axis in temp)
                    {
                        addAxis((UIElement)((GroupBox)uiElements[0]).Content, axis);
                    }
                }
            }

            public override string ToString()
            {
                return axes.ToArray().ToString();
            }

            public DynamicInterlock(string description, bool isSample = false) : base(description)
            {

                if (isSample == false)
                {
                    init();
                }
            }

            private void addAxis(UIElement parent, Collection<Axis> newAxes)
            {
                if (newAxes.Count < 1)
                {
                    onUiElementsChange.Invoke();
                    return;
                }
                foreach (Axis axis in newAxes)
                {
                    addAxis(parent, axis);
                }
            }
            private void addAxis(UIElement parent, Axis newAxis = null)
            {
                if (newAxis == null)
                {
                    newAxis = Axis.getSample();
                }

                Grid grid = (Grid)parent;

                grid.RowDefinitions.Add(new RowDefinition()
                {
                    Height = GridLength.Auto,
                });

                grid.RowDefinitions.Add(new RowDefinition()
                {
                    Height = GridLength.Auto,
                });
                grid.RowDefinitions.Add(new RowDefinition()
                {
                    Height = GridLength.Auto,
                });
                grid.RowDefinitions.Add(new RowDefinition()
                {
                    Height = GridLength.Auto,
                });
                grid.RowDefinitions.Add(new RowDefinition()
                {
                    Height = GridLength.Auto,
                });


                Label box1 = new Label()
                {
                    Name = "_axis",
                    Content = "Axis name",
                    HorizontalAlignment = HorizontalAlignment.Left,
                };

                box1.SetValue(Grid.RowProperty, grid.RowDefinitions.Count - 1);
                box1.SetValue(Grid.ColumnProperty, 0);

                grid.Children.Add(box1);

                TextBox box3 = new TextBox()
                {
                    Name = "_axisInsert",
                    IsReadOnly = false,
                    HorizontalAlignment = HorizontalAlignment.Right,
                };

                box3.SetValue(Grid.RowProperty, grid.RowDefinitions.Count - 1);
                box3.SetValue(Grid.ColumnProperty, 1);

                Binding box3Binding = new Binding("axisName")
                {
                    Mode = BindingMode.TwoWay,
                    Source = newAxis,
                    UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
                };
                box3.SetBinding(TextBox.TextProperty, box3Binding);
                grid.Children.Add(box3);               

                Button removeButton = new Button()
                {
                    Name = "_removeButton",
                    Content = "Remove axis",
                };

                removeButton.Click += RemoveButton_Click;
                removeButton.DataContext = newAxis;
                removeButton.SetValue(Grid.RowProperty, 1);
                removeButton.SetValue(Grid.ColumnProperty, 0);
                removeButton.SetValue(Grid.RowProperty, grid.RowDefinitions.Count - 4);
                removeButton.SetValue(Grid.ColumnProperty, 0);

                grid.Children.Add(removeButton);

                Separator sep1 = new Separator()
                {
                    Name = "_line1",
                };

                sep1.SetValue(Grid.RowProperty, grid.RowDefinitions.Count - 5);
                sep1.SetValue(Grid.ColumnProperty, 0);

                grid.Children.Add(sep1);

                Separator sep2 = new Separator()
                {
                    Name = "_line2",
                };

                sep2.SetValue(Grid.RowProperty, grid.RowDefinitions.Count - 5);
                sep2.SetValue(Grid.ColumnProperty, 1);

                grid.Children.Add(sep2);

                axes.Add(newAxis);

                onUiElementsChange += DynamicInterlock_onUiElementsChange;
                onUiElementsChange.Invoke();
            }

            private void DynamicInterlock_onUiElementsChange()
            {
                //throw new NotImplementedException();
            }

            private void AddButton_Click(object sender, RoutedEventArgs e)
            {
                addAxis((UIElement)((Button)sender).Parent);
            }

            public override string description { get; protected set; }

            void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
            {

            }
            public void RemoveButton_Click(object sender, RoutedEventArgs e)
            {
                if (sender.GetType() == typeof(Button))
                {
                    Button element = (Button)sender;

                    if (element.DataContext != null && element.DataContext.GetType() == typeof(Axis))
                    {
                        Axis removeAxis = (Axis)element.DataContext;

                        this.axes.Remove(removeAxis);

                        Collection<Axis> temp = this.axes;
                       
                        this.axes = new ObservableCollection<Axis>();
                        addAxis((UIElement)((GroupBox)uiElements[0]).Content, temp);

                        uiElements.Clear();
                        init();
                    }
                }
            }

            public override bool Equals(Property other)
            {
                if (other.GetType() != this.GetType())
                {
                    return false;
                }

                DynamicInterlock obj = (DynamicInterlock)other;

                return axes.SequenceEqual(obj.axes);
            }

            public override int GetHashCode()
            {
                return axes.GetHashCode();
            }

            public static DynamicInterlock getSample()
            {
                return new DynamicInterlock("emptyDescription", true)
                {
                    axes = new ObservableCollection<Axis>(new Axis[]
                    {
                        new Axis()
                        {
                            axisName = "Core1",
                        },
                        new Axis()
                        {
                            axisName = "Core3", 
                        }
                    }),
                };
            }

            public override Property deepClone()
            {
                DynamicInterlock inst = new DynamicInterlock(this.description)
                {
                };

                inst.axes.CollectionChanged += inst.OnCollectionChanged;

                foreach (Axis axis in this.axes)
                {
                    Axis newAxis = new Axis()
                    {
                        axisName = axis.axisName,  
                    };

                    inst.axes.Add(newAxis);
                    //inst.addAxis((UIElement)((GroupBox)inst.uiElements[0]).Content, newAxis);
                }

                return inst;
            }

            public override void initUiElements()
            {
                init();
            }

            public override void findAndReplace(string find, string replace)
            {
                //only axis name considered
                foreach (Axis axis in axes)
                {
                    axis.axisName = axis.axisName.Replace(find, replace);
                }                
            }

            [DataContract]
            public class Axis : INotifyPropertyChanged
            {             
                private string _axisName;

                [DataMember]
                public string axisName
                {
                    get { return _axisName; }
                    set
                    {
                        _axisName = value;
                        // Call OnPropertyChanged whenever the property is updated
                        OnPropertyChanged("axisName");
                    }
                }

                public override string ToString()
                {
                    return axisName;
                }

                public event PropertyChangedEventHandler PropertyChanged;

                // Create the OnPropertyChanged method to raise the event
                protected void OnPropertyChanged(string name)
                {
                    PropertyChangedEventHandler handler = PropertyChanged;
                    if (handler != null)
                    {
                        handler(this, new PropertyChangedEventArgs(name));
                    }
                }

                public static Axis getSample()
                {
                    return new Axis()
                    {
                        axisName = "sample_axis",
                    };
                }

                public bool Equals(Axis other)
                {

                    return axisName.Equals(other.axisName);
                }
            }
        }

        [DataContract]
        public class StaticInterlock : Property
        {     
            [DataMember]
            public string command { get; set; }

            [DataMember]
            public ObservableCollection<Axis> axes { get; set; }

            public override ObservableCollection<UIElement> uiElements { get; protected set; }

            private void init()
            {
                if (uiElements == null)
                {
                    uiElements = new ObservableCollection<UIElement>();
                }

                Grid grid = new Grid()
                {
                    Name = "_grid",
                };

                //first column containing static label and description text
                grid.ColumnDefinitions.Add(new ColumnDefinition()
                {
                    Width = GridLength.Auto,
                });

                //second column containing destination and value
                grid.ColumnDefinitions.Add(new ColumnDefinition()
                {
                    Width = GridLength.Auto,
                });

                grid.RowDefinitions.Add(new RowDefinition()
                {
                    Height = GridLength.Auto,
                });

                grid.RowDefinitions.Add(new RowDefinition()
                {
                    Height = GridLength.Auto,
                });

                Label box1 = new Label()
                {
                    Name = "_command",
                    HorizontalAlignment = HorizontalAlignment.Left,
                };

                box1.SetValue(Grid.RowProperty, 0);
                box1.SetValue(Grid.ColumnProperty, 0);

                Binding box1Binding = new Binding("command")
                {
                    Mode = BindingMode.OneWay,
                };
                box1.SetBinding(Label.ContentProperty, box1Binding);
                grid.Children.Add(box1);

                Label box2 = new Label()
                {
                    Name = "_command",
                    HorizontalAlignment = HorizontalAlignment.Left,
                };

                box2.SetValue(Grid.RowProperty, 0);
                box2.SetValue(Grid.ColumnProperty, 1);

                Binding box2Binding = new Binding("description")
                {
                    Mode = BindingMode.OneWay,
                };
                box2.SetBinding(Label.ContentProperty, box2Binding);
                grid.Children.Add(box2);

                Button addButton = new Button()
                {
                    Name = "_addButton",
                    Content = "Add axis",
                };

                addButton.Click += AddButton_Click;
                addButton.SetValue(Grid.RowProperty, 1);
                addButton.SetValue(Grid.ColumnProperty, 0);
                grid.Children.Add(addButton);


                uiElements.Add(new GroupBox()
                {
                    DataContext = this,
                    Content = grid,
                    Header = "Static interlock",
                    HorizontalAlignment = HorizontalAlignment.Stretch,
                });

                if (axes == null)
                {
                    axes = new ObservableCollection<Axis>();
                    axes.CollectionChanged += this.OnCollectionChanged;
                }
                else
                {
                    Axis[] temp = new Axis[axes.Count];
                    axes.CopyTo(temp, 0);
                    axes.Clear();

                    foreach (Axis axis in temp)
                    {
                        addAxis((UIElement)((GroupBox)uiElements[0]).Content, axis);
                    }
                }
            }

            public StaticInterlock(string description, bool isSample = false) : base(description)
            {

                if (isSample == false)
                {
                    init();
                }
            }

            private void addAxis(UIElement parent, Collection<Axis> newAxes)
            {
                if (newAxes.Count < 1)
                {
                    onUiElementsChange.Invoke();
                    return;
                }
                foreach (Axis axis in newAxes)
                {
                    addAxis(parent, axis);
                }
            }
            private void addAxis(UIElement parent, Axis newAxis = null)
            {
                if (newAxis == null)
                {
                    newAxis = Axis.getSample();
                }                          

                Grid grid = (Grid)parent;

                grid.RowDefinitions.Add(new RowDefinition()
                {
                    Height = GridLength.Auto,
                });

                grid.RowDefinitions.Add(new RowDefinition()
                {
                    Height = GridLength.Auto,
                });
                grid.RowDefinitions.Add(new RowDefinition()
                {
                    Height = GridLength.Auto,
                });
                grid.RowDefinitions.Add(new RowDefinition()
                {
                    Height = GridLength.Auto,
                });
                grid.RowDefinitions.Add(new RowDefinition()
                {
                    Height = GridLength.Auto,
                });


                Label box1 = new Label()
                {
                    Name = "_axis",
                    Content = "Axis name",
                    HorizontalAlignment = HorizontalAlignment.Left,
                };

                box1.SetValue(Grid.RowProperty, grid.RowDefinitions.Count -1);
                box1.SetValue(Grid.ColumnProperty, 0);
             
                grid.Children.Add(box1);

                TextBox box3 = new TextBox()
                {
                    Name = "_axisInsert",
                    IsReadOnly = false,
                    HorizontalAlignment = HorizontalAlignment.Right,
                };

                box3.SetValue(Grid.RowProperty, grid.RowDefinitions.Count - 1);
                box3.SetValue(Grid.ColumnProperty, 1);

                Binding box3Binding = new Binding("axisName")
                {
                    Mode = BindingMode.TwoWay,
                    Source = newAxis,
                    UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
                };
                box3.SetBinding(TextBox.TextProperty, box3Binding);
                grid.Children.Add(box3);

                Label labelExpectedState = new Label()
                {
                    Name = "_expectedState",
                    Content = "Expected state",
                    HorizontalAlignment = HorizontalAlignment.Left,
                };

                labelExpectedState.SetValue(Grid.RowProperty, grid.RowDefinitions.Count - 2);
                labelExpectedState.SetValue(Grid.ColumnProperty, 0);

                grid.Children.Add(labelExpectedState);

                TextBox boxExpectedState = new TextBox()
                {
                    Name = "_expectedState",
                    IsReadOnly = false,
                    HorizontalAlignment = HorizontalAlignment.Right,
                };

                boxExpectedState.SetValue(Grid.RowProperty, grid.RowDefinitions.Count - 2);
                boxExpectedState.SetValue(Grid.ColumnProperty, 1);

                Binding boxExpectedStateBinding = new Binding("expectedState")
                {
                    Mode = BindingMode.TwoWay,
                    Source = newAxis,
                };
                boxExpectedState.SetBinding(TextBox.TextProperty, boxExpectedStateBinding);
                grid.Children.Add(boxExpectedState);

                Label labelReaction = new Label()
                {
                    Name = "_reaction",
                    Content = "Reaction",
                    HorizontalAlignment = HorizontalAlignment.Left,
                };

                labelReaction.SetValue(Grid.RowProperty, grid.RowDefinitions.Count - 3);
                labelReaction.SetValue(Grid.ColumnProperty, 0);

                grid.Children.Add(labelReaction);

                TextBox boxReaction = new TextBox()
                {
                    Name = "_reaction",
                    IsReadOnly = false,
                    HorizontalAlignment = HorizontalAlignment.Right,
                };

                boxReaction.SetValue(Grid.RowProperty, grid.RowDefinitions.Count - 3);
                boxReaction.SetValue(Grid.ColumnProperty, 1);

                Binding boxReactionBinding = new Binding("reaction")
                {
                    Mode = BindingMode.TwoWay,
                    Source = newAxis,
                };
                boxReaction.SetBinding(TextBox.TextProperty, boxReactionBinding);
                grid.Children.Add(boxReaction);

                Button removeButton = new Button()
                {
                    Name = "_removeButton",
                    Content = "Remove axis",
                };

                removeButton.Click += RemoveButton_Click;
                removeButton.DataContext = newAxis;
                removeButton.SetValue(Grid.RowProperty, 1);
                removeButton.SetValue(Grid.ColumnProperty, 0);
                removeButton.SetValue(Grid.RowProperty, grid.RowDefinitions.Count - 4);
                removeButton.SetValue(Grid.ColumnProperty, 0);

                grid.Children.Add(removeButton);

                Separator sep1 = new Separator()
                {
                    Name = "_line1",
                };

                sep1.SetValue(Grid.RowProperty, grid.RowDefinitions.Count - 5);
                sep1.SetValue(Grid.ColumnProperty, 0);

                grid.Children.Add(sep1);

                Separator sep2 = new Separator()
                {
                    Name = "_line2",
                };

                sep2.SetValue(Grid.RowProperty, grid.RowDefinitions.Count - 5);
                sep2.SetValue(Grid.ColumnProperty, 1);

                grid.Children.Add(sep2);

                axes.Add(newAxis);

                onUiElementsChange += StaticInterlock_onUiElementsChange;
                onUiElementsChange.Invoke();
            }

            private void StaticInterlock_onUiElementsChange()
            {
                //throw new NotImplementedException();
            }

            private void AddButton_Click(object sender, RoutedEventArgs e)
            {                
                addAxis((UIElement)((Button)sender).Parent);
            }

            public override string description { get; protected set; }           

            void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
            {
               
            }
            public void RemoveButton_Click(object sender, RoutedEventArgs e)
            {
                if (sender.GetType() == typeof(Button))
                {
                    Button element = (Button)sender;

                    if (element.DataContext != null && element.DataContext.GetType() == typeof(Axis))
                    {
                        Axis removeAxis = (Axis)element.DataContext;

                        this.axes.Remove(removeAxis);

                        Collection<Axis> temp = this.axes;

                        uiElements.Clear();
                        init();

                        this.axes = new ObservableCollection<Axis>();
                        addAxis((UIElement)((GroupBox)uiElements[0]).Content, temp);
                    }
                }     
            }

            public override bool Equals(Property other)
            {
                if (other.GetType() != this.GetType())
                {
                    return false;
                }

                StaticInterlock obj = (StaticInterlock)other;

                return command.Equals(obj.command) && axes.SequenceEqual(obj.axes);
            }

            public override int GetHashCode()
            {
                return command.GetHashCode();
            }

            public static StaticInterlock getSample()
            {
                return new StaticInterlock("emptyDescription", true)
                {
                    axes = new ObservableCollection<Axis>(new Axis[]
                    {
                        new Axis()
                        {
                            axisName = "Core1",
                            expectedState = "12",
                            reaction = "2",
                        },
                        new Axis()
                        {
                            axisName = "Core3",
                            expectedState = "33",
                            reaction = "1",
                        }
                    }),

                    command = "ClampClose",
                };
            }       

            public override Property deepClone()
            {
                StaticInterlock inst = new StaticInterlock(this.description)
                { 
                    command = this.command,
                };

                inst.axes.CollectionChanged += inst.OnCollectionChanged;

                foreach (Axis axis in this.axes)
                {
                    Axis newAxis = new Axis()
                    {
                        axisName = axis.axisName,
                        expectedState = axis.expectedState,
                        reaction = axis.reaction,
                    };

                    //inst.axes.Add(newAxis);
                    inst.addAxis((UIElement)((GroupBox)inst.uiElements[0]).Content, newAxis);
                }

                return inst;
            }

            public override void initUiElements()
            {
                init();
            }

            public override void findAndReplace(string find, string replace)
            {
                //only axis name considered
                foreach (Axis axis in axes)
                {
                    axis.axisName = axis.axisName.Replace(find, replace);
                }
            }

            [DataContract]
            public class Axis : INotifyPropertyChanged
            {
                [DataMember]
                public string reaction { get; set; }

                [DataMember]
                public string expectedState { get; set; }

                private string _axisName;            

                [DataMember]
                public string axisName
                {
                    get { return _axisName; }
                    set
                    {
                        _axisName = value;
                        // Call OnPropertyChanged whenever the property is updated
                        OnPropertyChanged("axisName");
                    }
                }

                public event PropertyChangedEventHandler PropertyChanged;

                // Create the OnPropertyChanged method to raise the event
                protected void OnPropertyChanged(string name)
                {
                    PropertyChangedEventHandler handler = PropertyChanged;
                    if (handler != null)
                    {
                        handler(this, new PropertyChangedEventArgs(name));
                    }
                }

                public static Axis getSample()
                {
                    return new Axis()
                    {
                         axisName = "sample_axis",
                         expectedState = "value_of_expected_state",
                         reaction = "1:wait_2:stop",
                    };
                }

                public bool Equals(Axis other)
                {

                    return reaction.Equals(other.reaction) && expectedState.Equals(other.expectedState) && axisName.Equals(other.axisName);
                }             
            }
        }

        [DataContract]
        public class CommandProperty : Property
        {
            public override string description { get; protected set; }

            public override ObservableCollection<UIElement> uiElements { get; protected set; }

            [DataMember]
            public string commandName { get; private set; }
            [DataMember]
            public string propertyId { get; private set; }

            private string _propertyValue = "empty_value";

            [DataMember]
            public string propertyValue
            {
                get
                {
                    return _propertyValue;
                }
                set
                {
                    _propertyValue = value;
                    OnPropertyChanged("propertyValue");
                }
            }

            public CommandProperty(string description, bool isSample = false) : base(description)
            {
                if (isSample == false)
                {
                    initUiElements();
                }
            }

            public override Property deepClone()
            {
                return new CommandProperty(description)
                {
                    commandName = this.commandName,
                    propertyId = this.propertyId,
                    propertyValue = this.propertyValue,
                };
            }

            public override bool Equals(Property other)
            {
                if (other.GetType() == this.GetType())
                {
                    CommandProperty temp = (CommandProperty)other;
                    return this.commandName == temp.commandName && this.propertyId == temp.propertyId;
                }
                return false;
            }

            public override int GetHashCode()
            {
                return commandName.GetHashCode() ^ propertyId.GetHashCode();
            }

            public override void initUiElements()
            {
                uiElements = new ObservableCollection<UIElement>();

                Grid grid = new Grid()
                {
                    Name = "_grid",
                };

                //first column containing static label and description text
                grid.ColumnDefinitions.Add(new ColumnDefinition()
                {
                    Width = GridLength.Auto,
                });

                //second column containing destination and value
                grid.ColumnDefinitions.Add(new ColumnDefinition()
                {
                    Width = GridLength.Auto,
                });

                grid.RowDefinitions.Add(new RowDefinition()
                {
                    Height = GridLength.Auto,
                });

                grid.RowDefinitions.Add(new RowDefinition()
                {
                    Height = GridLength.Auto,
                });

                Label box2 = new Label()
                {
                    Name = "_destination",
                    HorizontalAlignment = HorizontalAlignment.Left,
                };

                box2.SetValue(Grid.RowProperty, 0);
                box2.SetValue(Grid.ColumnProperty, 0);                

                Binding box2Binding = new Binding("commandName")
                {
                    Mode = BindingMode.OneWay,
                };
                box2.SetBinding(Label.ContentProperty, box2Binding);
                grid.Children.Add(box2);

                Label boxProperty = new Label()
                {
                    Name = "_destination",
                    HorizontalAlignment = HorizontalAlignment.Left,
                };

                boxProperty.SetValue(Grid.RowProperty, 0);
                boxProperty.SetValue(Grid.ColumnProperty, 1);
                
                Binding boxPropertyBinding = new Binding("propertyId")
                {
                    Mode = BindingMode.OneWay,
                };
                boxProperty.SetBinding(Label.ContentProperty, boxPropertyBinding);
                grid.Children.Add(boxProperty);

                Label value = new Label()
                {
                    Name = "_label",
                    Content = "Property value",
                    HorizontalAlignment = HorizontalAlignment.Left,
                };

                value.SetValue(Grid.RowProperty, 1);
                value.SetValue(Grid.ColumnProperty, 0);

                grid.Children.Add(value);

                TextBox box3 = new TextBox()
                {
                    Name = "_value",
                    IsReadOnly = false,
                    HorizontalAlignment = HorizontalAlignment.Right,
                };

                box3.SetValue(Grid.RowProperty, 1);
                box3.SetValue(Grid.ColumnProperty, 1);

                Binding box3Binding = new Binding("propertyValue")
                {
                    Mode = BindingMode.TwoWay,
                };
                box3.SetBinding(TextBox.TextProperty, box3Binding);
                grid.Children.Add(box3);

                uiElements.Add(new GroupBox()
                {
                    DataContext = this,
                    Content = grid,
                    Header = "Sequencer command property " + description,
                    HorizontalAlignment = HorizontalAlignment.Stretch,
                });
            }

            public static CommandProperty getSample()
            {
                return new CommandProperty("emptyDescription", true)
                {
                    propertyId = "id_of_the_property",
                    propertyValue = "value_of_the_property",
                    commandName = "name_of_the_command",
                };
            }

            public override void findAndReplace(string find, string replace)
            {
                propertyValue = propertyValue.Replace(find, replace);
            }
        }

        [DataContract]
        public class FixDataInput : Property, IInput
        {
            [DataMember]
            public string destination { get; private set; } = "emptyDestination";

            [DataMember]
            public string value
            {
                get
                {
                    return _value;
                }
                set
                {
                    _value = value;
                    OnPropertyChanged("value");
                }
            }

            private string _value = "emptyValue";           

            public override ObservableCollection<UIElement> uiElements { get; protected set; }
            public override string description { get; protected set; } = "emptyDescription";

            public FixDataInput(string destination, string description, bool isSample = false) : base(description)
            {
                this.destination = destination;

                if (isSample == false)
                {
                    initUiElements();
                }
            }

            public override bool Equals(Property other)
            {
                if (other.GetType() == this.GetType())
                {
                    if (this.destination == ((FixDataInput)other).destination)
                    {
                        return true;
                    }
                }
                return false;
            }

            public override int GetHashCode()
            {
                return destination.GetHashCode();
            }

            public static FixDataInput getSample()
            {
                return new FixDataInput("emptyDestination", "emptyDescription", true);
            }

            public void apply(ProductInstance parent)
            {
                throw new NotImplementedException();
            }

            public override Property deepClone()
            {
                return new FixDataInput(this.destination, this.description)
                {
                    value = this.value,
                };
            }

            public override string ToString()
            {
                return string.Format("FixDataInput variable: {0} value {1}", destination, value);
            }

            public override void initUiElements()
            {
                uiElements = new ObservableCollection<UIElement>();

                Grid grid = new Grid()
                {
                    Name = "_grid",
                };

                //first column containing static label and description text
                grid.ColumnDefinitions.Add(new ColumnDefinition()
                {
                    Width = GridLength.Auto,
                });

                //second column containing destination and value
                grid.ColumnDefinitions.Add(new ColumnDefinition()
                {
                    Width = GridLength.Auto,
                });

                grid.RowDefinitions.Add(new RowDefinition()
                {
                    Height = GridLength.Auto,
                });

                grid.RowDefinitions.Add(new RowDefinition()
                {
                    Height = GridLength.Auto,
                });

                Label label = new Label()
                {
                    Name = "_label",
                    Content = "Input to FixData",
                    HorizontalAlignment = HorizontalAlignment.Left,
                };

                label.SetValue(Grid.RowProperty, 0);
                label.SetValue(Grid.ColumnProperty, 0);

                grid.Children.Add(label);

                Label box1 = new Label()
                {
                    Name = "_description",
                    HorizontalAlignment = HorizontalAlignment.Right,
                };

                box1.SetValue(Grid.RowProperty, 0);
                box1.SetValue(Grid.ColumnProperty, 1);

                Binding box1Binding = new Binding("description")
                {
                    Mode = BindingMode.OneWay,
                };
                box1.SetBinding(Label.ContentProperty, box1Binding);
                grid.Children.Add(box1);

                Label box2 = new Label()
                {
                    Name = "_destination",
                    HorizontalAlignment = HorizontalAlignment.Left,
                };

                box2.SetValue(Grid.RowProperty, 1);
                box2.SetValue(Grid.ColumnProperty, 0);

                Binding box2Binding = new Binding("destination")
                {
                    Mode = BindingMode.OneWay,
                };
                box2.SetBinding(Label.ContentProperty, box2Binding);
                grid.Children.Add(box2);

                TextBox box3 = new TextBox()
                {
                    Name = "_value",
                    IsReadOnly = false,
                    HorizontalAlignment = HorizontalAlignment.Right,
                };

                box3.SetValue(Grid.RowProperty, 1);
                box3.SetValue(Grid.ColumnProperty, 1);

                Binding box3Binding = new Binding("value")
                {
                    Mode = BindingMode.TwoWay,
                };
                box3.SetBinding(TextBox.TextProperty, box3Binding);
                grid.Children.Add(box3);

                uiElements.Add(new GroupBox()
                {
                    DataContext = this,
                    Content = grid,
                    Header = "Fix data " + description,
                    HorizontalAlignment = HorizontalAlignment.Stretch,
                });
            }

            public override void findAndReplace(string find, string replace)
            {
                value = value.Replace(find, replace);
            }
        }

        [DataContract]
        public class InstanceName : Property, IInput
        {
           
            [DataMember]
            public string value { get; set; } = "emptyValue";

            public override ObservableCollection<UIElement> uiElements { get; protected set; }
            public override string description { get; protected set; } = "emptyDescription";

            public InstanceName(bool isSample = false) : base("")
            {
                //value = loadValue();

                if (isSample == false)
                {
                    initUiElements();
                }           
            }

            public override bool Equals(Property other)
            {
                if (other.GetType() == this.GetType())
                {
                    return true;
                }
                return false;
            }

            public override int GetHashCode()
            {
                return this.GetType().GetHashCode();
            }

            public static InstanceName getSample()
            {
                return new InstanceName(true);
            }

            public void apply(ProductInstance parent)
            {
                throw new NotImplementedException();
            }

            public override Property deepClone()
            {
                return new InstanceName()
                {
                    value = this.value,
                };
            }

            public override void initUiElements()
            {
                uiElements = new ObservableCollection<UIElement>();

                Grid grid = new Grid()
                {
                    Name = "_grid",
                };

                //first column containing static label and description text
                grid.ColumnDefinitions.Add(new ColumnDefinition()
                {
                    Width = GridLength.Auto,
                });

                //second column containing destination and value
                grid.ColumnDefinitions.Add(new ColumnDefinition()
                {
                    Width = GridLength.Auto,
                });

                grid.RowDefinitions.Add(new RowDefinition()
                {
                    Height = GridLength.Auto,
                });

                grid.RowDefinitions.Add(new RowDefinition()
                {
                    Height = GridLength.Auto,
                });

                TextBox box3 = new TextBox()
                {
                    Name = "_value",
                    IsReadOnly = false,
                    HorizontalAlignment = HorizontalAlignment.Right,
                };

                box3.SetValue(Grid.RowProperty, 0);
                box3.SetValue(Grid.ColumnProperty, 0);

                Binding box3Binding = new Binding("value")
                {
                    Mode = BindingMode.TwoWay,
                };
                box3.SetBinding(TextBox.TextProperty, box3Binding);
                grid.Children.Add(box3);

                uiElements.Add(new GroupBox()
                {
                    DataContext = this,
                    Content = grid,
                    Header = "Name of the instance",
                    HorizontalAlignment = HorizontalAlignment.Stretch,
                });
            }

            public override void findAndReplace(string find, string replace)
            {
                value = value.Replace(find, replace);
            }
        }

        public interface IOutput
        {
            string value
            {
                get;
            }
        }

        public interface IInput
        {
            string value
            {
                set;
                get;
            }

            void apply(ProductInstance parent);
        }

        public abstract void findAndReplace(string find, string replace);
    }

    //[DataContract]
    public class PropertyCollection : HashSet<Property>, IDeepCloneable<PropertyCollection>
    {
        public void findAndReplace(string find, string replace)
        {
            foreach (Property property in this)
            {
                property.findAndReplace(find, replace);
            }
        }

        public static PropertyCollection getSample()
        {
            PropertyCollection collection = new PropertyCollection();
            collection.Add(Property.FixDataInput.getSample());
            collection.Add(Property.FixDataOutput.getSample());
            collection.Add(Property.InstanceName.getSample());
            collection.Add(Property.StaticInterlock.getSample());
            collection.Add(Property.DynamicInterlock.getSample());
            collection.Add(Property.DynamicInterlockException.getSample());
            collection.Add(Property.CommandProperty.getSample());

            return collection;
        }

        public List<Property> this[Type key]
        {
            get
            {
                List<Property> properties = new List<Property>();

                foreach (Property property in this)
                {
                    if (property.GetType() == key)
                    {
                        properties.Add(property);
                    }
                }

                return properties;
            }
        }

        public PropertyCollection deepClone()
        {
            PropertyCollection inst = new PropertyCollection();

            foreach (Property element in this)
            {
                inst.Add(element.deepClone());
            }

            return inst;
        }
    }
}
