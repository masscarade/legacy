﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

namespace codegen
{
    [DataContract]
    public class Rules
    {
        [DataMember]
        public List<SlotDefinition> slots { get; private set; }  = new List<SlotDefinition>();
        public static Rules getSample()
        {
            Rules inst = new Rules();
            inst.slots.Add(SlotDefinition.getSample());

            return inst; 
        }
    }

    public class SlotElement : List<ProductInstance>
    {
        [DataMember]
        public SlotDefinition definition { get; set; }

        public bool productDefinitionValid(ProductDefinition productDefinition)
        {
            return definition.productDefinitionValid(productDefinition);
        }

        public SlotElement(SlotDefinition definition)
        {
            this.definition = definition;
        }

        private SlotElement()
        {
        }

        public bool addAllowed
        {
            get
            {
                //always allow when there is no definition -> add used by datacontract!
                if (definition == null)
                {
                    return true;
                }

                return Count < definition.maxElements || definition.maxElements <= SlotDefinition.INFINITE_ELEMENTS;
            }
        }

        public new void Add(ProductInstance element)
        {
            if (addAllowed == true)
            {
                base.Add(element);
            }
            else
            {
                throw new SlotElementFullException(element);
            }
        }

        public class SlotElementFullException : Exception
        {
            private ProductInstance instance;

            public SlotElementFullException(ProductInstance instance)
            {
                this.instance = instance;
            }

            public override string Message
            {
                get
                {
                    return String.Format("Could not add {0}", instance);
                }
            }
        }
    }

    [DataContract]
    public class SlotDefinition : IEquatable<SlotDefinition>
    {
        [DataMember]
        public int maxElements { get; private set; }
        [DataMember]
        public List<Condition> conditions { get; private set; } = new List<Condition>();

        public const int INFINITE_ELEMENTS = 0;

        public bool productDefinitionValid(ProductDefinition productDefinition)
        {
            foreach (Condition condition in conditions)
            {
                if (condition.fulfillsCondition(productDefinition) == false)
                {
                    return false;
                }                
            }
            return true;
        }

        public override string ToString()
        {
            string str = "";

            foreach (Condition condition in conditions)
            {
                str += condition.ToString();
                str += " ";
            }

            return str;
        }

        public SlotDefinition(int maxElements = INFINITE_ELEMENTS)
        {
            this.maxElements = maxElements;
        }

        public SlotDefinition()
        {
            maxElements = INFINITE_ELEMENTS;
        }

        public static SlotDefinition getSample()
        {
            SlotDefinition SlotDefinition = new SlotDefinition();

            SlotDefinition.conditions.Add(Condition.ContainsTag.getSample());
            SlotDefinition.conditions.Add(Condition.ExcludesTag.getSample());
            SlotDefinition.conditions.Add(Condition.TypeEqual.getSample());
            SlotDefinition.conditions.Add(Condition.TypeUnequal.getSample());

            return SlotDefinition;
        }

        public bool Equals(SlotDefinition other)
        {
            return this.conditions.Equals(other.conditions);
        }
    }

    [DataContract]
    [KnownType(typeof(TypeEqual))]
    [KnownType(typeof(TypeUnequal))]
    [KnownType(typeof(ContainsTag))]
    [KnownType(typeof(ExcludesTag))]
    public abstract class Condition
    {
        public abstract bool fulfillsCondition(ProductDefinition inst);

        [DataContract]
        public class ContainsTag : Condition
        {
            [DataMember]
            public Tag tag { get; private set; }

            public ContainsTag(Tag tag)
            {
                this.tag = tag;
            }

            public static ContainsTag getSample()
            {
                return new ContainsTag(Tag.getSample())
                {
                };
            }

            public override string ToString()
            {
                return String.Format("== {0}",tag.ToString());
            }

            public override bool fulfillsCondition(ProductDefinition inst)
            {
                return inst.feature.tags.Contains(tag);
            }
        }

        [DataContract]
        public class ExcludesTag : Condition
        {
            [DataMember]
            public Tag tag { get; private set; }

            public ExcludesTag(Tag tag)
            {
                this.tag = tag;
            }

            public override string ToString()
            {
                return String.Format("!= {0}", tag.ToString());
            }

            public static ExcludesTag getSample()
            {
                return new ExcludesTag(Tag.getSample())
                {
                };
            }

            public override bool fulfillsCondition(ProductDefinition inst)
            {
                return !inst.feature.tags.Contains(tag);
            }
        }

        [DataContract]
        public class TypeEqual : Condition
        {
            [DataMember]
            public Feature.Type type { get; private set; }

            public TypeEqual(Feature.Type type)
            {
                this.type = type;
            }

            public override string ToString()
            {
                return String.Format("== {0}", type.ToString());
            }

            public override bool fulfillsCondition(ProductDefinition inst)
            {
                return inst.feature.type.Equals(type);
            }

            public static TypeEqual getSample()
            {
                return new TypeEqual(Feature.Type.getSample())
                {
                };
            }
        }


        [DataContract]
        public class TypeUnequal : Condition
        {
            [DataMember]
            public Feature.Type type { get; private set; }

            public TypeUnequal(Feature.Type type)
            {
                this.type = type;
            }

            public override string ToString()
            {
                return String.Format("!= {0}", type.ToString());
            }

            public override bool fulfillsCondition(ProductDefinition inst)
            {
                return !inst.feature.type.Equals(type);
            }

            public static TypeUnequal getSample()
            {
                return new TypeUnequal(Feature.Type.getSample())
                {
                };
            }
        }

    }
}
