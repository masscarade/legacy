﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

namespace codegen
{
    // test comment added 
    [DataContract]
    public class Feature
    {
        [DataMember]
        public Type type { get; private set; }

        /// <summary>
        /// used for filter, or search for products with tags
        /// </summary>
        [DataMember]
        public TagCollection tags { get; private set; }

        public static Feature getSample()
        {
            return new Feature()
            {
                type = Type.getSample(),
                tags = TagCollection.getSample(),
            };
        }

        public class Factory
        {
            public static Feature getSample()
            {
                return new Feature()
                {
                    tags = TagCollection.getSample(),
                    type = Type.getSample(),
                };
            }       
        }

        [DataContract]
        public class Type : IEquatable<Type>
        {
            [DataMember]
            public string name { get; private set; }

            public Type(string name)
            {
                this.name = name;
            }

            public override string ToString()
            {
                return name;
            }

            public override int GetHashCode()
            {
                return name.GetHashCode();
            }

            public bool Equals(Type other)
            {
                return this.name.ToLower().Equals(other.name.ToLower());
            }

            public static Type getSample()
            {
                return new Type("sample_type")
                {
                };
            }
        }
    }

    [DataContract]
    public class Tag : IEquatable<Tag>
    {
        [DataMember]
        public string name { get; private set; }

        public Tag(string name)
        {
            this.name = name;
        }

        public bool Equals(Tag other)
        {
            //ignoer case
            return this.name.ToLower().Equals(other.name.ToLower());
        }
        public override int GetHashCode()
        {
            return name.GetHashCode();
        }

        public static Tag getSample()
        {
            return new Tag("sample_tag")
            {
                //name = ,
            };
        }

        public override string ToString()
        {
            return name;
        }
    }

    public class TagCollection : HashSet<Tag>
    {
        public static TagCollection getSample()
        {
            TagCollection collection = new TagCollection();
            collection.Add(Tag.getSample());

            return collection;
        }
    }
}
