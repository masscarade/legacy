﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace project_creator
{
    [XmlRootAttribute("Testclass")]
    public class Testclass
    {
        public List<Baseclass> elements;

        public Testclass()
        {
            elements = new List<Baseclass>();
        }
    }

    [XmlInclude(typeof(Child1))]
    [XmlInclude(typeof(Child2))]
    public abstract class Baseclass
    {
        public abstract void output();
    }

    [XmlRootAttribute("Child1")]
    public class Child1 : Baseclass
    {
        public string text = "child1";
        public Child1()
        {
            
        }

        public override void output()
        {
            Console.Out.WriteLine("child1");
        }
    }

    [XmlRootAttribute("Child2")]
    public class Child2 : Baseclass
    {
        public Child2()
        {
            
            
        }

        public override void output()
        {
            Console.Out.WriteLine("child2");
        }
    }

    class Program
    {

        static void Main(string[] args)
        {
           /* List<ProjectFeature> features = new List<ProjectFeature>();

            features.Add(FeatureFactory<CorePuller>.produceFeature());
            features.Add(FeatureFactory<TopSeparator>.produceFeature());
            features.Add(FeatureFactory<CorePuller>.produceFeature());
            features.Add(FeatureFactory<CorePuller>.produceFeature());
            features.Add(FeatureFactory<CorePuller>.produceFeature());
            features.Add(FeatureFactory<TopSeparator>.produceFeature());
            features.Add(FeatureFactory<TopSeparator>.produceFeature());
            features.Add(FeatureFactory<CorePuller>.produceFeature());*/

            XmlSerializer serializer = new XmlSerializer(typeof(Testclass));
            TextWriter writer = new StreamWriter("test.xml");
           

            Testclass test = new Testclass();

            test.elements.Add(new Child1());
            test.elements.Add(new Child1());
            test.elements.Add(new Child2());
            test.elements.Add(new Child1());
            test.elements.Add(new Child1());

            

            Project project = new Project();

            serializer.Serialize(writer, test);
            writer.Close();

            FileStream fs = new FileStream("test.xml", FileMode.Open);
            Testclass readTest = (Testclass)serializer.Deserialize(fs);

            foreach (Baseclass i in readTest.elements)
            {
                i.output();
            }
        }

        class Project
        {
            private List<ProjectFeature> features;

            abstract class ProjectFeature
            {
                private string featureName;
                private List<ConfigurationObject> configurationObjects;

                public abstract void initialize(int index);

                private ProjectFeature()  {}

                [XmlInclude(typeof(OPC_UA))]
                [XmlInclude(typeof(Task))]
                [XmlInclude(typeof(Alarms))]
                [XmlInclude(typeof(Commands))]
                [XmlInclude(typeof(Actuator))]
                [XmlInclude(typeof(Axis))]
                abstract class ConfigurationObject
                {
                    [XmlRootAttribute("OPC_UA")]
                    class OPC_UA : ConfigurationObject
                    {
                    }

                    [XmlRootAttribute("Task")]
                    class Task : ConfigurationObject
                    {
                    }

                    [XmlRootAttribute("Alarms")]
                    class Alarms : ConfigurationObject
                    {
                    }

                    [XmlRootAttribute("Commands")]
                    class Commands : ConfigurationObject
                    {
                    }

                    [XmlRootAttribute("Actuator")]
                    class Actuator : ConfigurationObject
                    {
                    }

                    [XmlRootAttribute("Axis")]
                    class Axis : ConfigurationObject
                    {
                    }

                }

                static class FeatureFactory<TFeature> where TFeature : ProjectFeature, new()
                {
                    private static int objCnt = 0;

                    static public TFeature produceFeature()
                    {

                        TFeature newFeature = new TFeature();
                        newFeature.initialize(++objCnt);
                        return newFeature;
                    }
                }
                class CorePuller : ProjectFeature
                {

                    public override void initialize(int index)
                    {
                        featureName = "Core" + index.ToString();
                    }

                    public CorePuller()
                        : base()
                    {

                    }
                }

                class TopSeparator : ProjectFeature
                {

                    public override void initialize(int index)
                    {
                        featureName = "TopSep" + index.ToString();
                    }

                    public TopSeparator()
                        : base()
                    {

                    }
                }
            }                      
        } 
    }
}
