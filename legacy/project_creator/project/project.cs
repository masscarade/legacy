﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text.RegularExpressions;
using codegenLog;
using package;

namespace project_creator
{
    static partial class Project
    {
        //list of features in the project i.e. 2 * Corepuller, 1 * Clamp,... 
        private static List<ProjectFeature> features = new List<ProjectFeature>();

        private static sw_config.SwConfiguration swConfiguration;

        private static Settings currentSettings;

        public static string orderID { get; private set; }

        //represents the vis file
        private static mv_vis.Visualization vis;

        private static textsystem.Configuration textConfig;

        //path of the folder where the *.apj file of the destianation project is 
        public static string dest { get; private set; }
        public static string originalProject { get; private set; }
        //configuration for the machine 
        private static string config;
        //source dir of products
        private static string productSrc;

        static private mapp_meta.MpAlarm alarmConfiguration;
        static private XDocument ncMapping;
        static private List<mapp_meta.MpSequencer> sequencerConfiguration = new List<mapp_meta.MpSequencer>();
        static private mapp_meta.MpCodebox codeboxConfiguration;
        static private mapp_meta.MpGroup groupConfiguration;

        static private void setupConfig(Settings settings)
        {
            //apply wildcard to new config name
            if (orderID != null)
            {
                settings.destConfig.newName.Replace(ProjectFeature.WildcardMethods.ORDER_ID, orderID);
            }

            DirectoryInfo srcProject = new DirectoryInfo(settings.destProject);

            //copy config
            DirectoryInfo srcConfigDir = new DirectoryInfo(Project.dest + @"\Physical\" + settings.destConfig.srcConfig);
            string destPath = Project.dest + @"\Physical\" + settings.destConfig.newName;

            if (Directory.Exists(destPath) == true)
            {
                Directory.Delete(destPath, true);
            }

            Methods.CloneDirectory(srcConfigDir.FullName, destPath);

            /*add new config to pkg file*/
            /*add folder for the feature type if not existing*/
            string pkgDir = Directory.GetFiles(Project.dest + @"\Physical\", "*.pkg", SearchOption.TopDirectoryOnly)[0];
            Package pkg = Package.load(pkgDir);

            pkg.addElement(Package.OBJECTS_GROUP, settings.destConfig.newName, new XAttribute[]
                               {
                                    new XAttribute(Package.TYPE_ATTRIBUTE, Package.TYPE_ATTRIBUTE_CONFIGURATION),
                                    new XAttribute(Package.DESCRIPTION_ATTRIBUTE, "Auto generated"),
                               });

            pkg.save(pkgDir);

            foreach (Settings.DestConfig.References.Reference reference in settings.destConfig.references)
            {
                reference.configure(settings);
            }
        }

        static Project() { }

        static public void generate(Settings settings)
        {
            Project.dest = settings.destProject;
            Project.originalProject = Project.dest;
            currentSettings = settings;

            /*copy the project if definition exists*/
            if (settings.copyProject != null)
            {
                DirectoryInfo dir = new DirectoryInfo(settings.destProject);
                string dest = dir.Parent.FullName + @"/" + settings.copyProject.name;                

                if (Directory.Exists(dest) == true)
                {
                    Logging.log(string.Format("target directory {0} already exists", dest), LEVEL.INFO);

                    Methods.DeleteDirecotry(dest);
                }

                Methods.CloneDirectory(settings.destProject, dest);
                Project.dest = dest;              
            }

            setupConfig(settings);

            //run replace patterns after copying project and references
            //run replace pattern if existing
            if (settings.replacePatterns != null)
            {
                foreach (Settings.ReplacePattern pattern in settings.replacePatterns)
                {
                    Logging.enterContext(pattern);

                    string[] files = Directory.GetFiles(Project.dest + pattern.directory, pattern.filePattern, pattern.searchOption);

                    foreach (string file in files)
                    {
                        Logging.enterContext(file);

                        //set attribute always to normal in order to guarantee access
                        File.SetAttributes(file, FileAttributes.Normal);

                        string text = File.ReadAllText(file);

                        Logging.log(string.Format("find: {0} replace: {1} in: {2}", pattern.find, pattern.replace, file), LEVEL.INFO);
                        text = Regex.Replace(text, pattern.find, pattern.replace);
                        File.WriteAllText(file, text);

                        Logging.leaveContext();
                    }

                    Logging.leaveContext();
                }
            }
            
            Project.config = settings.destConfig.newName;

            //use first found sw config
            string swConfigPath = Directory.GetFiles(dest + @"\Physical\" + config, @"Cpu.sw", SearchOption.AllDirectories)[0];

            Logging.log(string.Format("loead sw configuration from {0}", swConfigPath), LEVEL.INFO);

            //get original sw config
            swConfiguration = sw_config.SwConfiguration.load(swConfigPath);

            string visPath = null;

            //use first found vis file
            string[] visPaths = Directory.GetFiles(dest + @"\Physical\" + config, @"*.vis", SearchOption.AllDirectories);

            if (visPaths.Length == 0)
            {
                Logging.log(string.Format("no vis file found, generating mappView will be skipped"), LEVEL.WARNING);
            }
            else if (visPaths.Length > 1)
            {
                visPath = visPaths[0];
                Logging.log(string.Format("multiple vis files found, use first found path {0}", visPath), LEVEL.WARNING);
            }
            else
            {
                visPath = visPaths[0];
                Logging.log(string.Format("load vis file from {0}", visPath), LEVEL.INFO);
            }

            if (visPath != null)
            {
                vis = mv_vis.Visualization.load(visPath);
            }

            //use first found vis file
            string textconfigPath = null;
            try
            {
                textconfigPath = Directory.GetFiles(dest + @"\Physical\" + config, @"*.textconfig", SearchOption.AllDirectories)[0];

                Logging.log(string.Format("load text config from {0}", textconfigPath), LEVEL.INFO);

                textConfig = textsystem.Configuration.load(textconfigPath);
            }
            catch (Exception e)
            {
                textconfigPath = null;
            }
            //get original vis

            string alarmConfigPath = null;
            try
            {
                alarmConfigPath = Directory.GetFiles(dest + @"\Physical\" + config, "*" + ProjectFeature.ConfigurationObject.Mapp.ALARM, SearchOption.AllDirectories)[0];

                Logging.log(string.Format("load mapp alarm config from {0}", alarmConfigPath), LEVEL.INFO);

                alarmConfiguration = mapp_meta.MpAlarm.load(alarmConfigPath);
            }
            catch (Exception e)
            {
                alarmConfiguration = null;
            }

            string ncmPath = null;
            try
            {
                ncmPath = Directory.GetFiles(dest + @"\Physical\" + config, "*" + ProjectFeature.ConfigurationObject.Mapp.NCM, SearchOption.AllDirectories)[0];

                Logging.log(string.Format("load nc mapping from {0}", ncmPath), LEVEL.INFO);

                using (TextReader reader = File.OpenText(ncmPath))
                {
                    ncMapping = XDocument.Load(reader);
                }                  
            }
            catch (Exception e)
            {
                ncMapping = null;
            }

            string[] sequencerConfigPath = null;
            try
            {
                sequencerConfigPath = Directory.GetFiles(dest + @"\Physical\" + config, "*" + ProjectFeature.ConfigurationObject.Mapp.SEQUENCER, SearchOption.AllDirectories);

                Logging.log(string.Format("load mapp sequencer config from {0}", sequencerConfigPath), LEVEL.INFO);

                sequencerConfigPath.ToList().ForEach(i => sequencerConfiguration.Add(mapp_meta.MpSequencer.load(i)));
            }
            catch (Exception e)
            {
                sequencerConfiguration = null;
            }

            string codeboxConfigPath = null;
            try
            {
                codeboxConfigPath = Directory.GetFiles(dest + @"\Physical\" + config, "*" + ProjectFeature.ConfigurationObject.Mapp.CODEBOX, SearchOption.AllDirectories)[0];

                Logging.log(string.Format("load mapp codebox config from {0}", codeboxConfigPath), LEVEL.INFO);

                codeboxConfiguration = mapp_meta.MpCodebox.load(codeboxConfigPath);
            }
            catch (Exception e)
            {
                codeboxConfiguration = null;
            }

            string groupConfigPath = null;
            try
            {
                groupConfigPath = Directory.GetFiles(dest + @"\Physical\" + config, "*" + ProjectFeature.ConfigurationObject.Mapp.GROUP, SearchOption.AllDirectories)[0];

                Logging.log(string.Format("load mapp group config from {0}", groupConfigPath), LEVEL.INFO);

                groupConfiguration = mapp_meta.MpGroup.load(groupConfigPath);
            }
            catch
            {
                groupConfiguration = null;
            }

            foreach (ProjectFeature feature in features)
            {
                Logging.enterContext(feature);
                feature.configure();
                Logging.leaveContext();
            }

            swConfiguration.save(swConfigPath);

            if (settings.destConfig.InitialContentLoad == true)
            {
                //vis.addLoadContentEventbinding(new FileInfo(visPath).DirectoryName);
            }

            if (visPath != null)
            {
                vis.save(visPath);
            }

            if (textconfigPath != null)
            {
                textConfig.save(textconfigPath);
            }

            if (alarmConfigPath != null)
            {
                alarmConfiguration.save(alarmConfigPath);
            }        
            //is suppoesed to be done by the axis themself
            //sequencerConfiguration.generateStaticInterlocks(settings.destConfig.staticInterlockMatrix);   

            if (sequencerConfigPath != null && sequencerConfiguration != null)
            {
                sequencerConfiguration.ForEach(i => i.save());

                foreach (string str in sequencerConfigPath)
                {
                    mapp_meta.MpSequencer.GenerateDynamicInterlocks(str, (Settings.DestConfig.DynamicInterlockMatrix.AllAxes)
                        settings.destConfig.dynamicInterlockMatrix);
                }
            }

            if (codeboxConfiguration != null)
            {
                codeboxConfiguration.save(codeboxConfigPath);
            }

            if (groupConfigPath != null)
            {
                groupConfiguration.save(groupConfigPath);
            }

            if (ncmPath != null)
            {
                ncMapping.Save(ncmPath);
            }

            /*remove ignores after generating*/
            if (settings.copyProject != null && settings.copyProject.exclude != null)
            {
                /*remove tech packages*/
                if (settings.copyProject.exclude.technologyPackages != null)
                {
                    //get apj file
                    string apjFile = Directory.GetFiles(dest, "*.apj")[0];

                    XDocument xdoc = XDocument.Load(apjFile);

                    foreach (string inst in settings.copyProject.exclude.technologyPackages.ignores)
                    {
                        var elements = xdoc.Descendants();

                        XElement xElement = null;

                        foreach (var xel in elements)
                        {
                            if (xel.Name.LocalName == inst)
                            {
                                xElement = xel;
                                break;
                            }
                        }

                        if (xElement == null)
                        {
                            Logging.log(string.Format("technology package {0} not found in project", inst), LEVEL.WARNING);
                        }
                        else
                        {
                            xElement.Remove();
                            Logging.log(string.Format("removed technology package {0}", inst), LEVEL.INFO);
                        }
                    }

                    xdoc.Save(apjFile);
                }
                /*remove configs*/
                if (settings.copyProject.exclude.physical != null)
                {
                    /*add new config to pkg file*/
                    /*add folder for the feature type if not existing*/
                    string pkgDir = Directory.GetFiles(dest + @"\Physical\", "*.pkg", SearchOption.TopDirectoryOnly)[0];
                    Package pkg = Package.load(pkgDir);             
                    
                    foreach (string ignore in settings.copyProject.exclude.physical.ignores)
                    {
                        Logging.log(string.Format("remove {0} from the project", ignore), LEVEL.INFO);

                        string dirName = dest + @"\Physical\" + ignore;

                        if (Directory.Exists(dirName) == true)
                        {
                            Logging.log(string.Format("remove {0} from the project", dirName), LEVEL.INFO);
                            Directory.Delete(dirName, true);

                            XElement elements = pkg.packageGroups[Package.OBJECTS_GROUP];

                            foreach (XElement obj in elements.Descendants())
                            {
                                if (obj.Value == ignore)
                                {
                                    Logging.log(string.Format("remove {0} from the project", obj), LEVEL.INFO);
                                    obj.Remove();
                                    break;
                                }
                            }
                        }
                    }

                    pkg.save(pkgDir);
                }

                /*remove parts of cpu*/
                if (settings.copyProject.exclude.cpuObject != null)
                {
                    foreach (string ignore in settings.copyProject.exclude.cpuObject.ignores)
                    {
                        Logging.log(string.Format("remove {0} from the project", ignore), LEVEL.INFO);

                        string path = dest + @"\Physical\" + settings.destConfig.newName + @"\";

                        path = Directory.GetDirectories(path)[0];

                        path += (@"\" + ignore);
                            
                            //ignore;

                        //delete a directory
                        if (Directory.Exists(path))
                        {
                            DirectoryInfo info = new DirectoryInfo(path);

                            string pkgDir = Directory.GetFiles(info.Parent.FullName, "*.pkg", SearchOption.TopDirectoryOnly)[0];
                            package.Package pkg = package.Package.load(pkgDir);

                            /*avoid exception for read only files*/
                            foreach (FileInfo item in info.GetFiles("*", SearchOption.AllDirectories))
                            {
                                File.SetAttributes(item.FullName, FileAttributes.Normal);
                            }

                            Logging.log(string.Format("remove {0} from the project", info.FullName), LEVEL.INFO);

                            info.Delete(true);

                            XContainer elements = pkg.packageGroups[Package.OBJECTS_GROUP];

                            foreach (XElement obj in elements.Descendants())
                            {
                                if (obj.Value == info.Name)
                                {
                                    Logging.log(string.Format("remove {0} from the project", obj), LEVEL.INFO);
                                    obj.Remove();
                                    break;
                                }
                            }

                            pkg.save(pkgDir);
                        }
                        //its a file otherwise
                        else if (File.Exists(path))
                        {
                            FileInfo info = new FileInfo(path);

                            string pkgDir = Directory.GetFiles(info.DirectoryName, "*.pkg", SearchOption.TopDirectoryOnly)[0];
                            package.Package pkg = package.Package.load(pkgDir);

                            Logging.log(string.Format("remove {0} from the project", info.FullName), LEVEL.INFO);

                            info.Delete();

                            XContainer elements = pkg.packageGroups[Package.OBJECTS_GROUP];

                            foreach (XElement obj in elements.DescendantNodes())
                            {
                                if (obj.Value == info.Name)
                                {
                                    Logging.log(string.Format("remove {0} from the project", obj), LEVEL.INFO);
                                    obj.Remove();
                                    break;
                                }
                            }

                            pkg.save(pkgDir);
                        }
                    }
                }

                /*remove generic parts*/
                if (settings.copyProject.exclude.generic != null)
                {
                    foreach (string ignore in settings.copyProject.exclude.generic.ignores)
                    {
                        Logging.log(string.Format("remove {0} from the project", ignore), LEVEL.INFO);

                        string path = dest + @"\" + ignore;

                        //delete a directory
                        if (Directory.Exists(path))
                        {
                            DirectoryInfo info = new DirectoryInfo(path);

                            string pkgDir = Directory.GetFiles(info.Parent.FullName, "*.pkg", SearchOption.TopDirectoryOnly)[0];
                            package.Package pkg = package.Package.load(pkgDir);

                            /*avoid exception for read only files*/
                            foreach (FileInfo item in info.GetFiles("*", SearchOption.AllDirectories))
                            {
                                File.SetAttributes(item.FullName, FileAttributes.Normal);
                            }

                            Logging.log(string.Format("remove {0} from the project", info.FullName), LEVEL.INFO);

                            info.Delete(true);

                            XContainer elements = pkg.packageGroups[Package.OBJECTS_GROUP];

                            foreach (XElement obj in elements.Descendants())
                            {
                                if (obj.Value == info.Name)
                                {
                                    Logging.log(string.Format("remove {0} from the project", obj), LEVEL.INFO);
                                    obj.Remove();
                                    break;
                                }
                            }

                            pkg.save(pkgDir);
                        }
                        //its a file otherwise
                        else if (File.Exists(path))
                        {
                            FileInfo info = new FileInfo(path);

                            string pkgDir = Directory.GetFiles(info.DirectoryName, "*.pkg", SearchOption.TopDirectoryOnly)[0];
                            package.Package pkg = package.Package.load(pkgDir);

                            Logging.log(string.Format("remove {0} from the project", info.FullName), LEVEL.INFO);

                            info.Delete();

                            XContainer elements = pkg.packageGroups[Package.OBJECTS_GROUP];

                            foreach (XElement obj in elements.DescendantNodes())
                            {
                                if (obj.Value == info.Name)
                                {
                                    Logging.log(string.Format("remove {0} from the project", obj), LEVEL.INFO);
                                    obj.Remove();
                                    break;
                                }
                            }

                            pkg.save(pkgDir);
                        }
                    }
                }

                /*remove parts of logical*/
                if (settings.copyProject.exclude.logical != null)
                {                    
                    foreach (string ignore in settings.copyProject.exclude.logical.ignores)
                    {
                        Logging.log(string.Format("remove {0} from the project", ignore), LEVEL.INFO);

                        string path = dest + @"\Logical\" + ignore;

                        //delete a directory
                        if (Directory.Exists(path))
                        {
                            DirectoryInfo info = new DirectoryInfo(path);

                            string pkgDir = Directory.GetFiles(info.Parent.FullName, "*.pkg", SearchOption.TopDirectoryOnly)[0];
                            package.Package pkg = package.Package.load(pkgDir);

                            /*avoid exception for read only files*/
                            foreach (FileInfo item in info.GetFiles("*", SearchOption.AllDirectories))
                            {
                                File.SetAttributes(item.FullName, FileAttributes.Normal);
                            }

                            Logging.log(string.Format("remove {0} from the project", info.FullName), LEVEL.INFO);

                            info.Delete(true);

                            XContainer elements = pkg.packageGroups[Package.OBJECTS_GROUP];

                            foreach (XElement obj in elements.Descendants())
                            {
                                if (obj.Value == info.Name)
                                {
                                    Logging.log(string.Format("remove {0} from the project", obj), LEVEL.INFO);
                                    obj.Remove();
                                    break;
                                }
                            }

                            pkg.save(pkgDir);
                        }
                        //its a file otherwise
                        else if (File.Exists(path))
                        {
                            FileInfo info = new FileInfo(path);

                            string pkgDir = Directory.GetFiles(info.DirectoryName, "*.pkg", SearchOption.TopDirectoryOnly)[0];
                            package.Package pkg = package.Package.load(pkgDir);

                            Logging.log(string.Format("remove {0} from the project", info.FullName), LEVEL.INFO);

                            info.Delete();

                            XContainer elements = pkg.packageGroups[Package.OBJECTS_GROUP];

                            foreach (XElement obj in elements.DescendantNodes())
                            {
                                if (obj.Value == info.Name)
                                {
                                    Logging.log(string.Format("remove {0} from the project", obj), LEVEL.INFO);
                                    obj.Remove();
                                    break;
                                }
                            }

                            pkg.save(pkgDir);
                        }                                            
                    }                   
                }
            }
        }

        static public void import(codegen.Order order, string productSrc)
        {
            Project.productSrc = productSrc;
            orderID = order.id.name;


            import(order.products, productSrc);
        }

        static public void import(codegen.ProductInstance inst, string productSrc)
        {
            Project.productSrc = productSrc;

            foreach (codegen.SlotElement slot in inst.slots)
            {
                import(slot, productSrc);
            }

            import(inst.definition.id.name, productSrc, inst.properties);
        }

        public static void import(ICollection<codegen.ProductInstance> instances, string productSrc)
        {
            foreach (codegen.ProductInstance inst in instances)
            {
                import(inst, productSrc);
            }
        }

        static public void import(codegen.ProductInstanceDictionary src, string productSrc)
        {
            Project.productSrc = productSrc;

            foreach (KeyValuePair<codegen.ProductInstance.ID, codegen.ProductInstance> set in src)
            {
                import(set.Value, productSrc);
            }
        }

        static public void import(string product, string productSrc, codegen.PropertyCollection porperties = null)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ProjectFeature));

            string[] paths = Directory.GetDirectories(productSrc, product, SearchOption.AllDirectories);

            string path = paths[0] + @"\Feature.xml";
            FileStream fs = new FileStream(path, FileMode.Open);

            ProjectFeature newFeature = (ProjectFeature)serializer.Deserialize(fs);
            fs.Close();

            //abstract features must not be instanciated
            if (newFeature.isAbstract == true)
            {
                Console.WriteLine(String.Format("Product {0} is abstract; skipped!", product));
                return;
            }

            //add objects of parents to new feature;
            foreach (string parent in newFeature.parents)
            {
                using (FileStream fsParent = new FileStream(Directory.GetDirectories(productSrc, parent, SearchOption.AllDirectories)[0] + @"\Feature.xml", FileMode.Open))
                {
                    ProjectFeature parentFeature = (ProjectFeature)serializer.Deserialize(fsParent);
                    newFeature.configurationObjects.AddRange(parentFeature.configurationObjects);
                }
            }

            newFeature.init(ID: product, porperties: porperties);
            features.Add(newFeature);
        }

        static public void import(StreamReader src, string productSrc)
        {
            string product;
            Project.productSrc = productSrc;

            while ((product = src.ReadLine()) != null)
            {
                import(product, productSrc);
            }
        }
    }
}