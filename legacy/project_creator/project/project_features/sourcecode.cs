﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text.RegularExpressions;

namespace project_creator
{
    partial class Project
    {
        public partial class ProjectFeature
        {
            public abstract partial class ConfigurationObject
            {
                [XmlRootAttribute("Sourcecode")]
                public abstract class Sourcecode : ConfigurationObject
                {
                    public List<Variable> variables;
                    public string task;

                    //list of config objects for the as project, such as OPC UA map, tasks,...
                    [XmlArray("ReplacePatterns")]
                    [XmlArrayItem("Pattern")]
                    public List<ReplacePattern> replacePatterns = new List<ReplacePattern>();

                    public class Variable
                    {
                        public string name;
                        public string value;
                        public string type;
                        public string comment;

                    }
                    public abstract class Target
                    {
                    }
                  
                    public Sourcecode()
                        : base()
                    {

                    }

                    public class C : Sourcecode
                    {
                        public List<CodeSnippet> code;

                        public class CodeSnippet
                        {
                            public string targetFunction;
                            public string code;
                        }


                        public static ConfigurationObject generateSample()
                        {

                            return (ConfigurationObject)new C()
                            {
                                taskIdentifier = "Axis",
                                task = "FeatureMan",
                                variables = new List<Variable>()
                                {
                                    new Variable() { name = "varName1", comment = "comment1 #ProductID", type ="varType1", value = "varValue1"},
                                    new Variable() { name = "varName2", comment = "comment2 #ProductID", type ="varType2", value = "varValue2"},
                                },
                                code = new List<CodeSnippet>()
                                {
                                    new CodeSnippet() { targetFunction = "void _CYCLIC ProgramCyclic(void)", code = "Axis.Enable = 1;FeatureManager(&Axis);"},
                                    new CodeSnippet() { targetFunction = "void _EXIT ProgramExit(void)", code = "DisableFeatureMan(&Axis);"},
                                },
                            };
                        }
                        public override void configure(ProjectFeature feature)
                        {
                            if (conditionsTrue(feature) == false)
                            {
                                return;
                            }

                            foreach (KeyValuePair<string, WildcardMethods.WildcardDelegate> pair in ProjectFeature.wildcards)
                            {
                                string replac = wildcardOverrides != null ? wildcardOverrides[pair.Key] : pair.Key;                                
                                task = Regex.Replace(task, replac, pair.Value(feature));
                            }

                            /*find task*/
                            DirectoryInfo destDir = new DirectoryInfo(dest + @"/" + task);

                            //add variables
                            string varCode = File.ReadAllText(destDir.GetFiles("*.var")[0].FullName);

                            if (this.variables != null)
                            {
                                using (StreamWriter writer = new StreamWriter(destDir.GetFiles("*.var")[0].FullName, true))
                                {
                                    writer.WriteLine("");
                                    writer.WriteLine("(*auto generated for" + feature.productID + " " + feature.getName() + "*)");
                                    writer.WriteLine("VAR");

                                    foreach (Variable var in this.variables)
                                    {
                                        string line = "\t" + var.name + " : " + var.type + " := " + var.value + ";(*" + var.comment + "*)";

                                        foreach (KeyValuePair<string, WildcardMethods.WildcardDelegate> pair in ProjectFeature.wildcards)
                                        {
                                            string replac = wildcardOverrides != null ? wildcardOverrides[pair.Key] : pair.Key;                                            
                                            line = Regex.Replace(line, replac, pair.Value(feature));
                                        }

                                        foreach (ReplacePattern pattern in replacePatterns)
                                        {
                                            line = Regex.Replace(line, pattern.find, pattern.replace);
                                        }

                                        line = line.Replace(this.taskIdentifier, feature.getName());

                                        writer.WriteLine(line);
                                    }

                                    writer.WriteLine("END_VAR");
                                }
                            }

                            /*write code*/

                            foreach (CodeSnippet snippet in code)
                            {
                                string line = snippet.code;

                                foreach (KeyValuePair<string, WildcardMethods.WildcardDelegate> pair in ProjectFeature.wildcards)
                                {
                                    string replac = wildcardOverrides != null ? wildcardOverrides[pair.Key] : pair.Key;                                    
                                    line = Regex.Replace(line, replac, pair.Value(feature));
                                }

                                foreach (ReplacePattern pattern in replacePatterns)
                                {
                                    line = Regex.Replace(line, pattern.find, pattern.replace);
                                }

                                line = line.Replace(this.taskIdentifier, feature.getName());

                                //find function
                                foreach (FileInfo file in destDir.GetFiles("*.c"))
                                {
                                    string code = File.ReadAllText(file.FullName);

                                    if (code.Contains(snippet.targetFunction))
                                    {
                                        string input = snippet.targetFunction.Replace("(", @"\(");
                                        input = input.Replace(")", @"\)");

                                        Regex rgx = new Regex(input);// + @"(\s *?).");

                                        Match match = rgx.Match(code);
                                        code = rgx.Replace(code, snippet.targetFunction + "\n" + line + "\n");
                                        File.WriteAllText(file.FullName, code);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}