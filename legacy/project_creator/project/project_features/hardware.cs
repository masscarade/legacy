﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.IO;
using System.Text.RegularExpressions;

namespace project_creator
{
    partial class Project
    {     
        public partial class ProjectFeature
        {
            public abstract partial class ConfigurationObject
            {
                [XmlRootAttribute("HardwareModule")]
                public class HardwareModule : ConfigurationObject
                {
                    [XmlAttribute("Source")]
                    public string source;

                    [XmlAttribute("Name")]
                    public string name;

                    [XmlAttribute("TargetModule")]
                    public string targetModule;

                    [XmlAttribute("TargetConnector")]
                    public string targetConnector;

                    [XmlAttribute("NamingPattern")]
                    public string namingPattern;

                    [XmlArray("ReplacePatterns")]
                    [XmlArrayItem("Pattern")]
                    public List<ReplacePattern> replacePatterns = new List<ReplacePattern>();

                    public static ConfigurationObject getSample()
                    {
                        return new HardwareModule()
                        {
                            name = "ACOPOS_P3_Name",
                            targetConnector = "SS1",
                            targetModule = "Name_of_PLK_IF",
                        };
                    }


                    public override void configure(ProjectFeature feature)
                    {

                        if (conditionsTrue(feature) == false)
                        {
                            return;
                        }

                        string name = feature.getName();

                        foreach (KeyValuePair<string, WildcardMethods.WildcardDelegate> pair in ProjectFeature.wildcards)
                        {
                            string replac = wildcardOverrides != null ? wildcardOverrides[pair.Key] : pair.Key;
                            name = Regex.Replace(name, replac, pair.Value(feature));
                        }

                        foreach (ReplacePattern pattern in replacePatterns)
                        {
                            name = Regex.Replace(name, pattern.find, pattern.replace);
                        }

                        if (taskIdentifier != null)
                        {
                            name = name.Replace(this.taskIdentifier, feature.getName());
                        }

                        if (namingPattern != null && namingPattern != "")
                        {
                            name = namingPattern;

                            foreach (KeyValuePair<string, WildcardMethods.WildcardDelegate> pair in ProjectFeature.wildcards)
                            {
                                string replac = wildcardOverrides != null ? wildcardOverrides[pair.Key] : pair.Key;
                                name = Regex.Replace(name, replac, pair.Value(feature));
                            }
                        }

                        XElement sourceElement;

                        //get source element
                        using (TextReader reader = File.OpenText(source))
                        {
                            XDocument xml = XDocument.Load(reader);

                            sourceElement = xml.Descendants()
                                            .Where(x => (string)x.Attribute("Name") == name)
                                            .FirstOrDefault();
                        }
                    }

                    public HardwareModule()
                        : base()
                    {
                    }
                }
            }
        }
    }
}