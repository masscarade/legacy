﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using codegen;
using codegenLog;
using package;

namespace project_creator
{
    partial class Project
    {
        public partial class ProjectFeature
        {
            public abstract partial class ConfigurationObject
            {
                public class ExcludeOverride : ConfigurationObject
                {
                    public Settings.CopyProject.Exclude overrides;
                    public override void configure(ProjectFeature feature)
                    {
                        if (conditionsTrue(feature) == false)
                        {
                            return;
                        }

                        if (overrides == null)
                        {
                            return;
                        }

                        currentSettings.copyProject.exclude.overrideExcludes(overrides);                      
                    }

                    public ExcludeOverride()
                        : base()
                    {

                    }                              
                }
            }
        }
    }
}