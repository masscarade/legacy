﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text.RegularExpressions;
using package;
using System.Xml.Linq;

namespace project_creator
{
    partial class Project
    {
        public partial class ProjectFeature
        {
            public abstract partial class ConfigurationObject
            {
                [XmlRootAttribute("OPC_UA")]
                public class OPC_UA : ConfigurationObject
                {

                    [XmlArray("ReplacePatterns")]
                    [XmlArrayItem("Pattern")]
                    public List<ReplacePattern> replacePatterns = new List<ReplacePattern>();

                    [XmlAttribute]
                    public string name;

                    public override void configure(ProjectFeature feature)
                    {
                        if (conditionsTrue(feature) == false)
                        {
                            return;
                        }

                        //use first found opc ua dir
                        string opcDir = Directory.GetDirectories(dest + @"\Physical\" + config, @"OpcUA", SearchOption.AllDirectories)[0];
                        //use first found package file
                        string pkgPath = Directory.GetFiles(opcDir, "*.pkg", SearchOption.AllDirectories)[0];

                        package.Package pkgFile = package.Package.load(pkgPath); 

                        string opcName = "";
                        string taskname = feature.componentName;

                        if (name == "" || name == null)
                        {
                            opcName = "o" + taskname + ".uad";
                        }
                        else
                        {
                            opcName = name;

                            foreach (KeyValuePair<string, WildcardMethods.WildcardDelegate> pair in ProjectFeature.wildcards)
                            {
                                string replac = wildcardOverrides != null ? wildcardOverrides[pair.Key] : pair.Key;                                
                                opcName = Regex.Replace(opcName, replac, pair.Value(feature));
                            }

                            foreach (ReplacePattern pattern in replacePatterns)
                            {
                                opcName = Regex.Replace(opcName, pattern.find, pattern.replace);
                            }

                            taskname = opcName;
                            opcName = "o" + taskname + ".uad";
                        }

                        pkgFile.addElement(Package.OBJECTS_GROUP, opcName, new XAttribute[]
                              {
                                    new XAttribute(Package.TYPE_ATTRIBUTE, Package.TYPE_ATTRIBUTE_FILE),
                                    new XAttribute(Package.DESCRIPTION_ATTRIBUTE, feature.getID()),
                              });



                        pkgFile.save(pkgPath);

                        string[] paths = Directory.GetDirectories(productSrc + @"\", feature.getID(), SearchOption.AllDirectories);
                        string path = paths[0];

                        string srcPath = Directory.GetFiles(dest + @"\Physical\", "o" + this.taskIdentifier + ".uad", SearchOption.AllDirectories)[0];

                        String srcData = File.ReadAllText(srcPath);

                        foreach (KeyValuePair<string, WildcardMethods.WildcardDelegate> pair in ProjectFeature.wildcards)
                        {
                            string replac = wildcardOverrides != null ? wildcardOverrides[pair.Key] : pair.Key;
                            srcData = Regex.Replace(srcData, replac, pair.Value(feature));
                        }

                        foreach (ReplacePattern pattern in replacePatterns)
                        {
                            srcData = Regex.Replace(srcData, pattern.find, pattern.replace);
                        }

                        //match string "<Task" to ">" 
                        MatchCollection matches = Regex.Matches(srcData, "(<Task).*>", RegexOptions.Multiline);
                       
                        foreach (Match match in matches)
                        {
                            string newLine = match.Value;

                            //in the new line the task name needs to be replaced from the task identifier with the featue name
                            newLine = Regex.Replace
                                (
                                    newLine,
                                    String.Format("Name=\"{0}\"", taskIdentifier),
                                    String.Format("Name=\"{0}\"",
                                    taskname)
                                );

                            srcData = Regex.Replace(srcData, match.Value, newLine);                            
                        }

                        srcData = Regex.Replace(srcData, taskIdentifier + ":", taskname + ":");

                        File.WriteAllText(opcDir + @"/" + opcName, srcData);
                    }
                    public OPC_UA()
                        : base()
                    {

                    }
                }
            }
        }
    }
}