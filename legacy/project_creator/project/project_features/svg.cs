﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using package;

namespace project_creator
{
    static partial class Project
    {
        public partial class ProjectFeature
        {
            public abstract partial class ConfigurationObject
            {                
                public class SVG : ConfigurationObject
                {
                    public string parent;
                    public List<string> files;
                    public string destination;

                    [XmlArray("ReplacePatterns")]
                    [XmlArrayItem("Pattern")]
                    public List<ReplacePattern> replacePatterns = new List<ReplacePattern>();

                    public override void configure(ProjectFeature feature)
                    {
                        if (conditionsTrue(feature) == false)
                        {
                            return;
                        }

                        string parentData = File.ReadAllText(dest + @"\" + parent);

                        foreach (KeyValuePair<string, WildcardMethods.WildcardDelegate> pair in ProjectFeature.wildcards)
                        {
                            string replac = wildcardOverrides != null ? wildcardOverrides[pair.Key] : pair.Key;                            
                            parentData = Regex.Replace(parentData, replac, pair.Value(feature));
                        }

                        foreach (ReplacePattern pattern in replacePatterns)
                        {
                            parentData = Regex.Replace(parentData, pattern.find, pattern.replace);
                        }

                        if (taskIdentifier != null)
                        {
                            parentData = parentData.Replace(this.taskIdentifier, feature.getName());
                        }

                        using (TextReader reader = new StringReader(parentData))
                        {
                            XDocument xparent = XDocument.Load(reader);

                            foreach (string file in files)
                            {
                                string data = File.ReadAllText(dest + @"\" + file);

                                foreach (KeyValuePair<string, WildcardMethods.WildcardDelegate> pair in ProjectFeature.wildcards)
                                {
                                    string replac = wildcardOverrides != null ? wildcardOverrides[pair.Key] : pair.Key;                                    
                                    data = Regex.Replace(data, replac, pair.Value(feature));
                                }

                                foreach (ReplacePattern pattern in replacePatterns)
                                {
                                    data = Regex.Replace(data, pattern.find, pattern.replace);
                                }

                                if (taskIdentifier != null)
                                {
                                    data = data.Replace(this.taskIdentifier, feature.getName());
                                }

                                using (TextReader elementReader = new StringReader(data))
                                {
                                    XDocument xElement = XDocument.Load(elementReader);

                                    IEnumerable<XElement> descendants = xElement.Root.Elements();

                                    xparent.Root.Add(descendants);
                                }
                            }

                            foreach (KeyValuePair<string, WildcardMethods.WildcardDelegate> pair in ProjectFeature.wildcards)
                            {
                                string replac = wildcardOverrides != null ? wildcardOverrides[pair.Key] : pair.Key;                                
                                destination = Regex.Replace(destination, replac, pair.Value(feature));
                            }

                            foreach (ReplacePattern pattern in replacePatterns)
                            {
                                destination = Regex.Replace(destination, pattern.find, pattern.replace);
                            }

                            if (taskIdentifier != null)
                            {
                                destination = destination.Replace(this.taskIdentifier, feature.getName());
                            }                            

                            File.WriteAllText(dest + @"/" + destination, xparent.ToString());

                            //check if pakcage file exists in dir
                            FileInfo destFile = new FileInfo(dest + @"/" + destination);
                            FileInfo[] pkgs = destFile.Directory.GetFiles("*.pkg", SearchOption.TopDirectoryOnly);

                            //adding to package file only valid when there is exactly one
                            if (pkgs.Length == 1)
                            {
                                package.Package pkg = package.Package.load(pkgs[0].FullName);

                                pkg.addElement(Package.OBJECTS_GROUP, destFile.Name, new XAttribute[]
                                {
                                    new XAttribute(Package.TYPE_ATTRIBUTE, Package.TYPE_ATTRIBUTE_FILE),
                                    new XAttribute(Package.DESCRIPTION_ATTRIBUTE, feature.getID()),
                                });

                                pkg.save(pkgs[0].FullName);
                            }
                        }                       
                    }
                }
            }
        }
    }
}
                   