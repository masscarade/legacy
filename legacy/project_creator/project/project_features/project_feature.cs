﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using codegenLog;

namespace project_creator
{
    public partial class Project
    {
        public partial class ProjectFeature
        {
            //i.e. clamp, core1,..needs to contain numerator
            [XmlAttribute("Type")]
            public string componentType { set; get; }
            [XmlAttribute("IsAbstract")]
            public bool isAbstract { set; get; }
            public List<string> parents { set; get; }
            [XmlAttribute("Unique")]
            public bool unique { set; get; }

            [XmlIgnore]
            public string exportName
            {
                get
                {
                    return componentType + ".xml";
                }
            }

            private string componentName;
            protected string productID;
            protected string productIndex;
            protected codegen.PropertyCollection properties;

            private static HashSet<string> producedNames = new HashSet<string>();

            public static readonly List<KeyValuePair<string, WildcardMethods.WildcardDelegate>> wildcards = new List<KeyValuePair<string, WildcardMethods.WildcardDelegate>>()
            {                
                new KeyValuePair<string, WildcardMethods.WildcardDelegate>(WildcardMethods.FEATURE_INDEX_DEC, WildcardMethods.featureIndexDec),
                new KeyValuePair<string, WildcardMethods.WildcardDelegate>(WildcardMethods.FEATURE_INDEX_INC, WildcardMethods.featureIndexInc),
                new KeyValuePair<string, WildcardMethods.WildcardDelegate>(WildcardMethods.FEATURE_INDEX, WildcardMethods.featureIndex),
                new KeyValuePair<string, WildcardMethods.WildcardDelegate>(WildcardMethods.PRODUCT_ID, WildcardMethods.productID),
                new KeyValuePair<string, WildcardMethods.WildcardDelegate>(WildcardMethods.PRODUCT_INDEX, WildcardMethods.productIndex),
                new KeyValuePair<string, WildcardMethods.WildcardDelegate>(WildcardMethods.FEATURE_INDEX_LETTER, WildcardMethods.featureIndexLetter),
                new KeyValuePair<string, WildcardMethods.WildcardDelegate>(WildcardMethods.ORDER_ID, WildcardMethods.orderID),
            };


            public static class WildcardMethods
            {
                public const string FEATURE_INDEX = "#Index";
                public const string FEATURE_INDEX_DEC = "#IndexDec";
                public const string FEATURE_INDEX_INC = "#IndexInc";
                public const string PRODUCT_ID = "#ProductID";
                public const string PRODUCT_INDEX = "#ProductIndex";
                public const string FEATURE_INDEX_LETTER = "#ILetter";
                public const string ORDER_ID = "#OrderID";

                public static string featureIndexLetter(ProjectFeature inst)
                {
                   

                    if (inst.componentName.Contains(inst.componentType) == false || inst.unique == true)
                    {
                        return "";
                    }

                    int index = int.Parse(inst.componentName.Replace(inst.componentType, ""));

                    return dezToLetter(index);
                }

                private static string dezToLetter(int index)
                {
                    string[] letters = {"a","b", "c", "d", "e", "f", "g", "h", "i", "j","k",
                        "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", };

                    int count = letters.Count();

                    if (index / count == 0)
                    {
                        return letters[index - 1];
                    }
                    else
                    {
                        return dezToLetter((index - index % count) / count) + letters[index % count];
                    }
                }

                public static string featureIndex(ProjectFeature inst)
                {
                    if (inst.componentName.Contains(inst.componentType) == false || inst.unique == true)
                    {
                        return "";
                    }
                    
                    int index = int.Parse(inst.componentName.Replace(inst.componentType, ""));

                    return index.ToString();
                }

                public static string orderID(ProjectFeature inst = null)
                {
                    return Project.orderID != null ? Project.orderID : ORDER_ID;
                }

                public static string featureIndexDec(ProjectFeature inst)
                {
                    if (inst.componentName.Contains(inst.componentType) == false || inst.unique == true)
                    {
                        return "";
                    }

                    int index = int.Parse(inst.componentName.Replace(inst.componentType, ""));
                    index--;

                    return index.ToString();
                }

                public static string featureIndexInc(ProjectFeature inst)
                {
                    if (inst.componentName.Contains(inst.componentType) == false || inst.unique == true)
                    {
                        return "";
                    }

                    int index = int.Parse(inst.componentName.Replace(inst.componentType, ""));
                    index++;

                    return index.ToString();
                }
                public static string productID(ProjectFeature inst)
                {
                    return inst.productID;
                }

                public static string productIndex(ProjectFeature inst)
                {
                    return inst.productIndex;
                }

                public delegate string WildcardDelegate(ProjectFeature inst);
            }

            

            //dictionary with counters for the features
            static Dictionary<string, int> features;
            static Dictionary<string, int> products;
            //list of config objects for the as project, such as OPC UA map, tasks,...
            [XmlArray("Configurations")]
            [XmlArrayItem("Configuration")]
            public List<ConfigurationObject> configurationObjects { private set; get; }

            [XmlAttribute("identifier")]
            public string identifier { set; get; }

            static ProjectFeature()
            {
                features = new Dictionary<string, int>();
                products = new Dictionary<string, int>();
            }

            public override string ToString()
            {
                return string.Format("ProjectFeature name: {0} product: {1}", componentName, productID); ;
            }

            public void configure()
            {
                foreach (ConfigurationObject obj in configurationObjects)
                {
                    Logging.enterContext(obj);

                    Logging.log("start configuring...", LEVEL.INFO);
                    obj.configure(this);
                    Logging.leaveContext();
                }
            }

            public string getName()
            {
                return componentName;
            }

            public string getID()
            {
                return productID;
            }

            public ProjectFeature()
            {
                configurationObjects = new List<ConfigurationObject>();
            }

            public ProjectFeature(string type, string ID, List<ConfigurationObject> config, bool unique = false)
            {
                this.componentType = type;
                this.productID = ID;
                this.configurationObjects = config;
                this.unique = unique;

                init(null);
            }

            public void export()
            {
                //for testing
                XmlSerializer serializer = new XmlSerializer(typeof(ProjectFeature));
                TextWriter writer = new StreamWriter(exportName);

                serializer.Serialize(writer, this);
                writer.Close();
            }

            public void init(string ID, codegen.PropertyCollection porperties = null)
            {
                this.productID = ID;

                init(porperties);
            }

            public void init(codegen.PropertyCollection porperties = null)
            {
                try
                {
                    if (++features[componentType] > 1 && unique == true)
                    {
                        throw new System.ArgumentException("Unique feature already existing: " + componentType + "; Product ID = " + productID);
                    }
                }
                catch (KeyNotFoundException)
                {
                    features.Add(componentType, 1);
                }

                componentName = componentType;

                if (unique == false)
                {
                    componentName += features[componentType].ToString();
                }

                try
                {
                    if (++products[productID] > 1 && unique == true)
                    {
                        throw new System.ArgumentException("Unique feature already existing: " + componentType + "; Product ID = " + productID);
                    }
                }
                catch (KeyNotFoundException)
                {
                    products.Add(productID, 1);
                }
                
                if (porperties != null)
                {
                    List<codegen.Property> instNames = porperties[typeof(codegen.Property.InstanceName)];

                    if (instNames.Count == 1)
                    {
                        componentName = ((codegen.Property.InstanceName)instNames[0]).value;
                    }
                    else if (instNames.Count > 1)
                    {
                        throw new System.ArgumentException("Name already used: " + ((codegen.Property.InstanceName)instNames[0]).value + "; Product ID = " + productID);
                    }              
                }

                if (producedNames.Contains(componentName) == true)
                {
                    throw new System.ArgumentException("Name already used: " + componentName + "; Product ID = " + productID);
                }

                this.properties = porperties;
                producedNames.Add(componentName);
                productIndex = products[productID].ToString();
            }

            [XmlInclude(typeof(OPC_UA))]
            [XmlInclude(typeof(MappView))]
            [XmlInclude(typeof(Mapp))]
            [XmlInclude(typeof(Task))]
            [XmlInclude(typeof(UserFile))]
            [XmlInclude(typeof(UserFile.FixData))]
            [XmlInclude(typeof(UserFile.Recipe))]
            [XmlInclude(typeof(Sourcecode.C))]
            [XmlInclude(typeof(SVG))]
            [XmlInclude(typeof(HardwareModule))]
            [XmlInclude(typeof(ExcludeOverride))]
            public abstract partial class ConfigurationObject
            {
                public abstract void configure(ProjectFeature feature);

                protected bool conditionsTrue(ProjectFeature feature)
                {
                    if (conditions != null)
                    {
                        foreach (Condition condition in conditions)
                        {
                            if (condition.isFullfilled(feature) == false)
                            {
                                return false;
                            }
                        }
                    }

                    return true;
                }

                [XmlAttribute("TaskIdentifier")]
                public string taskIdentifier;

                public List<Condition> conditions;

                public WildcardOverrideList wildcardOverrides;

                public class WildcardOverrideList : List<WildcardOverride>
                {
                    public static WildcardOverrideList getSample()
                    {
                        WildcardOverrideList list = new WildcardOverrideList();
                        list.Add(WildcardOverride.getSample());
                        return list;
                    }

                    /// <summary>
                    /// returns a new key for a wildcard, if an override exists for the specified key; returns the same key oíf nothing found
                    /// </summary>
                    /// <param name="key"></param>
                    /// <returns></returns>
                    public string this[string key]
                    {
                        get
                        {
                            foreach (WildcardOverride obj in this)
                            {
                                if (obj.oldKey == key)
                                {
                                    return obj.newKey;
                                }
                            }

                            //return original key of nothing is found
                            return key;
                        }
                    }
                }

                public class WildcardOverride
                {
                    public string oldKey;
                    public string newKey;

                    public static WildcardOverride getSample()
                    {
                        return new WildcardOverride()
                        {
                            oldKey = "#Index",
                            newKey = "HashtagIndex",
                        };
                    }
                }

                [XmlInclude(typeof(FeatureIndex.FeatureIndex))]
                [XmlInclude(typeof(FeatureIndex.FeatureIndexModulo))]
                [XmlInclude(typeof(FeatureIndex.ProductIndexEqual))]
                [XmlInclude(typeof(FeatureIndex.ProductIndexBigger))]
                [XmlInclude(typeof(FeatureIndex.ProductIndexSmaller))]
                [XmlInclude(typeof(FeatureIndex.ProductIndexUnequal))]
                public abstract class Condition
                {
                    public abstract bool isFullfilled(ProjectFeature feature);

                    public class FeatureIndexModulo : Condition
                    {
                        public int modulo;
                        public int result;
                        public override bool isFullfilled(ProjectFeature feature)
                        {
                            if (feature.componentName.Contains(feature.componentType) == false)
                            {
                                return false;
                            }

                            int index = int.Parse(feature.componentName.Replace(feature.componentType, ""));
                            return index % this.modulo == this.result;
                        }
                    }
                   
                    public class ProductIndexEqual : Condition
                    {
                        public int index;

                        public override bool isFullfilled(ProjectFeature feature)
                        {

                            if (feature.componentName.Contains(feature.componentType) == false)
                            {
                                return false;
                            }

                            int index = Convert.ToInt32(feature.productIndex);
                            return index == this.index;

                        }
                        
                    }

                    public class ProductIndexUnequal : Condition
                    {
                        public int index;

                        public override bool isFullfilled(ProjectFeature feature)
                        {

                            if (feature.componentName.Contains(feature.componentType) == false)
                            {
                                return false;
                            }

                            int index = Convert.ToInt32(feature.productIndex);
                            return index != this.index;

                        }

                    }

                    public class ProductIndexBigger : Condition
                    {
                        public int index;

                        public override bool isFullfilled(ProjectFeature feature)
                        {

                            if (feature.componentName.Contains(feature.componentType) == false)
                            {
                                return false;
                            }

                            int index = Convert.ToInt32(feature.productIndex);
                            return index > this.index;

                        }

                    }

                    public class ProductIndexSmaller : Condition
                    {
                        public int index;

                        public override bool isFullfilled(ProjectFeature feature)
                        {

                            if (feature.componentName.Contains(feature.componentType) == false)
                            {
                                return false;
                            }

                            int index = Convert.ToInt32(feature.productIndex);
                            return index < this.index;

                        }

                    }


                    [XmlInclude(typeof(FeatureIndex.FeatureIndexSmaller))]
                    [XmlInclude(typeof(FeatureIndex.FeatureIndexBigger))]
                    [XmlInclude(typeof(FeatureIndex.FeatureIndexEqual))]
                    [XmlInclude(typeof(FeatureIndex.FeatureIndexUnequal))]                    
                    public abstract class FeatureIndex : Condition
                    {
                        public int index;
                      
                        public class FeatureIndexEqual : FeatureIndex
                        {
                            public override bool isFullfilled(ProjectFeature feature)
                            {
                                if (feature.componentName.Contains(feature.componentType) == false)
                                {
                                    return false;
                                }

                                int index = int.Parse(feature.componentName.Replace(feature.componentType, ""));
                                return index == this.index;
                            }
                        }
                        public class FeatureIndexUnequal : FeatureIndex
                        {
                            public override bool isFullfilled(ProjectFeature feature)
                            {
                                if (feature.componentName.Contains(feature.componentType) == false)
                                {
                                    return false;
                                }

                                int index = int.Parse(feature.componentName.Replace(feature.componentType, ""));
                                return index != this.index;
                            }
                        }
                        public class FeatureIndexSmaller : FeatureIndex
                        {
                            public override bool isFullfilled(ProjectFeature feature)
                            {
                                if (feature.componentName.Contains(feature.componentType) == false)
                                {
                                    return false;
                                }

                                int index = int.Parse(feature.componentName.Replace(feature.componentType, ""));
                                return index < this.index;
                            }
                        }
                        public class FeatureIndexBigger : FeatureIndex
                        {
                            public override bool isFullfilled(ProjectFeature feature)
                            {
                                if (feature.componentName.Contains(feature.componentType) == false)
                                {
                                    return false;
                                }

                                int index = int.Parse(feature.componentName.Replace(feature.componentType, ""));
                                return index > this.index;
                            }
                        }
                    }
                }            
            }
        }
    }             
}
