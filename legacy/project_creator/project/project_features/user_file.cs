﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using codegenLog;

namespace project_creator
{
    static partial class Project
    {
        public partial class ProjectFeature
        {
            public abstract partial class ConfigurationObject
            {
                [XmlRootAttribute("USER File")]
                public abstract class UserFile : ConfigurationObject
                {
                    [XmlArray("ReplacePatterns")]
                    [XmlArrayItem("Pattern")]
                    public List<ReplacePattern> replacePatterns = new List<ReplacePattern>();

                    [XmlAttribute("PathOnUser")]
                    public string path;

                    [XmlAttribute("File")]
                    public string file;

                    [XmlAttribute]
                    public string name;

                    public UserFile()
                        : base()
                    {

                    }

                    [XmlRootAttribute("Fix Data")]
                    public class FixData : UserFile
                    {
                        public override void configure(ProjectFeature feature)
                        {
                            if (conditionsTrue(feature) == false)
                            {
                                Logging.log(string.Format("skip configuration of {0} -> condition check", this), LEVEL.INFO);
                                return;
                            }

                            string dataSrc = dest + @"\" + file;

                            Logging.log(string.Format("load fixdata from: {0}", dataSrc), LEVEL.INFO);
                            string data = File.ReadAllText(dataSrc);

                            if (feature.properties != null)
                            {
                                foreach (codegen.Property.FixDataInput property in feature.properties[typeof(codegen.Property.FixDataInput)])
                                {
                                    Logging.enterContext(property);

                                    string[] elements = property.destination.Split('.');
                                    Logging.log(string.Format("split up variable path to: {0}", string.Join(" ", elements)), LEVEL.INFO);

                                    // Loading from a file, you can also load from a stream

                                    using (TextReader reader = new StringReader(data))
                                    {
                                        XDocument xml = XDocument.Load(reader);

                                        int index = 0;

                                        XElement root = xml.Element("DATA");

                                        bool found = false;
                                        do
                                        {
                                            found = false;

                                            foreach (XElement element in root.Descendants())
                                            {
                                                if (element.Name != "DATA" && element.Name != "Element")
                                                {
                                                    if (element.Attribute("ID").Value == elements[index])
                                                    {
                                                        Logging.log(string.Format("found: {0}", elements[index]), LEVEL.INFO);

                                                        found = true;
                                                        index++;
                                                        root = element;
                                                        break;
                                                    }
                                                }
                                            }

                                            if (found == false)
                                            {
                                                Logging.log(string.Format("descendants of {0} do not contain {1}", root.Value, elements[index]), LEVEL.WARNING);
                                                break;
                                            }

                                        } while (index < elements.Length);

                                        if (found == false)
                                        {
                                            Logging.log(string.Format("skip configuration of {0}", property), LEVEL.WARNING);
                                            Logging.leaveContext();
                                            continue;
                                        }

                                        if (root.Attribute("ID").Value == elements.Last())
                                        {
                                            Logging.log(string.Format("writing value: {0} to: {1}", property.value, root), LEVEL.INFO);
                                            root.Attribute("Value").Value = property.value;                                            
                                            data = xml.ToString();
                                        }
                                    }
                                    Logging.leaveContext();
                                }                            
                            }
                            string rcpName = "";
                            string taskname = feature.componentName;

                            if (name == "" || name == null)
                            {
                                rcpName = taskname;
                            }
                            else
                            {
                                rcpName = name;

                                foreach (KeyValuePair<string, WildcardMethods.WildcardDelegate> pair in ProjectFeature.wildcards)
                                {
                                    string replac = wildcardOverrides != null ? wildcardOverrides[pair.Key] : pair.Key;
                                    Logging.log(string.Format("find: {0} replace {1}", replac, rcpName), LEVEL.INFO);
                                    rcpName = Regex.Replace(rcpName, replac, pair.Value(feature));
                                }

                                foreach (ReplacePattern pattern in replacePatterns)
                                {
                                    Logging.log(string.Format("find: {0} replace {1}", pattern.find, pattern.replace), LEVEL.INFO);
                                    rcpName = Regex.Replace(rcpName, pattern.find, pattern.replace);
                                }

                            }

                            Logging.log(string.Format("find: {0} replace {1}", taskIdentifier, rcpName), LEVEL.INFO);
                            data = Regex.Replace(data, @"\b" + taskIdentifier + @"\b", rcpName);

                            DirectoryInfo destDir = new DirectoryInfo(Directory.GetDirectories(dest, @"USER_files", SearchOption.AllDirectories)[0]);

                            destDir = destDir.GetDirectories("fix")[0];

                            string dataDest = destDir.FullName + @"/" + rcpName + ".xml";

                            Logging.log(string.Format("write data to {0}", dataDest), LEVEL.INFO);
                            File.WriteAllText(dataDest, data);
                        }         
                    }
                    [XmlRootAttribute("Recipe")]
                    public class Recipe : UserFile
                    {
                        public override void configure(ProjectFeature feature)
                        {
                            if (conditionsTrue(feature) == false)
                            {
                                return;
                            }

                            string data = File.ReadAllText(dest + @"\" + file);

                            string rcpName = "";
                            string taskname = feature.componentName;

                            if (name == "" || name == null)
                            {
                                rcpName = taskname;
                            }
                            else
                            {
                                rcpName = name;

                                foreach (KeyValuePair<string, WildcardMethods.WildcardDelegate> pair in ProjectFeature.wildcards)
                                {
                                    string replac = wildcardOverrides != null ? wildcardOverrides[pair.Key] : pair.Key;                                    
                                    rcpName = Regex.Replace(rcpName, replac, pair.Value(feature));
                                }

                                foreach (ReplacePattern pattern in replacePatterns)
                                {
                                    rcpName = Regex.Replace(rcpName, pattern.find, pattern.replace);
                                }

                            }

                            data = Regex.Replace(data,taskIdentifier , rcpName);

                            DirectoryInfo destDir = new DirectoryInfo(Directory.GetDirectories(dest, @"USER_files", SearchOption.AllDirectories)[0]);

                            destDir = destDir.GetDirectories("recipe")[0];

                            File.WriteAllText(destDir.FullName + @"/" + rcpName + ".xml", data);
                        }
                    
                    }
                }
            }
        }
    }
}