﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using codegenLog;
using package;

namespace project_creator
{
    partial class Project
    {     
        public partial class ProjectFeature
        {
            public abstract partial class ConfigurationObject
            {
                [XmlRootAttribute("Task")]
                public class Task : ConfigurationObject
                {
                    [XmlAttribute("TaskClass")]
                    public string taskClass;

                    [XmlAttribute("Mode")]
                    public Mode mode;

                    [XmlAttribute("NamingPattern")]
                    public string namingPattern;

                    [XmlElement("Task")]
                    public sw_config.SwConfigurationTaskClassTask task;

                    public enum Mode
                    {
                        REFERENCE,
                        COPY,
                    }

                    public override void configure(ProjectFeature feature)
                    {

                        if (conditionsTrue(feature) == false)
                        {
                            return;
                        }             

                        string name = feature.getName();

                        if(namingPattern != null && namingPattern != "")
                        {
                            name = namingPattern;

                            foreach (KeyValuePair<string, WildcardMethods.WildcardDelegate> pair in ProjectFeature.wildcards)
                            {
                                string replac = wildcardOverrides != null ? wildcardOverrides[pair.Key] : pair.Key;                                
                                name = Regex.Replace(name, replac, pair.Value(feature));
                            }
                        }

                        this.task.Name = name;
                        this.task.Description = feature.getID();

                        if (mode == Mode.COPY)
                        {
                            //find prg name
                            Match match = Regex.Match(this.task.Source, @"([^\..]+)\.prg", RegexOptions.RightToLeft);

                            string src = this.task.Source.Replace(".prg", "");
                            src = src.Replace(".", @"\");
                            src = Project.dest + @"\Logical\" + src;

                            DirectoryInfo srcDir = new DirectoryInfo(src);

                            string dest = srcDir.Parent.FullName + @"\" + name;

                            Directory.CreateDirectory(dest);

                            Methods.CloneDirectory(srcDir.FullName, dest);

                            string source = Regex.Match(dest.Replace(@"\","."), @"(?<=Logical\.)(.*)").Value;
                            this.task.Source = source + ".prg";

                            /*add folder for the feature type if not existing*/
                            string pagesPkgDir = Directory.GetFiles(srcDir.Parent.FullName, "*.pkg", SearchOption.TopDirectoryOnly)[0];
                            package.Package pagesPkg = package.Package.load(pagesPkgDir);

                            //add package for this feature type
                            pagesPkg.addElement(Package.OBJECTS_GROUP, name, new XAttribute[]
                            {
                                    new XAttribute(Package.TYPE_ATTRIBUTE, Package.TYPE_ATTRIBUTE_PROGRAM),
                                    new XAttribute(Package.DESCRIPTION_ATTRIBUTE, "Auto generated"),
                                    new XAttribute("Language", "ANSIC"),
                            });

                            pagesPkg.save(pagesPkgDir);

                        }
                      
                       
                        /*search for target taskclass*/
                        foreach (sw_config.SwConfigurationTaskClass taskClass in swConfiguration.TaskClass)
                        {
                            /*taskclass found*/
                            if (taskClass.Name == this.taskClass)
                            {
                                /*taskclass is empty*/
                                if (taskClass.Task == null)
                                {
                                    /*add first member to taskclass*/
                                    taskClass.Task = new sw_config.SwConfigurationTaskClassTask[1];
                                    taskClass.Task[0] = task;
                                }
                                else
                                {
                                    /*add task to taskclass*/
                                    List<sw_config.SwConfigurationTaskClassTask> tasks = new List<sw_config.SwConfigurationTaskClassTask>(taskClass.Task);

                                    if (tasks.Contains(task) == false)
                                    {                                                                               
                                        tasks.Add(task);
                                        taskClass.Task = tasks.ToArray();
                                    }                                                                        
                                }
                            }
                        }
                    }

                    public Task()
                        : base()
                    {
                        task = new sw_config.SwConfigurationTaskClassTask();

                    }

                }
            }
        }
    }
}