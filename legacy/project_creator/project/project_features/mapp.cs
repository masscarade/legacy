﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using codegen;
using codegenLog;
using package;

namespace project_creator
{
    partial class Project
    {

        [Serializable]
        [XmlType(TypeName = "ReplacePattern")]
        public struct ReplacePattern
        {
            public string find
            { get; set; }

            public string replace
            { get; set; }
        }

        public partial class ProjectFeature
        {
            public abstract partial class ConfigurationObject
            {
                [XmlRootAttribute("Mapp")]
                public class Mapp : ConfigurationObject
                {

                    const string MAPP_CONTROL = "mappControl";
                    const string MAPP_SERVICES = "mappServices";
                    const string MOTION = "Motion";

                    //list of config objects for the as project, such as OPC UA map, tasks,...
                    [XmlArray("Elements")]
                    [XmlArrayItem("Element")]
                    public List<MappElement> mappElements = new List<MappElement>();

                    public override void configure(ProjectFeature feature)
                    {
                        if (conditionsTrue(feature) == false)
                        {
                            return;
                        }
                        foreach (MappElement element in mappElements)
                        {
                            element.configure(feature);
                        }                       
                    }

                    public Mapp()
                        : base()
                    {

                    }
                    private static bool nodeIsChild(XmlNode parent, XmlNode inst)
                    {
                        foreach (XmlAttribute attribute in inst.Attributes)
                        {
                            if (attribute.Name == "id")
                            {
                                if (getNodeById(parent, attribute.Value) != null)
                                {
                                    return true;
                                }
                                break;
                            }                          
                        }
                        return false;
                    }

                    private static XmlNode getNodeById(XmlNode root, string id)
                    {
                        XmlNode result = null;
                        foreach (XmlNode node in root.ChildNodes)
                        {
                            foreach (XmlAttribute attribute in node.Attributes)
                            {
                                if (attribute.Name == "id" && attribute.Value == id)
                                {
                                    return node;
                                }
                            }

                            if (( result = getNodeById(node, id)) != null)
                            {
                                return result;
                            }
                        }

                        return result;
                    }

                    public const string ALARM = ".mpalarmxcore";
                    public const string SEQUENCER = ".mpsequencecore";
                    public const string RECIPEX = ".mprecipexml";
                    public const string EJECTOR_BASIC = ".ejectorbasic";
                    public const string SCALEPUMP = ".scalepump";
                    public const string SCALEPOTI = ".scalepoti";
                    public const string CLAMP_BASIC = ".clampbasic";
                    public const string COREPULLER_BASIC = ".corepullbasic";
                    public const string INJECTION_BASIC = ".injectbasic";
                    public const string PLASTIFICATION_BASIC = ".injectplastificationbasic";
                    public const string INJECTIONUNIT_BASIC = ".injunitbasic";
                    public const string AXIS_BASIC = ".mpaxisbasic";
                    public const string NCM = ".ncm";
                    public const string DATARECORDER = ".mpdatarecorder";
                    public const string CODEBOX = ".mpcodebox";
                    public const string GROUP = ".mpcomgroup";
                    public const string TEMP_CONTROLLER = ".tempcontroller";

                    public static MappElement generateObject(string file)
                    {
                        string extension = Path.GetExtension(file);

                        switch (extension)
                        {
                            case ALARM:
                                return new ConfigurationObject.Mapp.MappElement.AlarmX();
                                //break

                            case SEQUENCER:
                                return new ConfigurationObject.Mapp.MappElement.Sequencer();                          
                                //break

                            case RECIPEX:
                                return new ConfigurationObject.Mapp.MappElement.RecipeXml();
                            //break

                            case EJECTOR_BASIC:
                                return new ConfigurationObject.Mapp.MappElement.EjectoRBasic();
                            //break

                            case SCALEPOTI:
                                return new ConfigurationObject.Mapp.MappElement.ScalePoti();
                            //break

                            case SCALEPUMP:
                                return new ConfigurationObject.Mapp.MappElement.ScalePump();
                            //break

                            case CLAMP_BASIC:
                                return new ConfigurationObject.Mapp.MappElement.ClampBasic();
                            //break

                            case COREPULLER_BASIC:
                                return new ConfigurationObject.Mapp.MappElement.CorepullerBasic();
                            //break

                            case INJECTION_BASIC:
                                return new ConfigurationObject.Mapp.MappElement.InjectBasic();
                            //break

                            case PLASTIFICATION_BASIC:
                                return new ConfigurationObject.Mapp.MappElement.PlastificationBasic();
                            //break

                            case INJECTIONUNIT_BASIC:
                                return new ConfigurationObject.Mapp.MappElement.InjectionUnitBasic();

                            case DATARECORDER:
                                return new ConfigurationObject.Mapp.MappElement.Datarecorder();
                            //break

                            case CODEBOX:
                                return new ConfigurationObject.Mapp.MappElement.Codebox();

                            case AXIS_BASIC:
                                return new ConfigurationObject.Mapp.MappElement.AxisBasic();

                            case NCM:
                                return new ConfigurationObject.Mapp.MappElement.NCMapping();

                            case GROUP:
                                return new ConfigurationObject.Mapp.MappElement.Group();
                            //break


                            case TEMP_CONTROLLER:
                                return new ConfigurationObject.Mapp.MappElement.TempController();
                            //break

                            default:
                                return null;
                            //break;
                        }

                        //return null;
                    }
                 
                    [XmlInclude(typeof(AlarmX))]
                    [XmlInclude(typeof(Sequencer))]
                    [XmlInclude(typeof(MappPackage))]
                    [XmlInclude(typeof(RecipeXml))]
                    public abstract class MappElement
                    {
                        public abstract string type { get; }
                        public abstract string prefix { get; }
                        public abstract string extension { get; }

                        //list of config objects for the as project, such as OPC UA map, tasks,...
                        [XmlArray("ReplacePatterns")]
                        [XmlArrayItem("Pattern")]
                        public List<ReplacePattern> replacePatterns = new List<ReplacePattern>();                 

                        [XmlAttribute("Target")]
                        public string target;

                        [XmlAttribute("TaskIdentifier")]
                        public string taskIdentifier;

                        public virtual void configure(ProjectFeature feature)
                        {
                            throw new NotImplementedException();
                        }

                        public virtual string replaceContent(string srcData, ProjectFeature feature)
                        {
                            string data = Regex.Replace(srcData, taskIdentifier, feature.componentName);

                            return Regex.Replace(data, feature.productID, feature.componentName);
                        }

                        public virtual void configure(ProjectFeature feature, String srcData, String filename)
                        {

                            string data = replaceContent(srcData, feature);

                            DirectoryInfo destDir = new DirectoryInfo(Directory.GetDirectories(dest + @"/Physical/" + config , type, SearchOption.AllDirectories)[0]);

                            FileInfo file = new FileInfo(destDir.FullName + @"/" + prefix + feature.componentName + extension);
                            File.WriteAllText(file.FullName, data);

                            /*add folder for the feature type if not existing*/
                            string pkgDir = Directory.GetFiles(destDir.FullName, "*.pkg", SearchOption.TopDirectoryOnly)[0];
                            Package pkg = Package.load(pkgDir);

                            pkg.addFile(file.Name, feature.getID());
                            //pkg.addElement(Package.OBJECTS_GROUP, file.Name, new XAttribute[] { new XAttribute("Type","File"), });

                            //pkg.add(pkgObj);
                            pkg.save(pkgDir);
                        }

                        [XmlRootAttribute("Mapp Package")]
                        public class MappPackage : MappElement
                        {
                            public override string extension
                            {
                                get { throw new NotImplementedException(); }
                            }
                            public override string prefix
                            {
                                get { throw new NotImplementedException(); }
                            }
                            public override string type
                            {
                                get { throw new NotImplementedException(); }
                            }
                            [XmlAttribute("SourcePath")]
                            public string srcPath;

                            public List<Condition> conditions;

                            public override void configure(ProjectFeature feature, string srcData, string filename)
                            {
                                throw new NotImplementedException();
                            }

                            public override void configure(ProjectFeature feature)
                            {
                                if (conditions != null)
                                {
                                    foreach (Condition obj in conditions)
                                    {
                                        if (obj.isFullfilled(feature) == false)
                                        {
                                            Logging.log(String.Format("condition not fulfilled, skip configuration of {0}", feature), LEVEL.INFO);
                                            return;
                                        }
                                    }
                                }

                                string[] files = Directory.GetFiles(dest + @"/" + srcPath, "*", SearchOption.TopDirectoryOnly);

                                foreach (string file in files)
                                {
                                    Mapp.MappElement element = Mapp.generateObject(file);

                                    if(element == null)
                                    {
                                        continue;
                                    }

                                    element.taskIdentifier = this.taskIdentifier;
                                    element.target = this.target;
                                    element.replacePatterns = this.replacePatterns;

                                    string srcData = File.ReadAllText(file);                                   

                                    element.configure(feature, srcData, Path.GetFileName(file));
                                }

                            }

                        }

                        [XmlRootAttribute("RecipeXml")]
                        public class RecipeXml : MappElement
                        {
                            public override string extension
                            {
                                get { return RECIPEX; }
                            }
                            public override string prefix
                            {
                                get { return "rx"; }
                            }
                            public override string type
                            {
                                get { return MAPP_SERVICES; }
                            }
                        }

                        [XmlRootAttribute("TempController")]
                        public class TempController : MappElement
                        {
                            public override string extension
                            {
                                get { return TEMP_CONTROLLER; }
                            }
                            public override string prefix
                            {
                                get { return "tc"; }
                            }
                            public override string type
                            {
                                get { return MAPP_CONTROL; }
                            }
                        }

                        [XmlRootAttribute("EjectroBasic")]
                        public class EjectoRBasic : MappElement
                        {
                            public override string replaceContent(string srcData, ProjectFeature feature)
                            {
                                string data = Regex.Replace(srcData, taskIdentifier + ":", feature.componentName + ":");

                                return Regex.Replace(data, feature.productID, feature.componentName);
                            }
                            public override string extension
                            {
                                get { return EJECTOR_BASIC; }
                            }
                            public override string prefix
                            {
                                get { return "eb"; }
                            }
                            public override string type
                            {
                                get { return MAPP_CONTROL; }
                            }
                        }

                        [XmlRootAttribute("Datarecorder")]
                        public class Datarecorder : MappElement
                        {
                            public override string extension
                            {
                                get { return DATARECORDER; }
                            }
                            public override string prefix
                            {
                                get { return "dr"; }
                            }
                            public override string type
                            {
                                get { return MAPP_SERVICES; }
                            }
                        }

                        [XmlRootAttribute("CorepullerBasic")]
                        public class CorepullerBasic : MappElement
                        {
                            public override string replaceContent(string srcData, ProjectFeature feature)
                            {
                                string data = Regex.Replace(srcData, taskIdentifier + ":", feature.componentName + ":");

                                return Regex.Replace(data, feature.productID, feature.componentName);
                            }
                            public override string extension
                            {
                                get { return COREPULLER_BASIC; }
                            }
                            public override string prefix
                            {
                                get { return "cb"; }
                            }
                            public override string type
                            {
                                get { return MAPP_CONTROL; }
                            }
                        }

                        [XmlRootAttribute("InjectBasic")]
                        public class InjectBasic : MappElement
                        {
                            public override string replaceContent(string srcData, ProjectFeature feature)
                            {
                                string data = Regex.Replace(srcData, taskIdentifier + ":", feature.componentName + ":");

                                return Regex.Replace(data, feature.productID, feature.componentName);
                            }
                            public override string extension
                            {
                                get { return INJECTION_BASIC; }
                            }
                            public override string prefix
                            {
                                get { return "ib"; }
                            }
                            public override string type
                            {
                                get { return MAPP_CONTROL; }
                            }
                        }

                        [XmlRootAttribute("PlastificationBasic")]
                        public class PlastificationBasic : MappElement
                        {
                            public override string replaceContent(string srcData, ProjectFeature feature)
                            {
                                string data = Regex.Replace(srcData, taskIdentifier + ":", feature.componentName + ":");

                                return Regex.Replace(srcData, feature.productID, feature.componentName);
                            }
                            public override string extension
                            {
                                get { return PLASTIFICATION_BASIC; }
                            }
                            public override string prefix
                            {
                                get { return "pb"; }
                            }
                            public override string type
                            {
                                get { return MAPP_CONTROL; }
                            }
                        }

                        [XmlRootAttribute("AxisBasic")]
                        public class AxisBasic : MappElement
                        {
                            public override string replaceContent(string srcData, ProjectFeature feature)
                            {
                                string data = Regex.Replace(srcData, taskIdentifier, feature.componentName);

                                return Regex.Replace(data, feature.productID, feature.componentName);
                            }
                            public override string extension
                            {
                                get { return AXIS_BASIC; }
                            }
                            public override string prefix
                            {
                                get { return "ab"; }
                            }
                            public override string type
                            {
                                get { return MOTION; }
                            }
                        }

                        [XmlRootAttribute("NCM")]
                        public class NCMapping : MappElement
                        {
                            public override string replaceContent(string srcData, ProjectFeature feature)
                            {
                                string data = Regex.Replace(srcData, taskIdentifier, feature.componentName);

                                foreach (KeyValuePair<string, WildcardMethods.WildcardDelegate> pair in ProjectFeature.wildcards)
                                {
                                    string replac = pair.Key;
                                    data = Regex.Replace(data, replac, pair.Value(feature));
                                }

                                return Regex.Replace(data, feature.productID, feature.componentName);
                            }

                            public override void configure(ProjectFeature feature, String srcData, String filename)
                            {

                                string data = replaceContent(srcData, feature);

                                DirectoryInfo destDir = new DirectoryInfo(Directory.GetDirectories(dest + @"/Physical/" + config, type, SearchOption.AllDirectories)[0]);


                                using (TextReader reader = new StringReader(data))
                                {
                                    XDocument xml = XDocument.Load(reader);

                                    XElement top = ncMapping.Element("NcMapping");

                                    var abc = xml.Descendants("NcObject");

                                    top.Add(xml.Descendants("NcObject"));
                                }
                            }

                            public override string extension
                            {
                                get { return NCM; }
                            }
                            public override string prefix
                            {
                                get { return "nc"; }
                            }
                            public override string type
                            {
                                get { return MOTION; }
                            }
                        }

                        [XmlRootAttribute("InjectionUnitBasic")]
                        public class InjectionUnitBasic : MappElement
                        {
                            public override string replaceContent(string srcData, ProjectFeature feature)
                            {
                                string data = Regex.Replace(srcData, taskIdentifier + ":", feature.componentName + ":");

                                foreach (KeyValuePair<string, WildcardMethods.WildcardDelegate> pair in ProjectFeature.wildcards)
                                {
                                    string replac = pair.Key;
                                    data = Regex.Replace(data, replac, pair.Value(feature));
                                }

                                return Regex.Replace(data, feature.productID, feature.componentName);                               
                            }
                            public override string extension
                            {
                                get { return INJECTIONUNIT_BASIC; }
                            }
                            public override string prefix
                            {
                                get { return "nb"; }
                            }
                            public override string type
                            {
                                get { return MAPP_CONTROL; }
                            }
                        }

                        [XmlRootAttribute("ClampBasic")]
                        public class ClampBasic : MappElement
                        {
                            public override string replaceContent(string srcData, ProjectFeature feature)
                            {
                                string data = Regex.Replace(srcData, taskIdentifier + ":", feature.componentName + ":");

                                return Regex.Replace(data, feature.productID, feature.componentName);

                                foreach (KeyValuePair<string, WildcardMethods.WildcardDelegate> pair in ProjectFeature.wildcards)
                                {
                                    string replac = pair.Key;
                                    data = Regex.Replace(data, replac, pair.Value(feature));
                                }
                            }
                            public override string extension
                            {
                                get { return CLAMP_BASIC; }
                            }
                            public override string prefix
                            {
                                get { return "cl"; }
                            }
                            public override string type
                            {
                                get { return MAPP_CONTROL; }
                            }
                        }

                        [XmlRootAttribute("ScalePump")]
                        public class ScalePump : MappElement
                        {
                            public override string extension
                            {
                                get { return SCALEPUMP; }
                            }
                            public override string prefix
                            {
                                get { return "sp"; }
                            }
                            public override string type
                            {
                                get { return MAPP_CONTROL; }
                            }
                        }
                        [XmlRootAttribute("ScalePoti")]
                        public class ScalePoti : MappElement
                        {
                            public override string extension
                            {
                                get { return SCALEPOTI; }
                            }
                            public override string prefix
                            {
                                get { return "sc"; }
                            }
                            public override string type
                            {
                                get { return MAPP_CONTROL; }
                            }
                        }

                        [XmlRootAttribute("AlarmX")]
                        public class AlarmX : MappElement
                        {
                            public override string extension
                            {
                                get { return ALARM; }
                            }
                            public override string prefix
                            {
                                get { return "ax"; }
                            }

                            public override string type
                            {
                                get { return MAPP_SERVICES; }
                            }

                            public AlarmX()
                                : base()
                            {

                            }

                            public override void configure(ProjectFeature feature, string srcData, string filename)
                            {
                                srcData = srcData.Replace(this.taskIdentifier, feature.getName());

                                foreach (KeyValuePair<string, WildcardMethods.WildcardDelegate> pair in ProjectFeature.wildcards)
                                {
                                    string replac = pair.Key;
                                }

                                byte[] buffer = Encoding.UTF8.GetBytes(srcData);

                                using (MemoryStream stream = new MemoryStream(buffer))
                                {

                                    XmlSerializer srcSerializer = new XmlSerializer(typeof(mapp_meta.Configuration));
                                    alarmConfiguration.addElements((mapp_meta.Configuration)srcSerializer.Deserialize(stream));
                                }
                            }

                            public override void configure(ProjectFeature feature)
                            {

                                throw new NotImplementedException();

                                string[] paths = Directory.GetDirectories(productSrc + @"\", feature.getID(), SearchOption.AllDirectories);
                                string path = paths[0];

                                string[] alarmFiles = Directory.GetFiles(path, "*" + ProjectFeature.ConfigurationObject.Mapp.ALARM, SearchOption.AllDirectories);
                                string srcPath = alarmFiles[0];

                                String srcData = File.ReadAllText(srcPath);
                                srcData = srcData.Replace(this.taskIdentifier, feature.getName());

                                byte[] buffer = Encoding.UTF8.GetBytes(srcData);

                                using (MemoryStream stream = new MemoryStream(buffer))
                                {

                                    XmlSerializer srcSerializer = new XmlSerializer(typeof(mapp_meta.Configuration));
                                    alarmConfiguration.addElements((mapp_meta.Configuration)srcSerializer.Deserialize(stream));
                                }

                            }
                        }

                        [XmlRootAttribute("Sequencer")]
                        public class Sequencer : MappElement
                        {
                            public override string extension
                            {
                                get { return SEQUENCER; }
                            }
                            public override string prefix
                            {
                                get { return "sq"; }
                            }

                            public override string type
                            {
                                get { return MAPP_SERVICES; }
                            }
                            public Sequencer()
                                : base()
                            {

                            }

                            private string generateDynamicInterlocks(string axis, codegen.Property.DynamicInterlock interlock, string srcData)
                            {
                                if (interlock.axes == null)
                                {
                                    Logging.log("interlock contains no axes; skip dynamic interlock generation", LEVEL.INFO);
                                    return srcData;
                                }
                                if (interlock.axes.Count <= 0)
                                {
                                    Logging.log("interlock contains no axes; skip dynamic interlock generation", LEVEL.INFO);
                                    return srcData;
                                }

                                Logging.log("generating dynamic interlocks", LEVEL.INFO);

                                XDocument xml;

                                using (TextReader reader = new StringReader(srcData))
                                {
                                    xml = XDocument.Load(reader);

                                    XElement dynInterlocks = null;
                                    XElement parent = xml.Descendants().Where(x => (string)x.Attribute("ID") == "Sequence").First();

                                    /*merge all dynamic interlocs into one element*/
                                    Logging.log(string.Format("parse for dynamic interlock groups"), LEVEL.INFO);
                                    var temp = xml.Descendants().Where(x => (string)x.Attribute("ID") == "DynamicInterlocks");

                                    //dyn interlocks found
                                    if (temp.Count() != 0)
                                    {
                                        XElement first = temp.First();
                                        do
                                        {
                                            XElement inst = temp.Last();
                                            if (inst == first)
                                            {
                                                continue;
                                            }
                                            Logging.log(string.Format("merge redundant dyn interlocks"), LEVEL.INFO);
                                            first.Add(inst.Descendants());
                                            inst.Remove();
                                        } while (temp.Count() > 1);

                                        dynInterlocks = first;
                                    }

                                    if (dynInterlocks == null)
                                    {
                                        Logging.log(string.Format("no dynamic interlock groups found"), LEVEL.INFO);

                                        dynInterlocks = new XElement("Group");
                                        dynInterlocks.Add(new XAttribute("ID", "DynamicInterlocks"));
                                        parent.Add(dynInterlocks);

                                        Logging.log(string.Format("dynamic interlock group created"), LEVEL.INFO);
                                    }

                                    /*list all axes as string*/
                                    var axes = xml.Descendants()
                                        .Where(x => (string)x.Attribute("ID") == "Axes")
                                        .Descendants("Property")
                                        .Where(y => (string)y.Attribute("ID") == "Name")
                                        .Select(z => (string)z.Attribute("Value"));

                                    /*get interlock element for this axis; create new node if not existing*/
                                    Logging.log(string.Format("search existing interlocks group for {0}", axis), LEVEL.INFO);
                                    var node = dynInterlocks.Descendants("Property")
                                        .Where(x => (string)x.Attribute("ID") == "Axis" && (string)x.Attribute("Value") == axis);

                                    XElement currentInterlock = null;

                                    /*create new "DynamicInterlock[X]" node if no match was found*/
                                    if (node.Count() == 0)
                                    {
                                        Logging.log(string.Format("dynamic interlocks group for axis {0} not existing", axis), LEVEL.INFO);
                                        XElement interlockEl = new XElement("Group");
                                        interlockEl.Add(new XAttribute("ID", string.Format("DynamicInterlock[{0}]", dynInterlocks.Elements().Count())));

                                        XElement id = new XElement("Property");
                                        id.Add(new XAttribute("ID", "Axis"));
                                        id.Add(new XAttribute("Value", axis));

                                        interlockEl.Add(id);

                                        XElement list = new XElement("Group");
                                        list.Add(new XAttribute("ID", "Interlocks"));

                                        interlockEl.Add(list);

                                        dynInterlocks.Add(interlock);
                                        currentInterlock = interlockEl;
                                    }
                                    else if (node.Count() == 1)
                                    {
                                        currentInterlock = node.First();
                                    }
                                    else
                                    {
                                        Logging.log(string.Format("dynamic interlocks group for axis {0} found multiple times", axis), LEVEL.ERROR);
                                    }

                                    foreach (Property.DynamicInterlock.Axis ax in interlock.axes)
                                    {
                                        string element = ax.axisName;

                                        //do not generate interlock for current axis -> would be redundant
                                        if (element == axis)
                                        {
                                            continue;
                                        }

                                        if (axes.Contains(element))
                                        {
                                            Logging.log(string.Format("dynamic interlocks group for axis {0} already existing; skip", element), LEVEL.INFO);
                                            continue;
                                        }

                                        //get element containing the actuel axes list
                                        XElement axesList = currentInterlock.Elements().Where(x => (string)x.Attribute("ID") == "Interlocks").First();

                                        //check if the current axis is already included and skip if true
                                        if (axesList.Elements().Where(x => (string)x.Attribute("ID") == "Interlocks").Count() > 0)
                                        {
                                            continue;
                                        }

                                        Logging.log(string.Format("add dynamic interlock for {0} to {1}", element, axis), LEVEL.INFO);

                                        XElement interlockEl = new XElement("Property");
                                        interlockEl.Add(new XAttribute("ID", string.Format("Axes[{0}]", axesList.Elements().Count())));
                                        interlockEl.Add(new XAttribute("Value", element));

                                        axesList.Add(interlock);
                                    }
                                }

                                /*<Group ID="DynamicInterlocks">
                                   <Group ID="DynamicInterlock[0]">
                                     <Property ID="Axis" Value="Clamp" />
                                     <Group ID="Interlocks">
                                       <Property ID="Axes[0]" Value="Core1" />
                                     </Group>
                                   </Group>
                                   <Group ID="DynamicInterlock[1]">
                                     <Property ID="Axis" Value="Core1" />
                                     <Group ID="Interlocks">
                                       <Property ID="Axes[0]" Value="Core2" />
                                     </Group>
                                   </Group>
                                 </Group>*/

                                //return src data if no interlock applied
                                return xml.ToString();
                            }

                            private string generateStaticInterlocks(codegen.Property.StaticInterlock interlock, string srcData)
                            {
                                using (TextReader reader = new StringReader(srcData))
                                {
                                    XDocument xml = XDocument.Load(reader);

                                    foreach (XElement element in xml.Descendants())
                                    {
                                        XAttribute id = element.Attribute("ID");
                                        XAttribute value = element.Attribute("Value");

                                        if (id == null || value == null)
                                        {
                                            continue;
                                        }
                                        //search for "<Property ID="Name" Value="Clamp1CloseVacuum" />" to get the right command
                                        else if (id.Value == "Name" && value.Value == interlock.command)
                                        {
                                            XElement command = element.Parent;

                                            foreach (codegen.Property.StaticInterlock.Axis axis in interlock.axes)
                                            {
                                                foreach (XElement selector in command.Elements("Selector"))
                                                {
                                                    if (selector.Attribute("ID").Value == "CommandType")
                                                    {
                                                        int cInterlocks = selector.Elements("Selector").Count();

                                                        XElement newInterlock = new XElement("Selector", 
                                                            new XAttribute("ID", String.Format("ConditionCheck[{0}]", cInterlocks)));

                                                        newInterlock.Add(new XElement[]
                                                        {
                                                            new XElement("Property", new XAttribute("ID", "ConditionAxis"), new  XAttribute("Value", axis.axisName)),
                                                            new XElement("Property", new XAttribute("ID", "ExpectedState"), new  XAttribute("Value", axis.expectedState)),
                                                            new XElement("Property", new XAttribute("ID", "Reaction"), new  XAttribute("Value", axis.reaction)),
                                                        });

                                                        selector.Add(newInterlock);
                                                        return xml.ToString();
                                                    }
                                                }
                                            }                                            
                                        }
                                    }
                                }

                                //return src data if no interlock applied
                                return srcData;
                            }

                            private string generateStaticInterlocks(ProjectFeature feature, string srcData)
                            {
                                string data = srcData;

                                if (feature.properties != null)
                                {
                                    foreach (codegen.Property.StaticInterlock property in feature.properties[typeof(codegen.Property.StaticInterlock)])
                                    {
                                        data = generateStaticInterlocks(property, data);
                                    }
                                }

                                return data;
                            }

                            private string generateDynamicInterlocks(ProjectFeature feature, string srcData)
                            {
                                string data = srcData;

                                if (feature.properties != null)
                                {
                                    foreach (codegen.Property.DynamicInterlock property in feature.properties[typeof(codegen.Property.DynamicInterlock)])
                                    {
                                        Logging.enterContext(property);
                                        data = generateDynamicInterlocks(feature.componentName, property, data);
                                        Logging.leaveContext();
                                    }
                                }

                                return data;
                            }

                            private string generateSequencerCommandProperties(ProjectFeature feature, string srcData)
                            {
                                string data = srcData;

                                if (feature.properties != null)
                                {
                                    foreach (codegen.Property.CommandProperty property in feature.properties[typeof(codegen.Property.CommandProperty)])
                                    {
                                        data = generateSequencerCommandProperties(property, data);
                                    }
                                }

                                return data;
                            }

                            private string generateSequencerCommandProperties(Property.CommandProperty property, string srcData)
                            {
                                using (TextReader reader = new StringReader(srcData))
                                {
                                    XDocument xml = XDocument.Load(reader);

                                    foreach (XElement element in xml.Descendants())
                                    {
                                        XAttribute id = element.Attribute("ID");
                                        XAttribute value = element.Attribute("Value");

                                        if (id == null || value == null)
                                        {
                                            continue;
                                        }
                                        //search for "<Property ID="Name" Value="Clamp1CloseVacuum" />" to get the right command
                                        else if (id.Value == "Name" && value.Value == property.commandName)
                                        {
                                            XElement command = element.Parent;

                                            foreach (XElement selector in command.Elements("Selector"))
                                            {
                                                if (selector.Attribute("ID").Value == "CommandType")
                                                {
                                                    int cInterlocks = selector.Elements("Selector").Count();

                                                    foreach (XElement prop in selector.Elements("Property"))
                                                    {
                                                        if (prop.Attribute("ID").Value == property.propertyId)
                                                        {
                                                            prop.Remove();
                                                            break;
                                                        }
                                                    }
                                                }

                                                XElement newProperty = new XElement("Property",
                                                    new XAttribute("ID", property.propertyId),
                                                    new XAttribute("Value", property.propertyValue)
                                                    );


                                                selector.Add(newProperty);
                                                return xml.ToString();
                                            }
                                        }

                                    }
                                }

                                //return src data if no interlock applied
                                return srcData;
                            }

                            public override void configure(ProjectFeature feature, string srcData, string filename)
                            {
                                foreach (ReplacePattern pattern in replacePatterns)
                                {
                                    srcData = srcData.Replace(pattern.find, pattern.replace);
                                }

                                foreach (KeyValuePair<string, WildcardMethods.WildcardDelegate> pair in ProjectFeature.wildcards)
                                {
                                    string replac = pair.Key;
                                    srcData = Regex.Replace(srcData, replac, pair.Value(feature));
                                }

                                srcData = generateStaticInterlocks(feature, srcData);

                                srcData = generateDynamicInterlocks(feature, srcData);

                                srcData = generateSequencerCommandProperties(feature, srcData);

                                srcData = srcData.Replace(this.taskIdentifier, feature.getName());

                                byte[] buffer = Encoding.UTF8.GetBytes(srcData);

                                foreach (mapp_meta.MpSequencer dest in sequencerConfiguration)
                                {
                                    using (MemoryStream stream = new MemoryStream(buffer))
                                    {
                                        XmlSerializer srcSerializer = new XmlSerializer(typeof(mapp_meta.Configuration));
                                        dest.addElements((mapp_meta.Configuration)srcSerializer.Deserialize(stream));
                                    }
                                }
                            }                          

                            public override void configure(ProjectFeature feature)
                            {
                                string[] paths = Directory.GetDirectories(productSrc + @"\", feature.getID(), SearchOption.AllDirectories);
                                string path = paths[0];

                                string[] alarmFiles = Directory.GetFiles(path, "*" + ProjectFeature.ConfigurationObject.Mapp.SEQUENCER, SearchOption.AllDirectories);

                                foreach (string srcPath in alarmFiles)
                                {

                                    String srcData = File.ReadAllText(srcPath);

                                    foreach (ReplacePattern pattern in replacePatterns)
                                    {
                                        srcData = srcData.Replace(pattern.find, pattern.replace);
                                    }

                                    foreach (KeyValuePair<string, WildcardMethods.WildcardDelegate> pair in ProjectFeature.wildcards)
                                    {
                                        string replac = pair.Key;
                                        srcData = Regex.Replace(srcData, replac, pair.Value(feature));
                                    }

                                    srcData = generateStaticInterlocks(feature, srcData);

                                    srcData = generateDynamicInterlocks(feature, srcData);

                                    srcData = srcData.Replace(this.taskIdentifier, feature.getName());

                                    byte[] buffer = Encoding.UTF8.GetBytes(srcData);

                                    foreach (mapp_meta.MpSequencer dest in sequencerConfiguration)
                                    {
                                        using (MemoryStream stream = new MemoryStream(buffer))
                                        {
                                            XmlSerializer srcSerializer = new XmlSerializer(typeof(mapp_meta.Configuration));
                                            dest.addElements((mapp_meta.Configuration)srcSerializer.Deserialize(stream));
                                        }
                                    }
                                }

                            }
                        }
                        [XmlRootAttribute("Codebox")]
                        public class Codebox : MappElement
                        {
                            public override string extension
                            {
                                get { return CODEBOX; }
                            }
                            public override string prefix
                            {
                                get { return "cb"; }
                            }

                            public override string type
                            {
                                get { return MAPP_SERVICES; }
                            }
                            public Codebox()
                                : base()
                            {

                            }

                            public override void configure(ProjectFeature feature, string srcData, string filename)
                            {
                               foreach (ReplacePattern pattern in replacePatterns)
                                {
                                    srcData = srcData.Replace(pattern.find, pattern.replace);
                                }
                                srcData = srcData.Replace(this.taskIdentifier, feature.getName());

                                byte[] buffer = Encoding.UTF8.GetBytes(srcData);

                                using (MemoryStream stream = new MemoryStream(buffer))
                                {

                                    XmlSerializer srcSerializer = new XmlSerializer(typeof(mapp_meta.Configuration));
                                    codeboxConfiguration.addElements((mapp_meta.Configuration)srcSerializer.Deserialize(stream));
                                }
                            }

                            public override void configure(ProjectFeature feature)
                            {
                                string[] paths = Directory.GetDirectories(productSrc + @"\", feature.getID(), SearchOption.AllDirectories);
                                string path = paths[0];

                                string[] alarmFiles = Directory.GetFiles(path, "*" + ProjectFeature.ConfigurationObject.Mapp.CODEBOX, SearchOption.AllDirectories);
                                string srcPath = alarmFiles[0];

                                String srcData = File.ReadAllText(srcPath);
                                srcData = srcData.Replace(this.taskIdentifier, feature.getName());

                                byte[] buffer = Encoding.UTF8.GetBytes(srcData);

                                using (MemoryStream stream = new MemoryStream(buffer))
                                {

                                    XmlSerializer srcSerializer = new XmlSerializer(typeof(mapp_meta.Configuration));
                                    codeboxConfiguration.addElements((mapp_meta.Configuration)srcSerializer.Deserialize(stream));
                                }
                            }
                        }
                        [XmlRootAttribute("Group")]
                        public class Group : MappElement
                        {
                            public override string extension
                            {
                                get { return GROUP; }
                            }
                            public override string prefix
                            {
                                get { return "gr"; }
                            }

                            public override string type
                            {
                                get { return MAPP_SERVICES; }
                            }
                            public Group()
                                : base()
                            {

                            }

                            public override void configure(ProjectFeature feature, string srcData, string filename)
                            {
                                foreach (ReplacePattern pattern in replacePatterns)
                                {
                                    srcData = srcData.Replace(pattern.find, pattern.replace);
                                }
                                srcData = srcData.Replace(this.taskIdentifier, feature.getName());

                                byte[] buffer = Encoding.UTF8.GetBytes(srcData);

                                using (MemoryStream stream = new MemoryStream(buffer))
                                {

                                    XmlSerializer srcSerializer = new XmlSerializer(typeof(mapp_meta.Configuration));
                                    groupConfiguration.addElements((mapp_meta.Configuration)srcSerializer.Deserialize(stream));
                                }
                            }

                            public override void configure(ProjectFeature feature)
                            {
                                string[] paths = Directory.GetDirectories(productSrc + @"\", feature.getID(), SearchOption.AllDirectories);
                                string path = paths[0];

                                string[] alarmFiles = Directory.GetFiles(path, "*" + ProjectFeature.ConfigurationObject.Mapp.GROUP, SearchOption.AllDirectories);
                                string srcPath = alarmFiles[0];

                                String srcData = File.ReadAllText(srcPath);
                                srcData = srcData.Replace(this.taskIdentifier, feature.getName());

                                byte[] buffer = Encoding.UTF8.GetBytes(srcData);

                                using (MemoryStream stream = new MemoryStream(buffer))
                                {

                                    XmlSerializer srcSerializer = new XmlSerializer(typeof(mapp_meta.Configuration));
                                    groupConfiguration.addElements((mapp_meta.Configuration)srcSerializer.Deserialize(stream));
                                }
                            }
                        }
                    }                  
                }
            }
        }
    }
}