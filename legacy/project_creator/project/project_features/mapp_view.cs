﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text.RegularExpressions;
using mv_vis;
using codegenLog;
using System.Xml.Linq;
using package;

namespace project_creator
{
    partial class Project
    {     
        public partial class ProjectFeature
        {
            public abstract partial class ConfigurationObject
            {
                [XmlRootAttribute("MappView")]
                public class MappView : ConfigurationObject
                {          
                    //list of config objects for the as project, such as OPC UA map, tasks,...
                    [XmlArray("Elements")]
                    [XmlArrayItem("Element")]
                    public List<Element> mvElements = new List<Element>();

                    public override void configure(ProjectFeature feature)
                    {
                        if (conditionsTrue(feature) == false)
                        {
                            return;
                        }
                        foreach (Element element in mvElements)
                        {
                            element.configure(feature);
                        }                       
                    }

                    public MappView()
                        : base()
                    {

                    }
                    private static bool nodeIsChild(XmlNode parent, XmlNode inst)
                    {
                        foreach (XmlAttribute attribute in inst.Attributes)
                        {
                            if (attribute.Name == "id")
                            {
                                if (getNodeById(parent, attribute.Value) != null)
                                {
                                    return true;
                                }
                                break;
                            }                          
                        }
                        return false;
                    }

                    private static bool zIndexIsUnique(XmlNodeList nodes, string zIndex)
                    {
                        foreach (XmlNode node in nodes)
                        {
                            foreach(XmlAttribute attribute in node.Attributes)
                            {
                                if (attribute.Name == "zIndex" && attribute.Value == zIndex)
                                {
                                    return false;
                                }
                            }

                            if (node.HasChildNodes == true)
                            {
                                if (zIndexIsUnique(node.ChildNodes, zIndex) == false)
                                {
                                    return false;
                                }
                            }

                        }                        

                        return true;
                    }
                    private static XmlNode getNodeById(XmlNode root, string id)
                    {
                        XmlNode result = null;
                        foreach (XmlNode node in root.ChildNodes)
                        {
                            foreach (XmlAttribute attribute in node.Attributes)
                            {
                                if (attribute.Name == "id" && attribute.Value == id)
                                {
                                    return node;
                                }
                            }

                            if (( result = getNodeById(node, id)) != null)
                            {
                                return result;
                            }
                        }

                        return result;
                    }
                    private static string getUniqueZIndex(XmlNodeList nodes)
                    {
                        string zIndex = "0";

                        while (true)
                        {
                            Random rand = new Random();

                            zIndex = rand.Next(0, 32767).ToString();

                            if (zIndexIsUnique(nodes, zIndex) == true)
                            {
                                break;
                            }                           
                        }
                        return zIndex;
                    }

                    private const string CONTENT = ".content";
                    private const string BINDING = ".binding";
                    private const string PAGE = ".page";
                    private const string EVENTBINDING = ".eventbinding";
                    private const string SNIPPET = ".snippet";
                    private const string SESSION_VARIABLE = ".svar";
                    private const string EXPRESSION = ".expression";
                    private const string DIALOG = ".dialog";
                    private const string TEXT = ".tmx";

                    public static MappView.Element generateObject(string file, Element.MappViewPackage parent = null)
                    {
                        string extension = Path.GetExtension(file);

                        switch (extension)
                        {
                            case CONTENT:

                                bool preCache = false;

                                if (parent != null && parent.contentPreCachePattern != null)
                                {
                                    foreach (string pattern in parent.contentPreCachePattern)
                                    {
                                        if (Regex.IsMatch(Path.GetFileName(file), pattern) == true)
                                        {
                                            preCache = true;
                                            break;
                                        }
                                    }
                                }

                                return new ConfigurationObject.MappView.Element.Content(preCache);

                                //new MappView.Element.Content()

                                //break;

                            case BINDING:

                                return new ConfigurationObject.MappView.Element.Binding();
                                //break;

                            case PAGE:
                                return new ConfigurationObject.MappView.Element.Page();
                            //break;

                            case EVENTBINDING:
                                  return new ConfigurationObject.MappView.Element.Eventbinding();
                                //break;

                            case SNIPPET:
                                  return new ConfigurationObject.MappView.Element.Snippet();
                            //break;

                            case SESSION_VARIABLE:
                                  return new ConfigurationObject.MappView.Element.SessionVariable();
                            //break;

                            case DIALOG:
                                  return new ConfigurationObject.MappView.Element.Dialog();
                            //break;

                            case EXPRESSION:
                                  return new ConfigurationObject.MappView.Element.Expressions();

                            case TEXT:
                                return new ConfigurationObject.MappView.Element.Text();
                            //break;
      
                            default:
                                return null;
                            //break;
                        }

                        //return null;
                    }

                    [XmlInclude(typeof(Content))]
                    [XmlInclude(typeof(ContentSnippet))]
                    [XmlInclude(typeof(Binding))]
                    [XmlInclude(typeof(Eventbinding))]
                    [XmlInclude(typeof(SessionVariable))]
                    [XmlInclude(typeof(BindingList))]
                    [XmlInclude(typeof(Snippet))]
                    [XmlInclude(typeof(Page))]
                    [XmlInclude(typeof(Text))]
                    [XmlInclude(typeof(Expressions))]
                    [XmlInclude(typeof(Media))]
                    [XmlInclude(typeof(MappViewPackage))]
                    [XmlInclude(typeof(Dialog))]
                    public abstract class Element
                    {

                        //list of config objects for the as project, such as OPC UA map, tasks,...
                        [XmlArray("ReplacePatterns")]
                        [XmlArrayItem("Pattern")]
                        public List<ReplacePattern> replacePatterns = new List<ReplacePattern>();

                        public WildcardOverrideList wildcardOverrides;

                        protected abstract string visGroup { get; }

                        protected abstract string destDir { get; }

                        protected abstract string packageType { get; }

                        protected abstract string parentPackageType { get; }

                        //is replaced with the generatexd component name
                        [XmlAttribute("TaskIdentifier")]
                        public string taskIdentifier;

                        [XmlAttribute("Target")]
                        public string target;

                        protected virtual void addtoConfiguration(string id, ProjectFeature feature)
                        {
                            vis.addElement(visGroup, id);
                        }

                        public abstract void configure(ProjectFeature feature);

                        public virtual void configure(ProjectFeature feature, String srcData, String filename)
                        {
                            string rootDir = destDir;
                            string typeDir = rootDir + @"\" + feature.componentType;
                            string componentDir = typeDir + @"\" + feature.componentName;

                            string rootPkgDir = Directory.GetFiles(rootDir, "*.pkg", SearchOption.TopDirectoryOnly)[0];
                            Package rootPkg = Package.load(rootPkgDir);

                            //create folder for type and component
                            if (Directory.Exists(componentDir) == false)
                            {
                                Directory.CreateDirectory(componentDir);

                                /*add folder for the feature type if not existing*/

                                //add package for this feature type
                                rootPkg.addElement(Package.OBJECTS_GROUP, feature.componentType, new XAttribute[]
                                {
                                    new XAttribute(Package.TYPE_ATTRIBUTE, Package.TYPE_ATTRIBUTE_PACKAGE),
                                    new XAttribute(Package.DESCRIPTION_ATTRIBUTE, "Auto generated"),
                                });

                                rootPkg.setSubType(parentPackageType);
                                rootPkg.save(rootPkgDir);
                            }

                            /*add folder to the component if not exisitng*/
                            string typePkgDir;
                            Package typePkg;
                            try
                            {
                                typePkgDir = Directory.GetFiles(typeDir, "*.pkg", SearchOption.TopDirectoryOnly)[0];
                                typePkg = Package.load(typePkgDir);
                            }
                            catch (IndexOutOfRangeException)
                            {
                                typePkgDir = typeDir + @"/Package.pkg";

                                //load last used package and clear it of childern to have a reference
                                typePkg = Package.load(rootPkgDir);
                                typePkg.clearGroup(Package.OBJECTS_GROUP);

                                //typePkg.packageType = "EmptyMappViewPackage";
                            }

                            //add package for this instance
                            typePkg.addElement(Package.OBJECTS_GROUP, feature.componentName, new XAttribute[]
                            {
                                    new XAttribute(Package.TYPE_ATTRIBUTE, Package.TYPE_ATTRIBUTE_PACKAGE),
                                    new XAttribute(Package.DESCRIPTION_ATTRIBUTE, feature.getID()),
                            });

                            rootPkg.setSubType(parentPackageType);
                            typePkg.save(typePkgDir);

                            /*add folder to the component if not exisitng*/
                            string componentPkgDir;
                            package.Package componentPkg;
                            try
                            {
                                componentPkgDir = Directory.GetFiles(componentDir, "*.pkg", SearchOption.TopDirectoryOnly)[0];
                                componentPkg = Package.load(componentPkgDir);
                            }
                            catch (IndexOutOfRangeException)
                            {
                                componentPkgDir = componentDir + @"/Package.pkg";


                                //load last used package and clear it of childern to have a reference
                                componentPkg = Package.load(rootPkgDir);
                                componentPkg.clearGroup(Package.OBJECTS_GROUP);
                            }

                            foreach (KeyValuePair<string, WildcardMethods.WildcardDelegate> pair in ProjectFeature.wildcards)
                            {
                                string replac = wildcardOverrides != null ? wildcardOverrides[pair.Key] : pair.Key;
                                filename = Regex.Replace(filename, replac, pair.Value(feature));
                            }

                            foreach (ReplacePattern pattern in replacePatterns)
                            {
                                filename = Regex.Replace(filename, pattern.find, pattern.replace);
                            }

                            filename = filename.Replace(taskIdentifier, feature.getName());

                            componentPkg.addElement(Package.OBJECTS_GROUP, filename, new XAttribute[]
                            {
                                    new XAttribute(Package.TYPE_ATTRIBUTE, Package.TYPE_ATTRIBUTE_FILE),
                                    new XAttribute(Package.DESCRIPTION_ATTRIBUTE, feature.getID()),
                            });

                            rootPkg.setSubType(packageType);
                            componentPkg.save(componentPkgDir);

                            foreach (KeyValuePair<string, WildcardMethods.WildcardDelegate> pair in ProjectFeature.wildcards)
                            {
                                string replac = wildcardOverrides != null ? wildcardOverrides[pair.Key] : pair.Key;
                                srcData = Regex.Replace(srcData, replac, pair.Value(feature));
                            }

                            foreach (ReplacePattern pattern in replacePatterns)
                            {
                                srcData = Regex.Replace(srcData, pattern.find, pattern.replace);
                            }


                            srcData = srcData.Replace(this.taskIdentifier, feature.getName());
                            File.WriteAllText(componentDir + @"/" + filename, srcData);

                            XDocument doc = XDocument.Parse(srcData);

                            XAttribute idAttribute = doc.Root.Attribute("id");

                            //pass id if valid id found
                            string pageId = idAttribute == null ? filename : idAttribute.Value;

                            addtoConfiguration(pageId, feature);

                        }

                        private string getVisPath()
                        {
                            return Directory.GetFiles(dest + @"\Physical\" + config, @"*.vis", SearchOption.AllDirectories)[0];
                        }

                        [XmlRootAttribute("MappView Package")]
                        public class MappViewPackage : Element
                        {
                            [XmlAttribute("SourcePath")]
                            public string destPath;

                            public List<String> contentPreCachePattern;

                            protected override string destDir
                            {
                                get
                                {
                                    throw new NotImplementedException();
                                }
                            }

                            protected override string visGroup
                            {
                                get
                                {
                                    throw new NotImplementedException();
                                }
                            }

                            protected override string packageType
                            {
                                get
                                {
                                    throw new NotImplementedException();
                                }
                            }

                            protected override string parentPackageType
                            {
                                get
                                {
                                    throw new NotImplementedException();
                                }
                            }

                            public override void configure(ProjectFeature feature, string srcData, string filename)
                            {
                                throw new NotImplementedException();
                            }

                            public override void configure(ProjectFeature feature)
                            {
                                string[] files = Directory.GetFiles(dest + @"/" + destPath, "*", SearchOption.TopDirectoryOnly);

                                foreach (string file in files)
                                {
                                    MappView.Element element = MappView.generateObject(file, this);

                                    if (element == null)
                                    {
                                        continue;
                                    }

                                    element.taskIdentifier = this.taskIdentifier;
                                    element.target = this.target;
                                    element.replacePatterns = this.replacePatterns;
                                    element.wildcardOverrides = this.wildcardOverrides;

                                    string srcData = File.ReadAllText(file);

                                    element.configure(feature, srcData, Path.GetFileName(file));
                                }

                            }

                        }

                        //logical view, enter to vis file
                        public class Page : Element
                        {
                            [XmlAttribute("Source")]
                            public string srcFile;

                            protected override string destDir
                            {
                                get
                                {
                                    return dest + @"\Logical\mappView\" + target + @"\Pages";
                                }
                            }

                            protected override string packageType
                            {
                                get
                                {
                                    return Package.SUB_TYPE_ATTRIBUTE_PAGES;
                                }
                            }

                            protected override string parentPackageType
                            {
                                get
                                {
                                    return Package.SUB_TYPE_ATTRIBUTE_EMPTY_MV;
                                }
                            }

                            protected override string visGroup
                            {
                                get
                                {
                                    return mv_vis.Visualization.PAGES_GROUP;
                                }
                            }

                            public override void configure(ProjectFeature feature)
                            {

                                throw new NotImplementedException();
                            }
                        }

                        //logical view, enter to vis file
                        public class Content : Element
                        {
                            [XmlAttribute("Source")]
                            public string srcFile;
                            private bool preCache;

                            protected override string visGroup
                            {
                                get
                                {
                                    return Visualization.CONTENTS_GROUP;
                                }
                            }
                            protected override string parentPackageType
                            {
                                get
                                {
                                    return Package.SUB_TYPE_ATTRIBUTE_EMPTY_MV;
                                }
                            }
                            protected override string packageType
                            {
                                get
                                {
                                    return Package.SUB_TYPE_ATTRIBUTE_PAGES;
                                }
                            }

                            protected override string destDir
                            {
                                get
                                {
                                    return dest + @"\Logical\mappView\" + target + @"\Pages"; ;
                                }
                            }

                            protected override void addtoConfiguration(string id, ProjectFeature feature)
                            {
                                XAttribute attribute = new XAttribute(Visualization.CONTENT_PRE_CACHE_ATTRIBUTE, preCache);

                                Logging.enterContext(vis);
                                vis.addElement(Visualization.CONTENTS_GROUP, id, new XAttribute[] { attribute, });
                                Logging.leaveContext();
                            }

                            public override void configure(ProjectFeature feature)
                            {
                                throw new NotImplementedException();
                            }

                            public Content() { }

                            public Content(bool preCache)
                            {
                                this.preCache = preCache;
                            }
                       
                        }
                        public class Dialog : Element
                        {
                            [XmlAttribute("Source")]
                            public string srcFile;

                            protected override string visGroup
                            {
                                get
                                {
                                    return Visualization.DIALOGS_GROUP;
                                }
                            }
                            protected override string parentPackageType
                            {
                                get
                                {
                                    return null;
                                }
                            }

                            protected override string packageType
                            {
                                get
                                {
                                    return Package.SUB_TYPE_ATTRIBUTE_DIALOGS;
                                }
                            }

                            protected override string destDir
                            {
                                get
                                {
                                    return dest + @"\Logical\mappView\" + target + @"\Dialogs";
                                }
                            }

                            public override void configure(ProjectFeature feature)
                            {
                                throw new NotImplementedException();
                            }
                        }
                        //enter to vis file
                        public class BindingList : Element
                        {
                            protected override string destDir
                            {
                                get
                                {
                                    throw new NotImplementedException();
                                }
                            }
                            protected override string parentPackageType
                            {
                                get
                                {
                                    return null;
                                }
                            }
                            protected override string packageType
                            {
                                get
                                {
                                    return null;
                                }
                            }

                            protected override string visGroup
                            {
                                get
                                {
                                    throw new NotImplementedException();
                                }
                            }

                            public override void configure(ProjectFeature feature)
                            {
                                throw new NotImplementedException();
                            }

                            public override void configure(ProjectFeature feature, string srcData, string filename)
                            {
                                throw new NotImplementedException();
                            }

                        }
                        //enter to vis file
                        public class Eventbinding : Element
                        {
                            [XmlAttribute("Source")]
                            public string srcFile;

                            protected override string visGroup
                            {
                                get
                                {
                                    return Visualization.EVENT_BINDINGS_SETS_GROUP;
                                }
                            }
                            protected override string parentPackageType
                            {
                                get
                                {
                                    return null;
                                }
                            }

                            protected override string packageType
                            {
                                get
                                {
                                    return null;
                                }
                            }

                            protected override string destDir
                            {
                                get
                                {
                                    return Directory.GetDirectories(dest + @"\Physical\" + config, "mappView", SearchOption.AllDirectories)[0];
                                }
                            }

                            public override void configure(ProjectFeature feature)
                            {
                                throw new NotImplementedException();
                            }
                        }
                        //enter to vis file
                        public class SessionVariable : Element
                        {
                            [XmlAttribute("Source")]
                            public string srcFile;

                            protected override string visGroup
                            {
                                get
                                {
                                    return Visualization.VARIABLES_SETS_GROUP;
                                }
                            }
                            protected override string parentPackageType
                            {
                                get
                                {
                                    return packageType;
                                }
                            }

                            protected override string packageType
                            {
                                get
                                {
                                    return Package.SUB_TYPE_ATTRIBUTE_VARIABLES;
                                }
                            }

                            protected override string destDir
                            {
                                get
                                {
                                    return dest + @"\Logical\mappView\" + target + @"\Variables";
                                }
                            }

                            public override void configure(ProjectFeature feature)
                            {
                                throw new NotImplementedException();
                            }
                        }
                        public class Binding : Element
                        {
                            [XmlAttribute("Source")]
                            public string srcFile;

                            protected override string visGroup
                            {
                                get
                                {
                                    return Visualization.BINDIGS_SET_GROUP;
                                }
                            }
                            protected override string parentPackageType
                            {
                                get
                                {
                                    return null;
                                }
                            }

                            protected override string packageType
                            {
                                get
                                {
                                    return null;
                                }
                            }

                            protected override string destDir
                            {
                                get
                                {
                                    return Directory.GetDirectories(dest + @"\Physical\" + config, "mappView", SearchOption.AllDirectories)[0];
                                }
                            }

                            public override void configure(ProjectFeature feature)
                            {
                                throw new NotImplementedException();
                            }
                        }
                        //enter to vis file
                        public class Snippet : Element
                        {
                            [XmlAttribute("Source")]
                            public string srcFile;

                            protected override string visGroup
                            {
                                get
                                {
                                    return Visualization.SNIPPETS_SETS_GROUP;
                                }
                            }
                            protected override string parentPackageType
                            {
                                get
                                {
                                    return packageType;
                                }
                            }

                            protected override string packageType
                            {
                                get
                                {
                                    return Package.SUB_TYPE_ATTRIBUTE_SNIPPETS;
                                }
                            }

                            protected override string destDir
                            {
                                get
                                {
                                    return dest + @"\Logical\mappView\" + target + @"\Resources\Snippets";
                                }
                            }

                            public override void configure(ProjectFeature feature)
                            {
                                throw new NotImplementedException();
                            }
                        }
                        //logical view, enter to textsystem
                        public class Text : Element
                        {
                            [XmlAttribute("Source")]
                            public string srcFile;

                            protected override string visGroup
                            {
                                get
                                {
                                    throw new NotImplementedException();
                                }
                            }

                            protected override string packageType
                            {
                                get
                                {
                                    return null;
                                }
                            }
                            protected override string parentPackageType
                            {
                                get
                                {
                                    return null;
                                }
                            }

                            protected override string destDir
                            {
                                get
                                {
                                    return dest + @"\Logical\mappView\" + target + @"\Resources\Texts";
                                }
                            }

                            protected override void addtoConfiguration(string id, ProjectFeature feature)
                            {                             
                                string typeDir = destDir + @"\" + feature.componentType;
                                string componentDir = typeDir + @"\" + feature.componentName;
                                textConfig.addTmx(componentDir + @"/" + id);
                            }

                            public override void configure(ProjectFeature feature)
                            {
                                throw new NotImplementedException();
                            }
                        }
                        //logical view, enter to vis file
                        public class Expressions : Element
                        {
                            [XmlAttribute("Source")]
                            public string srcFile;

                            protected override string packageType
                            {
                                get
                                {
                                    return Package.SUB_TYPE_ATTRIBUTE_EXPRESSIONS;
                                }
                            }

                            protected override string parentPackageType
                            {
                                get
                                {
                                    return packageType;
                                }
                            }

                            protected override string visGroup
                            {
                                get
                                {
                                    return Visualization.EXPRESSIONS_SETS_GROUP;
                                }
                            }

                            protected override string destDir
                            {
                                get
                                {
                                    return dest + @"\Logical\mappView\" + target + @"\Expressions";
                                }
                            }

                            public override void configure(ProjectFeature feature)
                            {
                                throw new NotImplementedException();
                            }
                        }
                        //logical view
                        public class Media : Element
                        {
                            protected override string destDir
                            {
                                get
                                {
                                    throw new NotImplementedException();
                                }
                            }

                            protected override string packageType
                            {
                                get
                                {
                                    return Package.SUB_TYPE_ATTRIBUTE_MEDIA;
                                }
                            }

                            protected override string parentPackageType
                            {
                                get
                                {
                                    return packageType;
                                }
                            }

                            protected override string visGroup
                            {
                                get
                                {
                                    throw new NotImplementedException();
                                }
                            }

                            public override void configure(ProjectFeature feature)
                            {
                                throw new NotImplementedException();
                            }

                            public override void configure(ProjectFeature feature, string srcData, string filename)
                            {
                                throw new NotImplementedException();
                            }
                        }
                        //logical view
                        public class ContentSnippet : Element
                        {
                            [XmlAttribute("DestinationContent")]
                            public string destContent;
                            [XmlAttribute("DestinationID")]
                            public string destID;
                            [XmlAttribute("SourceContent")]
                            public string sourceFile;
                            [XmlAttribute("Position")]
                            public Position position;

                            protected override string visGroup
                            {
                                get
                                {
                                    throw new NotImplementedException();
                                }
                            }

                            protected override string packageType
                            {
                                get
                                {
                                    throw new NotImplementedException();
                                }
                            }

                            protected override string destDir
                            {
                                get
                                {
                                    throw new NotImplementedException();
                                }
                            }

                            protected override string parentPackageType
                            {
                                get
                                {
                                    throw new NotImplementedException();
                                }
                            }

                            public enum Position
                            {
                                INSIDE,
                                BEFORE,
                                AFTER,
                            }

                            public override void configure(ProjectFeature feature, string srcData, string filename)
                            {
                                throw new NotImplementedException();
                            }

                            public override void configure(ProjectFeature feature)
                            {
                                String destPath = dest + @"\Logical\mappView\" + @"\" + target + @"\Pages\" + destContent;

                                foreach (KeyValuePair<string, WildcardMethods.WildcardDelegate> pair in ProjectFeature.wildcards)
                                {
                                    string replac = wildcardOverrides != null ? wildcardOverrides[pair.Key] : pair.Key;                                   
                                    destPath = Regex.Replace(destPath, replac, pair.Value(feature));
                                }

                                foreach (ReplacePattern pattern in replacePatterns)
                                {
                                    destPath = Regex.Replace(destPath, pattern.find, pattern.replace);
                                }

                                foreach (KeyValuePair<string, WildcardMethods.WildcardDelegate> pair in ProjectFeature.wildcards)
                                {
                                    string replac = wildcardOverrides != null ? wildcardOverrides[pair.Key] : pair.Key;                                    
                                    destID = Regex.Replace(destID, replac, pair.Value(feature));
                                }

                                foreach (ReplacePattern pattern in replacePatterns)
                                {
                                    destID = Regex.Replace(destID, pattern.find, pattern.replace);
                                }

                                //destPath = destPath.Replace(taskIdentifier, feature.componentName);

                                String destData = File.ReadAllText(destPath);

                                //get source content                              
                                string srcDir;
                                string srcPath;

                                string[] sources = Directory.GetDirectories(productSrc, feature.getID(), SearchOption.AllDirectories);


                                srcDir = sources[0];
                                sources = Directory.GetFiles(srcDir, sourceFile, SearchOption.AllDirectories);

                                if (sources.Length == 1)
                                {
                                    srcPath = sources[0];
                                }
                                //only if no file in own directory found, look in parent directories
                                else
                                {
                                    srcPath = "";

                                    foreach (string product in feature.parents)
                                    {
                                        sources = Directory.GetDirectories(productSrc, product, SearchOption.AllDirectories);

                                        srcDir = sources[0];

                                        sources = Directory.GetFiles(srcDir, sourceFile, SearchOption.AllDirectories);

                                        if (sources.Length == 1)
                                        {
                                            srcPath = sources[0];

                                            break;
                                        }
                                    }

                                    if (srcPath == "")
                                    {
                                        throw new ArgumentException("source content not found");
                                    }
                                }

                                string srcData = File.ReadAllText(srcPath);

                                foreach (KeyValuePair<string, WildcardMethods.WildcardDelegate> pair in ProjectFeature.wildcards)
                                {
                                    string replac = wildcardOverrides != null ? wildcardOverrides[pair.Key] : pair.Key;
                                    srcData = Regex.Replace(srcData, replac, pair.Value(feature));
                                }

                                foreach (ReplacePattern pattern in replacePatterns)
                                {
                                    srcData = Regex.Replace(srcData, pattern.find, pattern.replace);
                                }

                                srcData = srcData.Replace(taskIdentifier, feature.componentName);

                                XmlDocument srcDoc = new XmlDocument();
                                srcDoc.LoadXml(srcData);

                                XmlNode srcNode = srcDoc.DocumentElement.FirstChild;

                                XmlDocument destDoc = new XmlDocument();
                                destDoc.Load(destPath);

                                XmlNode destNode = null;
                                XmlElement destElement = destDoc.DocumentElement;
                                destNode = destElement.GetElementsByTagName("Widgets")[0];
                                destNode = getNodeById(destNode, destID);

                                XmlNode refNode = null;

                                if (position != Position.INSIDE)
                                {
                                    refNode = destNode;
                                    destNode = destNode.ParentNode;
                                }


                                /* using (XmlReader reader = XmlReader.Create(new StringReader(destData)))
                                 {

                                     srcDoc.LoadXml(srcData);
                                     srcNode = srcDoc.DocumentElement.FirstChild;

                                     while (reader.Read())
                                     {
                                         if (reader.GetAttribute("id") == destID)
                                         {
                                             destDoc = new XmlDocument();
                                             destDoc.Load(reader);
                                             destElement = destDoc.DocumentElement;
                                             //destElement = destElement.GetElementsByTagName("Widgets")
                                             destNode = destElement.GetElementsByTagName("Widgets")[0];                                        
                                             break;
                                         }
                                     }
                                 }*/

                                foreach (XmlAttribute attribute in srcNode.Attributes)
                                {
                                    if (attribute.Name == "zIndex")
                                    {

                                        attribute.Value = getUniqueZIndex(destNode.ChildNodes);
                                        break;
                                    }
                                }

                                XmlNode importNode = destElement.OwnerDocument.ImportNode(srcNode, true);

                                foreach (XmlNode node in destNode.ChildNodes)
                                {
                                    if (node.Name == "Widgets")
                                    {
                                        destNode = node;
                                        break;
                                    }
                                }

                                if (nodeIsChild(destNode, importNode) == false)
                                {
                                    

                                    switch (position)
                                    {
                                        case Position.AFTER:
                                            destNode.InsertAfter(importNode, refNode);

                                            break;

                                        case Position.BEFORE:
                                            destNode.InsertBefore(importNode, refNode);

                                            break;

                                        default:
                                            destNode.AppendChild(importNode);

                                            break;
                                    }

                                    destDoc.Save(destPath);
                                }

                                //vis.addContent(new mv_vis.Content(doc.DocumentElement.GetAttribute("id")));*/


                                //File.WriteAllText(componentDir + @"/" + fileName, srcData);
                            }
                        }
                    }                  
                }
            }
        }
    }
}