﻿
using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
namespace project_creator
{
    partial class Project
    {
        public partial class ProjectFeature
        {
            public abstract partial class ConfigurationObject
            {
                [XmlRootAttribute("Alarms")]
                public class Alarms : ConfigurationObject
                {
                    [XmlAttribute("TaskIdentifier")]
                    public string taskIdentifier;

                    public Alarms()
                        : base()
                    {

                    }

                    public override void configure(ProjectFeature feature)
                    {
                        string[] paths = Directory.GetDirectories(productSrc + @"\", feature.getID(), SearchOption.AllDirectories);
                        string path = paths[0];

                        string[] alarmFiles = Directory.GetFiles(path, "*.mpalarmxcore", SearchOption.AllDirectories);
                        string srcPath = alarmFiles[0];

                        String srcData = File.ReadAllText(srcPath);
                        srcData = srcData.Replace(this.taskIdentifier, feature.getName());

                        byte[] buffer = Encoding.UTF8.GetBytes(srcData);

                        using (MemoryStream stream = new MemoryStream(buffer))
                        {

                            XmlSerializer srcSerializer = new XmlSerializer(typeof(mapp_meta.Configuration));
                            alarmConfiguration.addElements((mapp_meta.Configuration)srcSerializer.Deserialize(stream));
                        }

                    }
                }
            }
        }
    }
}