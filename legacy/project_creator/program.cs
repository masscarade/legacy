﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text.RegularExpressions;
using mapp_meta;
using System.Windows;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.Serialization;
using codegenLog;
using System.Threading;

public static class Methods
{
    public static void DeleteDirecotry(string dir)
    {
        Logging.log(string.Format("delete {0}", dir), LEVEL.INFO);

        string[] files = Directory.GetFiles(dir, "*", SearchOption.AllDirectories);

        int i = 1;
        int count = files.Length;

        foreach (var file in files)
        {
            FileInfo info = new FileInfo(file);

            File.SetAttributes(file, FileAttributes.Normal);

            if (i % 1000 == 0 || i == count)
            {
                Logging.status(string.Format("delete {0}/{1} {2}", i, count, info.Name), LEVEL.INFO, ConsoleColor.Red);
            }

            i++;
        }

        Directory.Delete(dir, true);

        Logging.endStatus();
    }

    private class CloneWorker
    {
        private int totalFiles;
        private int count;
        private string src;
        private string dest;
        private bool done;

        public CloneWorker(string destination, string source)
        {
            dest = destination;
            src = source;
            totalFiles = Directory.GetFiles(src, "*", SearchOption.AllDirectories).Length;
            count = 1;
        }

        public void start()
        {
            /*Thread thread = new Thread(this.clone);

            done = false;

            thread.Start();

            while (done == false)
            {
                Thread.Sleep(1000);
            }
            thread.Join();*/

            clone();
        }

        private void clone()
        {
            done = false;
            clone(dest);
            done = true;
        }

        private void clone(string destination, string subRoot = null)
        {
            string root = subRoot == null ? src : subRoot;

            foreach (var directory in Directory.GetDirectories(root))
            {
                string dirName = Path.GetFileName(directory);
                if (!Directory.Exists(Path.Combine(destination, dirName)))
                {
                    Directory.CreateDirectory(Path.Combine(destination, dirName));
                }

                clone(Path.Combine(destination, dirName), directory);
            }

            string[] files = Directory.GetFiles(root);

            foreach (var file in files)
            {
                string fileDest = Path.Combine(destination, Path.GetFileName(file));

                FileInfo info = new FileInfo(file);
                FileInfo destInfo = new FileInfo(fileDest);

                if (destInfo.Exists == true)
                {
                    Logging.log(string.Format("file already exists {0}", root), LEVEL.WARNING);

                    File.Delete(destInfo.FullName);
                    Logging.log(string.Format("deleted {0}", destInfo.Name), LEVEL.WARNING);
                }

                File.Copy(file, fileDest);

                if (count % 1000 == 0 || count == totalFiles)
                {
                    Logging.status(string.Format("clone {0}/{1} {2}", count, totalFiles, info.Name), LEVEL.INFO, ConsoleColor.Cyan);
                }

                count++;
            }

            //Console.WriteLine("sleep a bit..");
            //Thread.Sleep(100);
        }
    }

    public static void CloneDirectory(string root, string dest)
    {
        Logging.log(string.Format("clone {0}", root), LEVEL.INFO);
        CloneWorker worker = new CloneWorker(dest, root);

        worker.start();

        Logging.endStatus();


    }
    public static string PrintXML(string xml)
    {
        string result = "";

        MemoryStream mStream = new MemoryStream();
        XmlTextWriter writer = new XmlTextWriter(mStream, Encoding.Unicode);
        XmlDocument document = new XmlDocument();

        try
        {
            // Load the XmlDocument with the XML.
            document.LoadXml(xml);

            writer.Formatting = Formatting.Indented;

            // Write the XML into a formatting XmlTextWriter
            document.WriteContentTo(writer);
            writer.Flush();
            mStream.Flush();

            // Have to rewind the MemoryStream in order to read
            // its contents.
            mStream.Position = 0;

            // Read MemoryStream contents into a StreamReader.
            StreamReader sReader = new StreamReader(mStream);

            // Extract the text from the StreamReader.
            string formattedXml = sReader.ReadToEnd();

            result = formattedXml;
        }
        catch (XmlException)
        {
            // Handle the exception
        }

        mStream.Close();
        writer.Close();

        return result;
    }
}

namespace project_creator
{
    public class Settings
    {
        public string featureList;
        public string productSrc;
        public string destProject;
        public CopyProject copyProject;
        public DestConfig destConfig;
        public List<ReplacePattern> replacePatterns;

        public const string samplePath = "SettingsSample.xml";

        public class CopyProject
        {
            public string name;
            public Exclude exclude;

            public class Exclude
            {
                public Logical logical;
                public Physical physical;
                public TechnologyPackages technologyPackages;
                public CpuObject cpuObject;
                public Generic generic;

                internal void overrideExcludes(Exclude overrides)
                {
                    logical.overrideIgnores(overrides.logical);
                    physical.overrideIgnores(overrides.physical);
                    technologyPackages.overrideIgnores(overrides.technologyPackages);
                    cpuObject.overrideIgnores(overrides.cpuObject);
                    generic.overrideIgnores(overrides.generic);
                }

                public class CpuObject
                {
                    public List<string> ignores;

                    internal void overrideIgnores(CpuObject cpuObject)
                    {
                        if (cpuObject == null)
                        {
                            return;
                        }

                        if (ignores != null && cpuObject.ignores != null)
                        {
                            cpuObject.ignores.ForEach(x => ignores.Remove(x));
                        }
                    }
                }

                public class Generic
                {
                    public List<string> ignores;

                    internal void overrideIgnores(Generic generic)
                    {
                        if (generic == null)
                        {
                            return;
                        }

                        if (ignores != null && generic.ignores != null)
                        {
                            generic.ignores.ForEach(x => ignores.Remove(x));
                        }
                    }
                }

                public class Logical
                {
                    public List<string> ignores;

                    internal void overrideIgnores(Logical logical)
                    {
                        if (logical == null)
                        {
                            return;
                        }

                        if (ignores != null && logical.ignores != null)
                        {
                            logical.ignores.ForEach(x => ignores.Remove(x));
                        }
                    }
                }
                public class Physical
                {
                    public List<string> ignores;

                    internal void overrideIgnores(Physical physical)
                    {
                        if (physical == null)
                        {
                            return;
                        }

                        if (ignores != null && physical.ignores != null)
                        {
                            physical.ignores.ForEach(x => ignores.Remove(x));
                        }
                    }
                }
                public class TechnologyPackages
                {
                    public List<string> ignores;

                    internal void overrideIgnores(TechnologyPackages technologyPackages)
                    {
                        if (technologyPackages == null)
                        {
                            return;
                        }

                        if (ignores != null && technologyPackages.ignores != null)
                        {
                            technologyPackages.ignores.ForEach(x => ignores.Remove(x));
                        }
                    }
                }
            }
        }

        public class DestConfig
        {
            public string newName;
            public string srcConfig;
            public bool InitialContentLoad;
            public DynamicInterlockMatrix dynamicInterlockMatrix;
            public StaticInterlockMatrix staticInterlockMatrix;
            public References references;

            [XmlInclude(typeof(CommandPattern))]
            public abstract class StaticInterlockMatrix
            {
                public abstract void generate(List<mapp_meta.Group> srcAxes, List<mapp_meta.Group> commands);

                public class CommandPattern : StaticInterlockMatrix
                {
                    public List<Pattern> patterns;
                    public class Pattern
                    {
                        public string commandPattern;
                        public List<string> axisPatterns;
                        public string raction;
                        public string expectedState;

                        public List<string> getInterlockAxes(List<mapp_meta.Group> srcAxes)
                        {
                            List<string> axes = new List<string>();

                            foreach (mapp_meta.Group axis in srcAxes)
                            {
                                string axisName = "";

                                foreach (Property property in axis.Property)
                                {
                                    if (property.ID == "Name")
                                    {
                                        axisName = property.Value;
                                    }
                                }

                                foreach (string pattern in axisPatterns)
                                {
                                    if (Regex.IsMatch(axisName, pattern) == true)
                                    {
                                        axes.Add(axisName);
                                        continue;
                                    }
                                }
                            }
                            return axes;
                        }
                    }


                    public override void generate(List<mapp_meta.Group> srcAxes, List<mapp_meta.Group> commands)
                    {
                        foreach (Pattern pattern in patterns)
                        {
                            foreach (mapp_meta.Group command in commands)
                            {
                                foreach (Property property in command.Property)
                                {
                                    if (property.ID == "Name")
                                    {
                                        if (Regex.IsMatch(property.Value, pattern.commandPattern) == true)
                                        {
                                            foreach (GroupSelector commandSelector in command.Selector)
                                            {
                                                if (commandSelector.ID == "CommandType")
                                                {
                                                    List<GroupSelector> interlocks = new List<GroupSelector>(commandSelector.Selector);

                                                    List<string> axes = pattern.getInterlockAxes(srcAxes);

                                                    foreach (string axis in axes)
                                                    {
                                                        interlocks.Add(new GroupSelector()
                                                        {
                                                            ID = String.Format("ConditionCheck[{0}]", interlocks.Count),
                                                            Property = new Property[]
                                                            {
                                                                new Property()
                                                                {
                                                                    ID = "ConditionAxis",
                                                                    Value = axis,
                                                                },
                                                                new Property()
                                                                {
                                                                    ID = "ExpectedState",
                                                                    Value = pattern.expectedState,
                                                                },
                                                                 new Property()
                                                                {
                                                                    ID = "Reaction",
                                                                    Value = pattern.raction,
                                                                },
                                                            },
                                                        });
                                                    }

                                                    commandSelector.Selector = interlocks.ToArray();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            [XmlInclude(typeof(AllAxes))]
            public abstract class DynamicInterlockMatrix
            {
                public char separator
                {
                    get
                    {
                        return ',';
                    }
                }
                public abstract void generate(List<mapp_meta.Group> srcAxes, List<mapp_meta.Group> dynamicInterlocks);

                public class AllAxes : DynamicInterlockMatrix
                {
                    public List<string> blacklist;

                    /// <summary>
                    /// is only considering blacklist entries with one axis (i.e. "Inject")
                    /// </summary>
                    /// <param name="axis"></param>
                    /// <returns></returns>
                    public bool isBlacklisted(string axis)
                    {
                        foreach (string pattern in blacklist)
                        {
                            if (pattern.Contains(separator) == true)
                            {
                                continue;
                            }
                            if (Regex.IsMatch(axis, pattern) == true)
                            {
                                return true;
                            }
                        }

                        return false;
                    }

                    /// <summary>
                    /// is only considering blacklist entries with more then one axis (i.e. "Cycle,Clamp")
                    /// </summary>
                    /// <param name="axis"></param>
                    /// <param name="other"></param>
                    /// <returns></returns>
                    public bool isBlacklisted(string axis, string other)
                    {
                        foreach (string pattern in blacklist)
                        {
                            string[] entries = pattern.Split(separator);

                            if (entries.Length != 2)
                            {
                                continue;
                            }

                            if (Regex.IsMatch(axis, entries[0]) == true && Regex.IsMatch(other, entries[1]) == true)
                            {
                                return true;
                            }
                        }

                        return false;
                    }
                    public override void generate(List<mapp_meta.Group> srcAxes, List<mapp_meta.Group> dynamicInterlocks)
                    {
                        List<string> axesNames = new List<string>();
                        foreach (mapp_meta.Group axis in srcAxes)
                        {
                            string axisName = "";

                            foreach (Property property in axis.Property)
                            {
                                if (property.ID == "Name")
                                {
                                    axisName = property.Value;
                                }
                            }

                            if (isBlacklisted(axisName) == true)
                            {
                                continue;
                            }

                            mapp_meta.Group interlock = new mapp_meta.Group()
                            {
                                ID = String.Format("DynamicInterlock[{0}]", dynamicInterlocks.Count),
                                Property = new Property[]
                                    {
                                        new Property()
                                        {
                                            ID = "Axis",
                                            Selector = null,
                                            Value = axisName,
                                        }
                                    },
                                Selector = null,
                                Group1 = new mapp_meta.Group[]
                                {
                                    new mapp_meta.Group()
                                    {
                                        ID = "Interlocks",
                                        Selector = null,
                                        Group1 = null,
                                        Property = new Property[] { },
                                    }
                                }

                            };

                            List<Property> properties = new List<Property>(interlock.Group1[0].Property);

                            foreach (mapp_meta.Group lockAxis in srcAxes)
                            {
                                string lockName = "";

                                foreach (Property property in lockAxis.Property)
                                {
                                    if (property.ID == "Name")
                                    {
                                        lockName = property.Value;
                                    }
                                }

                                if (isBlacklisted(lockName) == true)
                                {
                                    continue;
                                }

                                if (isBlacklisted(axisName, lockName) == true)
                                {
                                    continue;
                                }

                                if (lockName == axisName)
                                {
                                    continue;
                                }

                                properties.Add(new Property()
                                {
                                    ID = String.Format("Axes[{0}]", properties.Count),
                                    Selector = null,
                                    Value = lockName,
                                });

                            }

                            interlock.Group1[0].Property = properties.ToArray();
                            dynamicInterlocks.Add(interlock);
                        }
                    }
                }
            }

            public class References : List<References.Reference>
            {
                [XmlInclude(typeof(Folder))]
                public abstract class Reference
                {

                    public abstract void configure(Settings parent);

                    [XmlInclude(typeof(MappView))]
                    [XmlInclude(typeof(MappServices))]
                    [XmlInclude(typeof(MappControl))]
                    [XmlInclude(typeof(Connectivity))]
                    [XmlInclude(typeof(TextSystem))]
                    public abstract class Folder : Reference
                    {
                        public string src;
                        public override void configure(Settings parent)
                        {
                            string destPath = getDestPath(parent);
                            string srcPath = getSrcPath(parent);

                            if (Directory.Exists(destPath) == true)
                            {
                                Directory.Delete(destPath, true);
                            }

                            Directory.CreateDirectory(destPath);

                            Methods.CloneDirectory(srcPath, destPath);
                        }

                        protected string getDestConfigPath(Settings parent)
                        {
                            return Project.dest + @"/Physical/" + parent.destConfig.newName;
                        }
                        protected string getSrcConfigPath(Settings parent)
                        {
                            return Project.dest + @"/Physical/" + this.src;
                        }

                        protected virtual string getSrcPath(Settings parent)
                        {
                            return Directory.GetDirectories(getSrcConfigPath(parent), type, SearchOption.AllDirectories)[0];
                        }
                        protected virtual string getDestPath(Settings parent)
                        {
                            string path = getDestConfigPath(parent);
                            return Directory.GetDirectories(path, type, SearchOption.AllDirectories)[0];
                        }

                        protected abstract string type { get; }

                        public class MappServices : Folder
                        {
                            protected override string type
                            {
                                get
                                {
                                    return "mappServices";
                                }
                            }
                        }
                        public class MappControl : Folder
                        {
                            protected override string type
                            {
                                get
                                {
                                    return "mappControl";
                                }
                            }
                        }
                        public class MappView : Folder
                        {
                            protected override string type
                            {
                                get
                                {
                                    return "mappView";
                                }
                            }
                        }
                        public class Connectivity : Folder
                        {
                            protected override string type
                            {
                                get
                                {
                                    return "Connectivity";
                                }
                            }
                        }
                        public class TextSystem : Folder
                        {
                            protected override string type
                            {
                                get
                                {
                                    return "TextSystem";
                                }
                            }
                        }
                    }
                }
            }
        }

        public static void saveSample()
        {
            using (TextWriter writer = new StreamWriter(samplePath))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Settings));

                serializer.Serialize(writer, getSample());
            }
        }

        public class ReplacePattern
        {
            public string directory;
            public string filePattern;
            public string find;
            public string replace;
            public SearchOption searchOption;

            public static ReplacePattern getSample()
            {
                return new ReplacePattern()
                {
                    directory = "dir_relative_in_project",
                    filePattern = "regex_for_files",
                    find = "<prop type=\"x - BR - TS:Namespace\">Clamp*.</prop>",
                    replace = "<prop type=\"x - BR - TS:Namespace\">Clamp1Abc</prop>",
                    searchOption = SearchOption.AllDirectories,
                };
            }
        }

        public static Settings getSample()
        {
            Settings settings = new Settings()
            {
                featureList = "Order.txt",
                destProject = @"C:\projects\LWB\Project\Dev",
                productSrc = @"C:\projects\LWB\Products\TestProducts",
                replacePatterns = new List<ReplacePattern>(new ReplacePattern[]
                {
                    ReplacePattern.getSample(),
                    ReplacePattern.getSample(),
                }),
                copyProject = new CopyProject()
                {
                    name = "name_of_the_new_project",
                    exclude = new CopyProject.Exclude()
                    {
                        physical = new CopyProject.Exclude.Physical()
                        {
                            ignores = new List<string>(new string[] { "config_name", "other_config_name", }),
                        },
                        logical = new CopyProject.Exclude.Logical()
                        {
                            ignores = new List<string>(new string[] { "path_in_logical", "other_path_in_logical", }),
                        },
                        technologyPackages = new CopyProject.Exclude.TechnologyPackages()
                        {
                            ignores = new List<string>(new string[] { "mappView",})
                        },
                        cpuObject = new CopyProject.Exclude.CpuObject()
                        {
                            ignores = new List<string>(new string[] { "mappView", })
                        },
                        generic = new CopyProject.Exclude.Generic()
                        {
                            ignores = new List<string>(new string[] { @"Physical\StandardSW\5APC3100_KBU2_000\mappView", })
                        },
                    },
                },
                destConfig = new DestConfig()
                {
                    newName = "VCRS500_messe",
                    srcConfig = "VCRS_500",
                    InitialContentLoad = true,
                    references = new DestConfig.References(),
                    dynamicInterlockMatrix = new DestConfig.DynamicInterlockMatrix.AllAxes()
                    {
                        blacklist = new List<string>(new string[] { "RegexPattern", "OnotherRegexPattern", }),
                    },
                    staticInterlockMatrix = new DestConfig.StaticInterlockMatrix.CommandPattern()
                    {
                        patterns = new List<DestConfig.StaticInterlockMatrix.CommandPattern.Pattern>
                        (new DestConfig.StaticInterlockMatrix.CommandPattern.Pattern[]
                        {
                            new DestConfig.StaticInterlockMatrix.CommandPattern.Pattern()
                            {
                                commandPattern = "Zone_1Injection1Inject",
                                expectedState = "1",
                                raction = "2",
                                axisPatterns = new List<string>( new string[]
                                {
                                    "Core1",
                                    "Core3",
                                }),
                            },
                            new DestConfig.StaticInterlockMatrix.CommandPattern.Pattern()
                            {
                                commandPattern = "^Zone_1Injection1Inject$",
                                expectedState = "99",
                                raction = "1",
                                axisPatterns = new List<string>( new string[]
                                {
                                    "Core2",
                                }),
                            },
                            new DestConfig.StaticInterlockMatrix.CommandPattern.Pattern()
                            {
                                commandPattern = "Zone_1Injection1DecompressBefore",
                                expectedState = "44",
                                raction = "1",
                                axisPatterns = new List<string>( new string[]
                                {
                                    "Core",
                                }),
                            },
                        }),
                    }
                }
            };

            settings.destConfig.references.Add(new DestConfig.References.Reference.Folder.MappControl() { src = "base_sim" });
            settings.destConfig.references.Add(new DestConfig.References.Reference.Folder.MappServices() { src = "base_sim" });
            settings.destConfig.references.Add(new DestConfig.References.Reference.Folder.MappView() { src = "base_sim" });
            settings.destConfig.references.Add(new DestConfig.References.Reference.Folder.Connectivity() { src = "base_sim" });
            settings.destConfig.references.Add(new DestConfig.References.Reference.Folder.TextSystem() { src = "base_sim" });

            return settings;
        }
    }

    public class program
    {
        static bool wait = false;
        static string settingsSrc = "Settings.xml";
        static bool generate = true;
        /// <summary>
        /// handling the command line interface and it's options
        /// </summary>
        public abstract class CLIOption
        {
            /// <summary>
            /// activates each option
            /// </summary>
            public abstract void apply();

            /// <summary>
            /// generates a collection of CLIOption objects based on CLI args
            /// </summary>
            /// <param name="args"></param>
            /// <returns></returns>
            public static CLIOptionCollection parse(string[] args)
            {
                CLIOptionCollection result = new CLIOptionCollection();

                //using enumerator instead of foreach to aquire CLI option values more elegant
                System.Collections.IEnumerator enumerator = args.GetEnumerator();

                while(enumerator.MoveNext())
                {
                    CLIOption tmp;

                    string arg = (string)enumerator.Current;

                    Logging.log(string.Format("attempt to parse {0} for CLI options", arg), LEVEL.INFO);

                    switch (arg)
                    {
                        case "--help":
                        case "-h":
                            Logging.enterContext(arg);

                            tmp = new CLIOption.Help();
                            result.Add(tmp);
                            Logging.log(string.Format("successfully parsed CLI option {0}", tmp), LEVEL.INFO);

                            break;

                        case "--version":
                        case "-v":
                            Logging.enterContext(arg);

                            //the git version number is written into this assembly file via a pre-build event                       
                            using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("project_creator.version.txt"))
                            {                                
                                using (StreamReader reader = new StreamReader(stream))
                                {
                                    tmp = new CLIOption.Version(Regex.Replace(reader.ReadToEnd(), @"\s+", ""));
                                    result.Add(tmp);
                                    Logging.log(string.Format("successfully parsed CLI option {0}", tmp), LEVEL.INFO);
                                }
                            }
                            Logging.leaveContext();
                            break;

                        case "--generate-samples":
                        case "-s":
                            Logging.enterContext(arg);
                            //option needs to just exist to be applicable
                            result.Add(new CLIOption.GenerateSamples());
                            Logging.leaveContext();
                            break;

                        case "--wait-for-exit":
                        case "-w":
                            Logging.enterContext(arg);
                            //option needs to just exist to be applicable
                            result.Add(new CLIOption.WaitForExit());
                            Logging.leaveContext();
                            break;

                        case "--settings-input":
                        case "-i":
                            Logging.enterContext(arg);
                            
                            //this CLI option requires a parameter
                            if (enumerator.MoveNext() == false)
                            {
                                Logging.log(string.Format("option {0} has no valid value", arg), LEVEL.WARNING);
                            }
                            else
                            {
                                tmp = new CLIOption.SettingsInput((string)enumerator.Current);
                                result.Add(tmp);

                                Logging.log(string.Format("successfully parsed CLI option {0}", tmp), LEVEL.INFO);
                            }
                            Logging.leaveContext();
                            break;

                        case "--verbose":

                            Logging.enterContext(arg);

                            if (enumerator.MoveNext() == false)
                            {
                                Logging.log(string.Format("option {0} has no valid value", arg), LEVEL.WARNING);
                            }
                            else
                            {
                                LEVEL lvl;

                                string value = (string)enumerator.Current;

                                if (Enum.TryParse(value, true, out lvl))
                                {
                                    if (Enum.IsDefined(typeof(LEVEL), lvl))
                                    {
                                        tmp = new CLIOption.Verbose(lvl);
                                        result.Add(tmp);
                                        Logging.log(string.Format("successfully parsed CLI option {0}", tmp), LEVEL.INFO);
                                    }
                                    else
                                    {
                                        Logging.log(string.Format("value {0} for option {1} is not valid", value, arg), LEVEL.WARNING);
                                    }
                                }
                                else
                                {
                                    Logging.log(string.Format("value {0} for option {1} is not valid", value, arg), LEVEL.WARNING);
                                }
                            }

                            Logging.leaveContext();
                            break;

                        default:
                            Logging.log(string.Format("unknown command line argument {0} will be ignored", arg), LEVEL.WARNING);
                            break;
                    }
                }

                return result;
            }

            public class WaitForExit : CLIOption
            {
                public override void apply()
                {
                    wait = true;
                }
            }

            public class Version : CLIOption
            {
                public string value { get; private set; }

                public Version(string value) { this.value = value;}

                public override string ToString()
                {
                    return string.Format("CLI Option: Version {0}", value);
                }

                public override void apply()
                {
                    generate = false;

                    Console.WriteLine("Version: {0}", this.value);
                }
            }
            public class GenerateSamples : CLIOption
            {
                public override string ToString()
                {
                    return string.Format("CLI Option: generate samples");
                }
                public override void apply()
                {
                    Logging.enterContext(this);

                    generate = false;

                    Settings.saveSample();
                    Logging.log(string.Format("saved settings sample as {0}", Settings.samplePath), LEVEL.INFO);

                    List<Project.ProjectFeature.ConfigurationObject> config = new List<Project.ProjectFeature.ConfigurationObject>();
                    config.Add(new Project.ProjectFeature.ConfigurationObject.OPC_UA());
                    config.Add(new Project.ProjectFeature.ConfigurationObject.Task()
                    {
                        mode = Project.ProjectFeature.ConfigurationObject.Task.Mode.COPY,
                        namingPattern = "taskBla#Index",
                        wildcardOverrides = Project.ProjectFeature.ConfigurationObject.WildcardOverrideList.getSample(),
                    });

                    Project.ProjectFeature.ConfigurationObject.UserFile.Recipe recipe = new Project.ProjectFeature.ConfigurationObject.UserFile.Recipe();
                    recipe.file = "abc";
                    recipe.path = "abc";
                    config.Add(recipe);

                    Project.ProjectFeature.ConfigurationObject.UserFile.FixData fix = new Project.ProjectFeature.ConfigurationObject.UserFile.FixData();
                    fix.file = "abc";
                    fix.path = "abc";
                    config.Add(fix);

                    config.Add(Project.ProjectFeature.ConfigurationObject.HardwareModule.getSample());

                    config.Add(new Project.ProjectFeature.ConfigurationObject.ExcludeOverride()
                    {
                       overrides = new Settings.CopyProject.Exclude()
                       { 
                           technologyPackages = new Settings.CopyProject.Exclude.TechnologyPackages()
                           {
                               ignores = new List<string>(new string[] { "mappView"})
                           },
                       }
                    });

                    Project.ProjectFeature.ConfigurationObject.Mapp mapp = new Project.ProjectFeature.ConfigurationObject.Mapp();

                    Project.ProjectFeature.ConfigurationObject.Mapp.MappElement.MappPackage package = new Project.ProjectFeature.ConfigurationObject.Mapp.MappElement.MappPackage();
                    package.taskIdentifier = "abc";
                    package.srcPath = "path";
                    package.replacePatterns.Add(new Project.ReplacePattern() { find = "find_what", replace = "replace_by" });
                    mapp.mappElements.Add(package);
                    mapp.taskIdentifier = "taskIdentifier";

                    mapp.conditions = new List<Project.ProjectFeature.ConfigurationObject.Condition>();
                    mapp.conditions.Add(new Project.ProjectFeature.ConfigurationObject.Condition.FeatureIndex.FeatureIndexBigger()
                    {
                        index = 0,
                    });
                    mapp.conditions.Add(new Project.ProjectFeature.ConfigurationObject.Condition.FeatureIndex.FeatureIndexSmaller()
                    {
                        index = 0,
                    });

                    mapp.conditions.Add(new Project.ProjectFeature.ConfigurationObject.Condition.FeatureIndex.FeatureIndexEqual()
                    {
                        index = 0,
                    });

                    mapp.conditions.Add(new Project.ProjectFeature.ConfigurationObject.Condition.FeatureIndex.FeatureIndexUnequal()
                    {
                        index = 0,
                    });

                    mapp.conditions.Add(new Project.ProjectFeature.ConfigurationObject.Condition.FeatureIndex.ProductIndexEqual()
                    {
                        index = 0,
                    });

                    mapp.conditions.Add(new Project.ProjectFeature.ConfigurationObject.Condition.FeatureIndex.ProductIndexUnequal()
                    {
                        index = 0,
                    });

                    mapp.conditions.Add(new Project.ProjectFeature.ConfigurationObject.Condition.FeatureIndex.ProductIndexSmaller()
                    {
                        index = 0,
                    });

                    mapp.conditions.Add(new Project.ProjectFeature.ConfigurationObject.Condition.FeatureIndex.ProductIndexBigger()
                    {
                        index = 0,
                    });

                    mapp.conditions.Add(new Project.ProjectFeature.ConfigurationObject.Condition.FeatureIndex.FeatureIndexModulo()
                    {
                        modulo = 2,
                        result = 0,
                    });

                    package.conditions = mapp.conditions;

                    config.Add(mapp);

                    Project.ProjectFeature.ConfigurationObject.MappView mappView = new Project.ProjectFeature.ConfigurationObject.MappView();

                    Project.ProjectFeature.ConfigurationObject.MappView.Element.MappViewPackage mvPackage = new Project.ProjectFeature.ConfigurationObject.MappView.Element.MappViewPackage();
                    mvPackage.taskIdentifier = "abc";
                    mvPackage.destPath = "path";
                    mvPackage.replacePatterns.Add(new Project.ReplacePattern() { find = "find_what", replace = "replace_by" });
                    mappView.mvElements.Add(mvPackage);
                    mappView.taskIdentifier = "taskIdentifier";
                    mvPackage.contentPreCachePattern = new List<string>();
                    mvPackage.contentPreCachePattern.Add("some*");
                    mvPackage.contentPreCachePattern.Add("reg*ex");
                    mvPackage.contentPreCachePattern.Add("*pattern");

                    mappView.conditions = new List<Project.ProjectFeature.ConfigurationObject.Condition>();
                    mappView.conditions.Add(new Project.ProjectFeature.ConfigurationObject.Condition.FeatureIndex.FeatureIndexBigger()
                    {
                        index = 0,
                    });
                    mappView.conditions.Add(new Project.ProjectFeature.ConfigurationObject.Condition.FeatureIndex.FeatureIndexSmaller()
                    {
                        index = 0,
                    });

                    mappView.conditions.Add(new Project.ProjectFeature.ConfigurationObject.Condition.FeatureIndex.FeatureIndexEqual()
                    {
                        index = 0,
                    });

                    mappView.conditions.Add(new Project.ProjectFeature.ConfigurationObject.Condition.FeatureIndex.FeatureIndexUnequal()
                    {
                        index = 0,
                    });

                    mappView.conditions.Add(new Project.ProjectFeature.ConfigurationObject.Condition.FeatureIndex.ProductIndexEqual()
                    {
                        index = 0,
                    });

                    mappView.conditions.Add(new Project.ProjectFeature.ConfigurationObject.Condition.FeatureIndex.ProductIndexUnequal()
                    {
                        index = 0,
                    });

                    mappView.conditions.Add(new Project.ProjectFeature.ConfigurationObject.Condition.FeatureIndex.ProductIndexSmaller()
                    {
                        index = 0,
                    });

                    mappView.conditions.Add(new Project.ProjectFeature.ConfigurationObject.Condition.FeatureIndex.ProductIndexBigger()
                    {
                        index = 0,
                    });

                    mappView.conditions.Add(new Project.ProjectFeature.ConfigurationObject.Condition.FeatureIndex.FeatureIndexModulo()
                    {
                        modulo = 2,
                        result = 0,
                    });

                    config.Add(mappView);
                    config.Add(new Project.ProjectFeature.ConfigurationObject.SVG()
                    {
                        parent = "parent.svg",
                        destination = "result.svg",
                        files = new List<string>(new string[]
                        {
                            "svg1.svg",
                            "svg2.svg",
                        }),
                    });

                    config.Add(Project.ProjectFeature.ConfigurationObject.Sourcecode.C.generateSample());

                    Project.ProjectFeature feature = new Project.ProjectFeature("TestFeature", "001", config);
                    feature.export();

                    Logging.log(string.Format("saved sa feature sample as {0}", feature.exportName), LEVEL.INFO);

                    Project.ProjectFeature feature1 = new Project.ProjectFeature("TestFeature_abstract", "001", config);
                    feature1.isAbstract = true;
                    feature1.export();

                    Logging.log(string.Format("saved sa feature sample as {0}", feature1.exportName), LEVEL.INFO);

                    Project.ProjectFeature feature2 = new Project.ProjectFeature("TestFeature", "001", config);
                    feature2.parents = new List<string>(new string[] { "TestFeature_abstract" });
                    feature2.export();

                    Logging.log(string.Format("saved sa feature sample as {0}", feature2.exportName), LEVEL.INFO);

                    /*create properties sample*/
                    XmlWriterSettings xmlSettings = new XmlWriterSettings()
                    {
                        Indent = true,
                    };

                    using (XmlWriter write = XmlWriter.Create("PropertiesSample.xml", xmlSettings))
                    {
                        DataContractSerializer serializer = new DataContractSerializer(typeof(codegen.PropertyCollection));

                        serializer.WriteObject(write, codegen.PropertyCollection.getSample());
                    }

                    Logging.log(string.Format("saved a properties sample as PropertiesSample.xml"), LEVEL.INFO);

                    using (XmlWriter write = XmlWriter.Create("RulesSample.xml", xmlSettings))
                    {
                        DataContractSerializer serializer = new DataContractSerializer(typeof(codegen.Rules));

                        serializer.WriteObject(write, codegen.Rules.getSample());
                    }

                    Logging.log(string.Format("saved a rules sample as RulesSample.xml"), LEVEL.INFO);

                    Logging.leaveContext();
                }
            }

            public class Verbose : CLIOption
            {
                public Verbose(LEVEL lvl) { level = lvl; }
               
                public LEVEL level { get; private set; }

                public override string ToString()
                {
                    return string.Format("CLI Option: verbose level {0}", level);
                }
                public override void apply()
                {
                    Logging.enterContext(this);

                    Logging.log(string.Format("set logging level to {0}", level), LEVEL.INFO);
                    Logging.leaveContext();

                    Logging.outputMessageLevel = level;                    
                }
            }

            public class SettingsInput : CLIOption
            {
                public string file { get; private set; }

                public SettingsInput(string file) { this.file = file; }

                public override string ToString()
                {
                    return string.Format("CLI Option: Settings source file {0}", file);
                }

                public override void apply()
                {
                    Logging.enterContext(this);
                    
                    settingsSrc = file;
                    Logging.log(string.Format("set settings source file to {0}", settingsSrc), LEVEL.INFO);

                    Logging.leaveContext();
                }
            }

            public class Help : CLIOption
            {
                public override string ToString()
                {
                    return string.Format("CLI Option: help");
                }

                public override void apply()
                {
                    generate = false;

                    //the git version number is written into this assembly file via a pre-build event
                    using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("project_creator.help.org"))
                    {
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            Console.WriteLine(reader.ReadToEnd());
                        }
                    }
                }
            }
        }      
        public class CLIOptionCollection : Collection<CLIOption>
        {
           
        }
        /* feature list src, product source, destination project, destination config*/
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                Logging.log(string.Format("started project creator with args: {0}", string.Join(" ", args)), LEVEL.INFO);
            }         
            CLIOptionCollection options = CLIOption.parse(args);

            //going through all options and apply them
            foreach (CLIOption option in options)
            {
                option.apply();
            }

            if (generate == false)
            {
                Logging.log(string.Format("Project will not be generated"), LEVEL.WARNING);

                if (wait == false)
                {
                    Console.WriteLine("done...So Long, and Thanks for All the Fish");
                }
                else
                {
                    Console.WriteLine("done...press any key to terminate");
                    Console.Read();
                }

                return;
            }
            //test logging and nesting of logging

            Logging.errorLogEvent += (context) =>
            {
                Console.WriteLine("exit due to error");

                if (wait == true)
                {
                    Console.WriteLine("press any key to terminate...");
                    Console.Read();
                }

                // close Console app
                System.Environment.Exit(1);
            };
            /*Logging.log("test1", LEVEL.INFO);
            Logging.log("test2", LEVEL.INFO);

            Logging.enterContext("new test context");
            Logging.log("test11", LEVEL.INFO);
            Logging.log("test12", LEVEL.WARNING);
            Logging.log("test13", LEVEL.ERROR);

            Logging.leaveContext();
            Logging.log("test3", LEVEL.ERROR);*/

            using (FileStream destFs = new FileStream(settingsSrc, FileMode.Open))
            {
                XmlSerializer destSerializer = new XmlSerializer(typeof(Settings));
                Settings settings = (Settings)destSerializer.Deserialize(destFs);

                string extension = new FileInfo(settings.featureList).Extension;

                if (extension == ".txt")
                {
                    Project.import(new StreamReader(settings.featureList), settings.productSrc);
                }
                else if (extension == ".xml")
                {
                    codegen.Order order;

                    using (XmlReader reade = XmlReader.Create(settings.featureList))
                    {
                        DataContractSerializer serializer = new DataContractSerializer(typeof(codegen.Order));

                        order = (codegen.Order)serializer.ReadObject(reade);
                    }

                    Project.import(order, settings.productSrc);
                }

                Project.generate(settings);

                Logging.writeLogStatistics();

                if (wait == false)
                {
                    Console.WriteLine("done...So Long, and Thanks for All the Fish");
                }
                else
                {
                    Console.WriteLine("done...press any key to terminate");
                    Console.Read();
                }
            }
        }       
    }
}