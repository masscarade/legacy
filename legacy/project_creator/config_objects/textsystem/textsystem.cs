﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

namespace textsystem
{

    public partial class Configuration
    {
        public void addTmx(string path)
        {
            string entry = path.Substring(path.IndexOf(@"Logical\"));
            entry = entry.Replace(@"\\", ".");
            entry = entry.Replace(@"//", ".");
            entry = entry.Replace(@"\", ".");
            entry = entry.Replace(@"/", ".");
            entry = entry.Replace(@"Logical.", "");

            foreach (ConfigurationElement item in Items)
            {
                foreach (Group gr in item.Group)
                {
                    if (gr.ID == "TargetFiles")
                    {
                        List<GroupProperty> entries = new List<GroupProperty>();
                        entries.AddRange(gr.Property);

                        entries.Add(new GroupProperty() { Value = entry });
                        gr.Property = entries.ToArray();

                    }
                }                
            }
        }

        public static Configuration load(string path)
        {
            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Configuration));
                return (Configuration)serializer.Deserialize(fs);
            }
        }
        public void save(string path)
        {
            foreach (ConfigurationElement item in Items)
            {
                foreach (Group gr in item.Group)
                {
                    if (gr.ID == "TargetFiles")
                    {
                        int i = 1;
                        foreach (GroupProperty property in gr.Property)
                        {
                            property.ID = "TargetFile["+ i++.ToString() + "]";
                        }
                    }
                }
            }

            using (TextWriter writer = new StreamWriter(path))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Configuration));
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add("", "");

                serializer.Serialize(writer, this);
            }
        }
    }
}
