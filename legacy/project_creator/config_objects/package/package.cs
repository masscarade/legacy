﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.IO;
using codegenLog;

namespace package
{
    public class Configuration : Package
    {
        public Configuration(XDocument xDocument) : base(xDocument)
        {
        }
    }
    public class Package
    {
        public const string OBJECTS_GROUP = "Objects";
        public const string OBJECT = "Object";

        public const string TYPE_ATTRIBUTE = "Type";
        public const string TYPE_ATTRIBUTE_FILE = "File";
        public const string TYPE_ATTRIBUTE_PACKAGE = "Package";
        public const string TYPE_ATTRIBUTE_PROGRAM = "Program";
        public const string TYPE_ATTRIBUTE_CONFIGURATION = "Configuration";

        public const string SUB_TYPE_ATTRIBUTE = "SubType";
        public const string SUB_TYPE_ATTRIBUTE_VARIABLES = "VariablesPackage";
        public const string SUB_TYPE_ATTRIBUTE_BINARY = "Binary";
        public const string SUB_TYPE_ATTRIBUTE_ANSIC = "ANSIC";
        public const string SUB_TYPE_ATTRIBUTE_Layouts = "Layouts";
        public const string SUB_TYPE_ATTRIBUTE_SNIPPETS = "SnippetsPackage";
        public const string SUB_TYPE_ATTRIBUTE_TEXTS = "TextsPackage";
        public const string SUB_TYPE_ATTRIBUTE_STYLES = "StylesSetPackage";
        public const string SUB_TYPE_ATTRIBUTE_EMPTY_MV = "EmptyMappVIewPackage";
        public const string SUB_TYPE_ATTRIBUTE_DIALOGS = "DialogPackage";
        public const string SUB_TYPE_ATTRIBUTE_EXPRESSIONS = "ExpressionsPackage";
        public const string SUB_TYPE_ATTRIBUTE_PAGES = "PagePackage";
        public const string SUB_TYPE_ATTRIBUTE_AREA_CONTENTS = "AreaContentsPackage";
        public const string SUB_TYPE_ATTRIBUTE_WIDGETS = "WidgetsPackage";
        public const string SUB_TYPE_ATTRIBUTE_MEDIA = "MediaPackage";


        public const string DESCRIPTION_ATTRIBUTE = "Description";


        public readonly Dictionary<string, string> groupElements = new Dictionary<string, string>()
        {
            {OBJECTS_GROUP, OBJECT },
        };

        public readonly Dictionary<string, XElement> packageGroups = new Dictionary<string, XElement>()
        {
            {OBJECTS_GROUP, null },
        };

        private XDocument xDoc;

        public void clearGroup(string group)
        {
            packageGroups[group].RemoveNodes();
        }

        public Package(XDocument xDocument)
        {
            this.xDoc = xDocument;

            string[] keys = packageGroups.Keys.ToArray();

            Logging.enterContext(this);

            foreach (string group in keys)
            {
                packageGroups[group] = initGroup(group);
            }

            Logging.leaveContext();
        }

        public void setSubType(string subType)
        {
            if (subType == null)
            {
                return;
            }

            XElement el = null;

            foreach (XElement node in xDoc.Descendants())
            {
                if (node.Name.LocalName == "Package")
                {
                    el = node;
                    break;
                }
            }

            XAttribute attr = el.Attribute(Package.SUB_TYPE_ATTRIBUTE);

            if (attr == null)
            {
                attr = new XAttribute(Package.SUB_TYPE_ATTRIBUTE, subType);
                el.Add(attr);
            }
            else
            {
                attr.Value = subType;
            }
        }

        public void addFile(string file, string description = "")
        {
            addElement(OBJECTS_GROUP, file, new XAttribute[] {new XAttribute("Type", "File"), new XAttribute(Package.DESCRIPTION_ATTRIBUTE, description), });
        }

        public void addPackage(string file, string description = "")
        {
            addElement(OBJECTS_GROUP, file, new XAttribute[] { new XAttribute("Type", "Package"), new XAttribute(Package.DESCRIPTION_ATTRIBUTE, description), });
        }

        public void addElement(string group, string id, XAttribute[] attributes = null)
        {
            if (packageGroups.ContainsKey(group) == false)
            {
                Logging.log(string.Format("failed to add; group {0} not existing in {1}", group, this), LEVEL.ERROR);
            }

            XElement groupEl = packageGroups[group];

            string elementName = groupElements[group];

            foreach (XElement obj in groupEl.Elements())
            {
                if (obj.Value == id)
                {
                    Logging.log(string.Format("skipp adding; element {0} already existing in {1}", id, group), LEVEL.WARNING);
                    return;
                }
            }

            XElement newEl = new XElement(elementName);
            newEl.Value = id;
            Logging.log(string.Format("added element {0} to group {1}", id, group), LEVEL.INFO);

            if (attributes != null)
            {
                Logging.log(string.Format("added attributes to {0} element {1}", group, id), LEVEL.INFO);
                newEl.Add(attributes);
            }

            groupEl.Add(newEl);
        }

        private XElement initGroup(string name)
        {
            XElement el = null;

            foreach (XElement node in xDoc.Descendants())
            {
                if (node.Name.LocalName == name)
                {
                    el = node;
                    break;
                }
            }

            if (el == null)
            {
                Logging.log(string.Format("group {0} not existing in {1}", name, this), LEVEL.WARNING);
                el = new XElement(name);
                xDoc.Root.Add(el);

                Logging.log(string.Format("created and added new group {0}", name), LEVEL.INFO);
            }

            return el;
        }

        public static Package load(string path)
        {
            Package vis = new Package(XDocument.Load(path));

            Logging.log(string.Format("loaded {0}", vis), LEVEL.INFO);

            return vis;
        }
        public void save(string path)
        {
            xDoc.Save(path);
        }
    }
}