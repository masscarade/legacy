﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using codegenLog;

namespace mv_vis
{
    public class Visualization
    {
        public const string CONTENTS_GROUP = "Contents";
        public const string CONTENT = "Content";

        public const string PAGES_GROUP = "Pages";
        public const string PAGE = "Page";

        public const string NAVIGATIONS_GROUP = "Navigations";
        public const string NAVIGATION = "Navigation";

        public const string BINDIGS_SET_GROUP = "BindingsSets";
        public const string BINDINGS_SET = "BindingsSet";

        public const string DIALOGS_GROUP = "Dialogs";
        public const string DIALOG = "Dialog";

        public const string SNIPPETS_SETS_GROUP = "SnippetsSets";
        public const string SNIPPETS_SET = "SnippetsSet";

        public const string EXPRESSIONS_SETS_GROUP = "ExpressionsSets";
        public const string EXPRESSIONS_SET = "ExpressionsSet";

        public const string EVENT_BINDINGS_SETS_GROUP = "EventBindingsSets";
        public const string EVENT_BINDINGS_SET = "EventBindingsSet";

        public const string VARIABLES_SETS_GROUP = "VariablesSets";
        public const string VARIABLES_SET = "VariablesSet";

        public const string THEMES_GROUP = "Themes";
        public const string THEME = "Theme";

        public const string CONFIGURATIONS_GROUP = "Configurations";
        public const string CONFIGURATION = "Configuration";
        public const string CONFIGURATION_ID = "key";

        public const string DEFAULT_ID_ATTRIBUTE = "refId";

        public const string CONTENT_PRE_CACHE_ATTRIBUTE = "preCache";

        public string visId { get; private set; }

        public readonly Dictionary<string, string> elementIdOverrides = new Dictionary<string, string>()
        {
            {CONFIGURATION, CONFIGURATION_ID},
        };

        public readonly Dictionary<string, string> groupElements = new Dictionary<string, string>()
        {
            {CONTENTS_GROUP, CONTENT },
            {PAGES_GROUP, PAGE },
            {NAVIGATIONS_GROUP, NAVIGATION },
            {BINDIGS_SET_GROUP, BINDINGS_SET },
            {DIALOGS_GROUP, DIALOG },
            {SNIPPETS_SETS_GROUP, SNIPPETS_SET },
            {EXPRESSIONS_SETS_GROUP, EXPRESSIONS_SET },
            {EVENT_BINDINGS_SETS_GROUP, EVENT_BINDINGS_SET },
            {VARIABLES_SETS_GROUP, VARIABLES_SET },
            {THEMES_GROUP, THEME },
            {CONFIGURATIONS_GROUP, CONFIGURATION },
        };

        public readonly Dictionary<string, XElement> visGroups = new Dictionary<string, XElement>()
        {
            {CONTENTS_GROUP, null },
            {PAGES_GROUP, null },
            {NAVIGATIONS_GROUP, null },
            {BINDIGS_SET_GROUP, null },
            {DIALOGS_GROUP, null },
            {SNIPPETS_SETS_GROUP, null },
            {EXPRESSIONS_SETS_GROUP, null },
            {EVENT_BINDINGS_SETS_GROUP, null },
            {VARIABLES_SETS_GROUP, null },
            {THEMES_GROUP, null },
            {CONFIGURATIONS_GROUP, null },
        };

        private XDocument xDoc;

        public Visualization(XDocument xDocument)
        {
            this.xDoc = xDocument;

            string[] keys = visGroups.Keys.ToArray();

            visId = xDoc.Root.Attribute("id").Value;

            Logging.enterContext(this);

            foreach (string group in keys)
            {
                visGroups[group] = initGroup(group);
            }

            Logging.leaveContext();
        }

        public void addElement(string group, string id, XAttribute[] attributes = null)
        {
            if (visGroups.ContainsKey(group) == false)
            {
                Logging.log(string.Format("failed to add; group {0} not existing in {1}", group, this), LEVEL.ERROR);
            }

            XElement groupEl = visGroups[group];

            foreach (XElement obj in groupEl.Elements())
            {
                if (obj.Value == id)
                {
                    Logging.log(string.Format("skipp adding; element {0} already existing in {1}", id, group), LEVEL.WARNING);
                    return;
                }
            }

            string idAttribute = DEFAULT_ID_ATTRIBUTE;
            string elementName = groupElements[group];

            if (elementIdOverrides.ContainsKey(elementName))
            {
                idAttribute = elementIdOverrides[elementName];
            }

            XElement newEl = new XElement(elementName);
            newEl.Add(new XAttribute(idAttribute, id));
            Logging.log(string.Format("added element {0} to group {1}", id, group), LEVEL.INFO);

            if (attributes != null)
            {
                Logging.log(string.Format("added attributes to {0} element {1}", group, id), LEVEL.INFO);
                newEl.Add(attributes);
            }

            groupEl.Add(newEl);
        }

        private XElement initGroup(string name)
        {
            XElement el = null;

            foreach (XElement node in xDoc.Descendants())
            {
                if (node.Name.LocalName == name)
                {
                    el = node;
                    break;
                }
            }

            if (el == null)
            {
                Logging.log(string.Format("group {0} not existing in {1}", name, this), LEVEL.WARNING);
                el = new XElement(name);
                xDoc.Root.Add(el);

                Logging.log(string.Format("created and added new group {0}", name), LEVEL.INFO);
            }

            return el;
        }

        public override string ToString()
        {
            return string.Format("Visualization id: {0}", visId);
        }

        public static Visualization load(string path)
        {            
            Visualization vis = new Visualization(XDocument.Load(path));
   
            Logging.log(string.Format("loaded {0}", vis), LEVEL.INFO);

            return vis;
        }
        public void save(string path)
        {
            xDoc.Save(path);
        }     
    }   
}