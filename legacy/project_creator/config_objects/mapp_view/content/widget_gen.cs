﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by xsd, Version=4.0.30319.17929.
// 
namespace mv_content
{
    using System.Xml.Serialization;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.br-automation.com/iat2014/widget")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.br-automation.com/iat2014/widget", IsNullable=false)]
    public partial class Arguments {
        
        private Argument[] argumentField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Argument")]
        public Argument[] Argument {
            get {
                return this.argumentField;
            }
            set {
                this.argumentField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.br-automation.com/iat2014/widget")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.br-automation.com/iat2014/widget", IsNullable=false)]
    public partial class Argument {
        
        private string descriptionField;
        
        private string nameField;
        
        private string typeField;
        
        private int indexField;
        
        private bool indexFieldSpecified;
        
        /// <remarks/>
        public string Description {
            get {
                return this.descriptionField;
            }
            set {
                this.descriptionField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string name {
            get {
                return this.nameField;
            }
            set {
                this.nameField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string type {
            get {
                return this.typeField;
            }
            set {
                this.typeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int index {
            get {
                return this.indexField;
            }
            set {
                this.indexField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool indexSpecified {
            get {
                return this.indexFieldSpecified;
            }
            set {
                this.indexFieldSpecified = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.br-automation.com/iat2014/widget")]
    public partial class Member {
        
        private string refIdField;
        
        private string attributeField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string refId {
            get {
                return this.refIdField;
            }
            set {
                this.refIdField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string attribute {
            get {
                return this.attributeField;
            }
            set {
                this.attributeField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.br-automation.com/iat2014/widget")]
    public partial class BindingMember {
        
        private Member sourceMemberField;
        
        private Member targetMemberField;
        
        private string modeField;
        
        /// <remarks/>
        public Member SourceMember {
            get {
                return this.sourceMemberField;
            }
            set {
                this.sourceMemberField = value;
            }
        }
        
        /// <remarks/>
        public Member TargetMember {
            get {
                return this.targetMemberField;
            }
            set {
                this.targetMemberField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string mode {
            get {
                return this.modeField;
            }
            set {
                this.modeField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.br-automation.com/iat2014/widget")]
    public partial class BindingTemplate {
        
        private BindingMember[] bindingMemberField;
        
        private string idField;
        
        private string widgetTypeField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("BindingMember")]
        public BindingMember[] BindingMember {
            get {
                return this.bindingMemberField;
            }
            set {
                this.bindingMemberField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string id {
            get {
                return this.idField;
            }
            set {
                this.idField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string widgetType {
            get {
                return this.widgetTypeField;
            }
            set {
                this.widgetTypeField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.br-automation.com/iat2014/widget")]
    public partial class Description {
        
        private string nameField;
        
        private string valueField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string name {
            get {
                return this.nameField;
            }
            set {
                this.nameField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value {
            get {
                return this.valueField;
            }
            set {
                this.valueField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.br-automation.com/iat2014/widget")]
    public partial class Category {
        
        private string nameField;
        
        private string valueField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string name {
            get {
                return this.nameField;
            }
            set {
                this.nameField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value {
            get {
                return this.valueField;
            }
            set {
                this.valueField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.br-automation.com/iat2014/widget")]
    public partial class IsProjectable {
        
        private bool valueField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public bool Value {
            get {
                return this.valueField;
            }
            set {
                this.valueField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.br-automation.com/iat2014/widget")]
    public partial class StyleElement {
        
        private string attributeField;
        
        private string selectorField;
        
        private string idsuffixField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string attribute {
            get {
                return this.attributeField;
            }
            set {
                this.attributeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string selector {
            get {
                return this.selectorField;
            }
            set {
                this.selectorField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string idsuffix {
            get {
                return this.idsuffixField;
            }
            set {
                this.idsuffixField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.br-automation.com/iat2014/widget")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.br-automation.com/iat2014/widget", IsNullable=false)]
    public partial class Properties {
        
        private Property[] propertyField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Property")]
        public Property[] Property {
            get {
                return this.propertyField;
            }
            set {
                this.propertyField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.br-automation.com/iat2014/widget")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.br-automation.com/iat2014/widget", IsNullable=false)]
    public partial class Property {
        
        private string descriptionField;
        
        private string nameField;
        
        private string typeField;
        
        private string iatTypeField;
        
        private string categoryField;
        
        private bool initOnlyField;
        
        private bool readOnlyField;
        
        private bool requiredField;
        
        private bool projectableField;
        
        private bool projectableFieldSpecified;
        
        private string defaultValueField;
        
        private string typeRefIdField;
        
        private string nodeRefIdField;
        
        private string groupRefIdField;
        
        private string groupOrderField;
        
        private bool hideField;
        
        private bool hideFieldSpecified;
        
        /// <remarks/>
        public string Description {
            get {
                return this.descriptionField;
            }
            set {
                this.descriptionField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string name {
            get {
                return this.nameField;
            }
            set {
                this.nameField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string type {
            get {
                return this.typeField;
            }
            set {
                this.typeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string iatType {
            get {
                return this.iatTypeField;
            }
            set {
                this.iatTypeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string category {
            get {
                return this.categoryField;
            }
            set {
                this.categoryField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool initOnly {
            get {
                return this.initOnlyField;
            }
            set {
                this.initOnlyField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool readOnly {
            get {
                return this.readOnlyField;
            }
            set {
                this.readOnlyField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool required {
            get {
                return this.requiredField;
            }
            set {
                this.requiredField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool projectable {
            get {
                return this.projectableField;
            }
            set {
                this.projectableField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool projectableSpecified {
            get {
                return this.projectableFieldSpecified;
            }
            set {
                this.projectableFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string defaultValue {
            get {
                return this.defaultValueField;
            }
            set {
                this.defaultValueField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string typeRefId {
            get {
                return this.typeRefIdField;
            }
            set {
                this.typeRefIdField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string nodeRefId {
            get {
                return this.nodeRefIdField;
            }
            set {
                this.nodeRefIdField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string groupRefId {
            get {
                return this.groupRefIdField;
            }
            set {
                this.groupRefIdField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType="integer")]
        public string groupOrder {
            get {
                return this.groupOrderField;
            }
            set {
                this.groupOrderField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool hide {
            get {
                return this.hideField;
            }
            set {
                this.hideField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool hideSpecified {
            get {
                return this.hideFieldSpecified;
            }
            set {
                this.hideFieldSpecified = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.br-automation.com/iat2014/widget")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.br-automation.com/iat2014/widget", IsNullable=false)]
    public partial class Events {
        
        private Event[] eventField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Event")]
        public Event[] Event {
            get {
                return this.eventField;
            }
            set {
                this.eventField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.br-automation.com/iat2014/widget")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.br-automation.com/iat2014/widget", IsNullable=false)]
    public partial class Event {
        
        private string descriptionField;
        
        private Argument[] argumentsField;
        
        private string nameField;
        
        /// <remarks/>
        public string Description {
            get {
                return this.descriptionField;
            }
            set {
                this.descriptionField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable=false)]
        public Argument[] Arguments {
            get {
                return this.argumentsField;
            }
            set {
                this.argumentsField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string name {
            get {
                return this.nameField;
            }
            set {
                this.nameField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.br-automation.com/iat2014/widget")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.br-automation.com/iat2014/widget", IsNullable=false)]
    public partial class Methods {
        
        private Method[] methodField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Method")]
        public Method[] Method {
            get {
                return this.methodField;
            }
            set {
                this.methodField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.br-automation.com/iat2014/widget")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.br-automation.com/iat2014/widget", IsNullable=false)]
    public partial class Method {
        
        private string descriptionField;
        
        private Argument[] argumentsField;
        
        private string nameField;
        
        /// <remarks/>
        public string Description {
            get {
                return this.descriptionField;
            }
            set {
                this.descriptionField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable=false)]
        public Argument[] Arguments {
            get {
                return this.argumentsField;
            }
            set {
                this.argumentsField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string name {
            get {
                return this.nameField;
            }
            set {
                this.nameField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.br-automation.com/iat2014/widget")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.br-automation.com/iat2014/widget", IsNullable=false)]
    public partial class Permissions {
        
        private Permission[] permissionField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Permission")]
        public Permission[] Permission {
            get {
                return this.permissionField;
            }
            set {
                this.permissionField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.br-automation.com/iat2014/widget")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.br-automation.com/iat2014/widget", IsNullable=false)]
    public partial class Permission {
        
        private string descriptionField;
        
        private string nameField;
        
        /// <remarks/>
        public string Description {
            get {
                return this.descriptionField;
            }
            set {
                this.descriptionField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string name {
            get {
                return this.nameField;
            }
            set {
                this.nameField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.br-automation.com/iat2014/widget")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.br-automation.com/iat2014/widget", IsNullable=false)]
    public partial class StyleProperties {
        
        private StyleProperty[] stylePropertyField;
        
        private string defaultStyleField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("StyleProperty")]
        public StyleProperty[] StyleProperty {
            get {
                return this.stylePropertyField;
            }
            set {
                this.stylePropertyField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string defaultStyle {
            get {
                return this.defaultStyleField;
            }
            set {
                this.defaultStyleField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.br-automation.com/iat2014/widget")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.br-automation.com/iat2014/widget", IsNullable=false)]
    public partial class StyleProperty {
        
        private StyleElement[] styleElementField;
        
        private string[] descriptionField;
        
        private string nameField;
        
        private string typeField;
        
        private string categoryField;
        
        private string defaultField;
        
        private bool not_styleableField;
        
        private bool not_styleableFieldSpecified;
        
        private string ownerField;
        
        private bool hideField;
        
        private bool hideFieldSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("StyleElement")]
        public StyleElement[] StyleElement {
            get {
                return this.styleElementField;
            }
            set {
                this.styleElementField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Description")]
        public string[] Description {
            get {
                return this.descriptionField;
            }
            set {
                this.descriptionField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string name {
            get {
                return this.nameField;
            }
            set {
                this.nameField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string type {
            get {
                return this.typeField;
            }
            set {
                this.typeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string category {
            get {
                return this.categoryField;
            }
            set {
                this.categoryField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string @default {
            get {
                return this.defaultField;
            }
            set {
                this.defaultField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool not_styleable {
            get {
                return this.not_styleableField;
            }
            set {
                this.not_styleableField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool not_styleableSpecified {
            get {
                return this.not_styleableFieldSpecified;
            }
            set {
                this.not_styleableFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string owner {
            get {
                return this.ownerField;
            }
            set {
                this.ownerField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool hide {
            get {
                return this.hideField;
            }
            set {
                this.hideField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool hideSpecified {
            get {
                return this.hideFieldSpecified;
            }
            set {
                this.hideFieldSpecified = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.br-automation.com/iat2014/widget")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.br-automation.com/iat2014/widget", IsNullable=false)]
    public partial class ASEngineeringInfo {
        
        private IsProjectable isProjectableField;
        
        /// <remarks/>
        public IsProjectable IsProjectable {
            get {
                return this.isProjectableField;
            }
            set {
                this.isProjectableField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.br-automation.com/iat2014/widget")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.br-automation.com/iat2014/widget", IsNullable=false)]
    public partial class Dependencies {
        
        private string[] filesField;
        
        private string[] widgetsField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("File", IsNullable=false)]
        public string[] Files {
            get {
                return this.filesField;
            }
            set {
                this.filesField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Widget", IsNullable=false)]
        public string[] Widgets {
            get {
                return this.widgetsField;
            }
            set {
                this.widgetsField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.br-automation.com/iat2014/widget")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.br-automation.com/iat2014/widget", IsNullable=false)]
    public partial class Categories {
        
        private Category[] categoryField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Category")]
        public Category[] Category {
            get {
                return this.categoryField;
            }
            set {
                this.categoryField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.br-automation.com/iat2014/widget")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.br-automation.com/iat2014/widget", IsNullable=false)]
    public partial class Widget {
        
        private ASEngineeringInfo aSEngineeringInfoField;
        
        private Dependencies dependenciesField;
        
        private Category[] categoriesField;
        
        private Description[] descriptionsField;
        
        private Method[] methodsField;
        
        private Permission[] permissionsField;
        
        private Property[] propertiesField;
        
        private StyleProperties stylePropertiesField;
        
        private Event[] eventsField;
        
        private BindingTemplate[] bindingTemplatesField;
        
        private string nameField;
        
        private string categoryField;
        
        /// <remarks/>
        public ASEngineeringInfo ASEngineeringInfo {
            get {
                return this.aSEngineeringInfoField;
            }
            set {
                this.aSEngineeringInfoField = value;
            }
        }
        
        /// <remarks/>
        public Dependencies Dependencies {
            get {
                return this.dependenciesField;
            }
            set {
                this.dependenciesField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable=false)]
        public Category[] Categories {
            get {
                return this.categoriesField;
            }
            set {
                this.categoriesField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable=false)]
        public Description[] Descriptions {
            get {
                return this.descriptionsField;
            }
            set {
                this.descriptionsField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable=false)]
        public Method[] Methods {
            get {
                return this.methodsField;
            }
            set {
                this.methodsField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable=false)]
        public Permission[] Permissions {
            get {
                return this.permissionsField;
            }
            set {
                this.permissionsField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable=false)]
        public Property[] Properties {
            get {
                return this.propertiesField;
            }
            set {
                this.propertiesField = value;
            }
        }
        
        /// <remarks/>
        public StyleProperties StyleProperties {
            get {
                return this.stylePropertiesField;
            }
            set {
                this.stylePropertiesField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable=false)]
        public Event[] Events {
            get {
                return this.eventsField;
            }
            set {
                this.eventsField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable=false)]
        public BindingTemplate[] BindingTemplates {
            get {
                return this.bindingTemplatesField;
            }
            set {
                this.bindingTemplatesField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string name {
            get {
                return this.nameField;
            }
            set {
                this.nameField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string category {
            get {
                return this.categoryField;
            }
            set {
                this.categoryField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.br-automation.com/iat2014/widget")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.br-automation.com/iat2014/widget", IsNullable=false)]
    public partial class WidgetLibrary {
        
        private Widget[] widgetField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Widget")]
        public Widget[] Widget {
            get {
                return this.widgetField;
            }
            set {
                this.widgetField = value;
            }
        }
    }
}
