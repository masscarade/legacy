﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace mapp_meta
{

    [XmlRootAttribute("Configuration")]
    public partial class MpAlarm : Configuration
    {
        private List<mapp_meta.Group> srcSnippets = new List<mapp_meta.Group>();
        private List<mapp_meta.Group> srcAlarms = new List<mapp_meta.Group>();
        private List<mapp_meta.Group> srcMappings = new List<mapp_meta.Group>();

        public MpAlarm()
            : base()
        {

        }

        public static MpAlarm load(string path)
        {
            using (FileStream destFs = new FileStream(path, FileMode.Open))
            {
                XmlSerializer destSerializer = new XmlSerializer(typeof(mapp_meta.Configuration));
                mapp_meta.Configuration mpConfig = (mapp_meta.Configuration)destSerializer.Deserialize(destFs);

                return new mapp_meta.MpAlarm(mpConfig);
            }
        }

        public void addElements(mapp_meta.Configuration inst)
        {
            /*foreach (mapp_meta.ConfigurationElement item in inst.Items)
            {
                foreach (mapp_meta.Group gr in item.Group)
                {
                    if (gr.ID == "mapp.AlarmX.Core.Configuration")
                    {
                        srcAlarms.AddRange(gr.Group1);
                    }
                    else if (gr.ID == "mapp.AlarmX.Core.Snippets")
                    {
                        srcSnippets.AddRange(gr.Group1);
                    }
                    else if (gr.ID == "mapp.AlarmX.Core")
                    {
                        //srcMappings.AddRange(gr.Group1);
                    }
                }
            }*/

            foreach (mapp_meta.ConfigurationElement item in inst.Items)
            {
                foreach (mapp_meta.Group gr in item.Group)
                {
                    if (gr.Group1 == null)
                    {
                        continue;
                    }

                    if (gr.ID == "mapp.AlarmX.Core.Configuration")
                    {
                        srcAlarms.AddRange(gr.Group1);
                    }
                    else if (gr.ID == "mapp.AlarmX.Core.Snippets")
                    {
                        srcSnippets.AddRange(gr.Group1);
                    }

                    else if (gr.ID == "mapp.AlarmX.Core")
                    {
                        srcMappings.AddRange(gr.Group1[0].Group1);
                    }
                }
            }
        }

        public void save(string path)
        {
            foreach (mapp_meta.ConfigurationElement item in this.Items)
            {
                foreach (mapp_meta.Group gr in item.Group)
                {
                    if (gr.ID == "mapp.AlarmX.Core.Configuration")
                    {
                        gr.Group1 = srcAlarms.ToArray();

                        int index = 0;
                        foreach (mapp_meta.Group alarm in gr.Group1)
                        {
                            alarm.ID = "[" + index++ + "]";
                        }
                    }
                    else if (gr.ID == "mapp.AlarmX.Core.Snippets")
                    {
                        gr.Group1 = srcSnippets.ToArray();

                        int index = 0;

                        foreach (mapp_meta.Group snippet in gr.Group1)
                        {
                            snippet.ID = "[" + index++ + "]";
                        }
                    }

                    else if (gr.ID == "mapp.AlarmX.Core")
                    {
                        gr.Group1[0].Group1 = srcMappings.ToArray();

                        int index = 0;

                        foreach (mapp_meta.Group mapping in gr.Group1[0].Group1)
                        {
                            mapping.ID = "[" + index++ + "]";
                        }
                    }                
                }
            }

            using (TextWriter writer = new StreamWriter(path))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(MpAlarm));
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add("", "");

                mapp_meta.Configuration alarmConfig = (mapp_meta.Configuration)this;

                serializer.Serialize(writer, this);
            }
        }

        public MpAlarm(mapp_meta.Configuration inst)
        {
            this.Items = inst.Items;
            addElements(inst);

        }
    }
}