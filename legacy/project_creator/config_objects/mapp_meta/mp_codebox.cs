﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace mapp_meta
{

    [XmlRootAttribute("Configuration")]
    public partial class MpCodebox : Configuration
    {
        private List<mapp_meta.Group> srcConditions = new List<mapp_meta.Group>();
        private List<mapp_meta.Group> srcActions = new List<mapp_meta.Group>();

        public MpCodebox()
            : base()
        {

        }

        public static MpCodebox load(string path)
        {
            using (FileStream destFs = new FileStream(path, FileMode.Open))
            {
                XmlSerializer destSerializer = new XmlSerializer(typeof(mapp_meta.Configuration));
                mapp_meta.Configuration mpConfig = (mapp_meta.Configuration)destSerializer.Deserialize(destFs);

                return new mapp_meta.MpCodebox(mpConfig);
            }
        }

        public void addElements(mapp_meta.Configuration inst)
        {
            foreach (mapp_meta.ConfigurationElement item in inst.Items)
            {
                foreach (mapp_meta.Group gr in item.Group)
                {
                    if (gr.ID == "macro")
                    {
                        foreach (mapp_meta.Group subGr in gr.Group1)
                        {
                            if (subGr.ID == "act")
                            {
                                srcActions.AddRange(subGr.Group1);
                            }
                            else if (subGr.ID == "cond")
                            {
                                srcConditions.AddRange(subGr.Group1);
                            }
                        }
                    }
                }
            }
        }

        public void save(string path)
        {
            foreach (mapp_meta.ConfigurationElement item in this.Items)
            {
                foreach (mapp_meta.Group gr in item.Group)
                {
                    if (gr.ID == "macro")
                    {
                        foreach (mapp_meta.Group subGr in gr.Group1)
                        {
                            if (subGr.ID == "act")
                            {
                                subGr.Group1 = srcActions.ToArray();

                                int index = 0;
                                foreach (mapp_meta.Group action in subGr.Group1)
                                {
                                    action.ID = index++.ToString();
                                }
                            }
                            else if (subGr.ID == "cond")
                            {
                                subGr.Group1 = srcConditions.ToArray();

                                int index = 0;
                                foreach (mapp_meta.Group condition in subGr.Group1)
                                {
                                    condition.ID = index++.ToString();
                                }
                            }
                        }
                    }
                }
            }

            using (TextWriter writer = new StreamWriter(path))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(MpCodebox));
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add("", "");

                mapp_meta.Configuration alarmConfig = (mapp_meta.Configuration)this;

                serializer.Serialize(writer, this);
            }
        }

        public MpCodebox(mapp_meta.Configuration inst)
        {
            this.Items = inst.Items;
            addElements(inst);

        }
    }
}