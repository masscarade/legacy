﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace mapp_meta
{

    [XmlRootAttribute("Configuration")]
    public partial class MpGroup : Configuration
    {
        private Dictionary<string, List<mapp_meta.Group>> groups = new Dictionary<string, List<Group>>();

        public MpGroup()
            : base()
        {

        }

        public static MpGroup load(string path)
        {
            using (FileStream destFs = new FileStream(path, FileMode.Open))
            {
                XmlSerializer destSerializer = new XmlSerializer(typeof(mapp_meta.Configuration));
                mapp_meta.Configuration mpConfig = (mapp_meta.Configuration)destSerializer.Deserialize(destFs);

                return new mapp_meta.MpGroup(mpConfig);
            }
        }

        public void addElements(mapp_meta.Configuration inst)
        {
            foreach (mapp_meta.ConfigurationElement item in inst.Items)
            {
                if (item.Type == "mpcomgroup")
                {
                    if (groups.ContainsKey(item.ID) == false)
                    {
                        List<mapp_meta.Group> group = new List<Group>();
                        group.AddRange(item.Group);

                        groups.Add(item.ID, group);
                    }
                    else
                    {
                        List<Property> linking = new List<Property>();
                        linking.AddRange(item.Group[0].Group1[0].Property);
                        linking.AddRange(groups[item.ID][0].Group1[0].Property);

                        groups[item.ID][0].Group1[0].Property = linking.ToArray();

                    }
                }
            }
        }

        public void save(string path)
        {
            foreach (mapp_meta.ConfigurationElement item in this.Items)
            {
                int index = 0;
                foreach (mapp_meta.Property property in item.Group[0].Group1[0].Property)
                {
                    property.ID = index++.ToString();

                }
            }

            using (TextWriter writer = new StreamWriter(path))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(MpGroup));
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add("", "");

                mapp_meta.Configuration alarmConfig = (mapp_meta.Configuration)this;

                serializer.Serialize(writer, this);
            }
        }

        public MpGroup(mapp_meta.Configuration inst)
        {
            this.Items = inst.Items;
            addElements(inst);

        }
    }
}