﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Xml.Linq;
using codegenLog;

namespace mapp_meta
{

    [XmlRootAttribute("Configuration")]
    public partial class MpSequencer : Configuration
    {
        [XmlIgnore]
        public List<mapp_meta.Group> srcAxes { get; private set; } = new List<mapp_meta.Group>();
        [XmlIgnore]
        public List<mapp_meta.Group> srcDynamicInterlocks { get; private set; } = new List<mapp_meta.Group>();
        [XmlIgnore]
        public List<mapp_meta.Group> srcCommands { get; private set; } = new List<mapp_meta.Group>();
        [XmlIgnore]
        public string srcPath { get; private set; }

        public MpSequencer()
            : base()
        {

        }

        public static MpSequencer load(string path)
        {
            using (FileStream destFs = new FileStream(path, FileMode.Open))
            {
                XmlSerializer destSerializer = new XmlSerializer(typeof(mapp_meta.Configuration));
                mapp_meta.Configuration mpConfig = (mapp_meta.Configuration)destSerializer.Deserialize(destFs);

                return new mapp_meta.MpSequencer(mpConfig) { srcPath = path };
            }
        }

        public void addElements(mapp_meta.Configuration inst)
        {
            foreach (mapp_meta.ConfigurationElement item in inst.Items)
            {
                foreach (mapp_meta.Group gr in item.Group)
                {                   
                    foreach (mapp_meta.Group subGr in gr.Group1)
                    {
                        if (subGr.ID == "Commands")
                        {
                            srcCommands.AddRange(subGr.Group1);
                        }
                        else if (subGr.ID == "Axes")
                        {
                            srcAxes.AddRange(subGr.Group1);
                        }
                        else if (subGr.ID == "DynamicInterlocks")
                        {
                            srcDynamicInterlocks.AddRange(subGr.Group1);
                        }
                    }

                    if (srcDynamicInterlocks.Count == 0)
                    {

                        List<Group> group1 = new List<Group>(gr.Group1);
                        group1.Add(new Group()
                        {
                            ID = "DynamicInterlocks",
                        });

                        gr.Group1 = group1.ToArray();
                    }
                }
            }
        }

        public void generateStaticInterlocks(project_creator.Settings.DestConfig.StaticInterlockMatrix matrix)
        {
            if (matrix == null)
            {
                return;
            }
            matrix.generate(srcAxes, srcCommands);
        }

        public void save(string destPath = null)
        {
            string path = destPath != null ? destPath : srcPath;

            foreach (mapp_meta.ConfigurationElement item in this.Items)
            {
                bool axesAdded = false;
                bool commandsAdded = false;
                bool dynamicInterlocksAdded = false;

                foreach (mapp_meta.Group gr in item.Group)
                {
                    foreach (mapp_meta.Group subGr in gr.Group1)
                    {
                        if (subGr.ID == "Commands")
                        {
                            subGr.Group1 = srcCommands.ToArray();

                            int index = 0;
                            foreach (mapp_meta.Group command in subGr.Group1)
                            {
                                command.ID = "Command[" + index++ + "]";
                            }

                            commandsAdded = true;
                        }
                        if (subGr.ID == "Axes")
                        {
                            subGr.Group1 = srcAxes.ToArray();

                            int index = 0;
                            foreach (mapp_meta.Group axes in subGr.Group1)
                            {
                                axes.ID = "Axis[" + index++ + "]";
                            }

                            axesAdded = true;
                        }
                        if (subGr.ID == "DynamicInterlocks")
                        {
                            subGr.Group1 = srcDynamicInterlocks.ToArray();

                            int index = 0;
                            foreach (mapp_meta.Group axes in subGr.Group1)
                            {
                                axes.ID = "DynamicInterlock[" + index++ + "]";
                            }

                            dynamicInterlocksAdded = true;
                        }
                    }

                    if (axesAdded == false)
                    {
                        List<mapp_meta.Group> groups = new List<Group>(gr.Group1);
                        mapp_meta.Group axes = new Group()
                        {
                            ID = "Axes",
                            Group1 = srcAxes.ToArray(),
                        };

                        axes.Group1 = srcAxes.ToArray();

                        int index = 0;
                        foreach (mapp_meta.Group axis in axes.Group1)
                        {
                            axis.ID = "Axis[" + index++ + "]";
                        }

                        groups.Add(axes);

                        gr.Group1 = groups.ToArray();

                        axesAdded = true;
                    }
                }
            }

            using (TextWriter writer = new StreamWriter(path))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(MpSequencer));
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add("", "");

                mapp_meta.Configuration alarmConfig = (mapp_meta.Configuration)this;

                serializer.Serialize(writer, this);
            }
        }

        public MpSequencer(mapp_meta.Configuration inst)
        {
            this.Items = inst.Items;
            addElements(inst);
        }

        internal static void GenerateDynamicInterlocks(string path, project_creator.Settings.DestConfig.DynamicInterlockMatrix.AllAxes matrix)
        {
            Logging.log("generating dynamic interlocks", LEVEL.INFO);         

            XDocument xml;

            Logging.log(string.Format("read sequencer from: {0}", path), LEVEL.INFO);

            using (TextReader reader = File.OpenText(path))
            {
                xml = XDocument.Load(reader);

                XElement dynInterlocks = null;
                XElement parent = xml.Descendants().Where(x => (string)x.Attribute("ID") == "Sequence").First();

                /*merge all dynamic interlocs into one element*/
                Logging.log(string.Format("parse for dynamic interlock groups"), LEVEL.INFO);
                var temp = xml.Descendants().Where(x => (string)x.Attribute("ID") == "DynamicInterlocks");

                //dyn interlocks found
                if (temp.Count() != 0)
                {                    
                    XElement first = temp.First();
                    do
                    {
                        XElement inst = temp.Last();
                        if (inst == first)
                        {
                            continue;
                        }
                        Logging.log(string.Format("merge redundant dyn interlocks", path), LEVEL.INFO);
                        first.Add(inst.Descendants());
                        inst.Remove();
                    }while (temp.Count() > 1) ;

                    dynInterlocks = first;
                }

                if (dynInterlocks == null)
                {
                    Logging.log(string.Format("no dynamic interlock groups found"), LEVEL.INFO);

                    dynInterlocks = new XElement("Group");
                    dynInterlocks.Add(new XAttribute("ID", "DynamicInterlocks"));
                    parent.Add(dynInterlocks);

                    Logging.log(string.Format("dynamic interlock group created"), LEVEL.INFO);
                }

                /*list all axes as string*/
                var axes = xml.Descendants()
                    .Where(x => (string)x.Attribute("ID") == "Axes")
                    .Descendants("Property")
                    .Where(y=>(string)y.Attribute("ID")=="Name")
                    .Select(z => (string)z.Attribute("Value"));

                //remove blacklist from axes list to ignore them in the intelock generation
                //               axes = axes.Except(matrix.blacklist);
                
                foreach (string axis in axes)
                {
                    if (matrix.isBlacklisted(axis) == true)
                    {
                        Logging.log(string.Format("axis {0} is blacklisted", axis), LEVEL.INFO);
                        continue;
                    }

                    /*get interlock element for this axis; create new node if not existing*/
                    var node = dynInterlocks.Descendants("Property")
                        .Where(x => (string)x.Attribute("ID") == "Axis" && (string)x.Attribute("Value") == axis);

                    XElement currentInterlock = null;

                    /*create new "DynamicInterlock[X]" node if no match was found*/
                    if (node.Count() == 0)
                    {
                        Logging.log(string.Format("dynamic interlocks for axis {0} not existing", axis), LEVEL.INFO);
                        XElement interlock = new XElement("Group");
                        interlock.Add(new XAttribute("ID", string.Format("DynamicInterlock[{0}]", dynInterlocks.Elements().Count())));

                        XElement id = new XElement("Property");
                        id.Add(new XAttribute("ID", "Axis"));
                        id.Add(new XAttribute("Value", axis));

                        interlock.Add(id);

                        XElement list = new XElement("Group");
                        list.Add(new XAttribute("ID", "Interlocks"));

                        interlock.Add(list);

                        dynInterlocks.Add(interlock);
                        currentInterlock = interlock;
                    }
                    else if (node.Count() == 1)
                    {
                        currentInterlock = node.First();
                    }
                    else
                    {
                        Logging.log(string.Format("dynamic interlocks for axis {0} found multiple times", axis), LEVEL.ERROR);
                    }

                    foreach (string element in axes)
                    {
                        //do not generate interlock for current axis -> would be redundant
                        if (element == axis)
                        {
                            continue;
                        }

                        if ((matrix.isBlacklisted(axis, element) == true) || (matrix.isBlacklisted(element))) 
                        {
                            Logging.log(string.Format("axis {0} is blacklisted with {1}", axis, element), LEVEL.INFO);
                            continue;
                        }

                        //get element containing the actuel axes list
                        XElement axesList = currentInterlock.Elements().Where(x => (string)x.Attribute("ID") == "Interlocks").First();

                        //check if the current axis is already included and skip if true
                        if (axesList.Elements().Where(x => (string)x.Attribute("ID") == "Interlocks").Count() > 0)
                        {
                            continue;
                        }

                        Logging.log(string.Format("add dynamic interlock for {0} to {1}", element, axis), LEVEL.INFO);

                        XElement interlock = new XElement("Property");
                        interlock.Add(new XAttribute("ID", string.Format("Axes[{0}]", axesList.Elements().Count())));
                        interlock.Add(new XAttribute("Value", element));

                        axesList.Add(interlock);
                    }
                }
                          
                /*<Group ID="DynamicInterlocks">
                   <Group ID="DynamicInterlock[0]">
                     <Property ID="Axis" Value="Clamp" />
                     <Group ID="Interlocks">
                       <Property ID="Axes[0]" Value="Core1" />
                     </Group>
                   </Group>
                   <Group ID="DynamicInterlock[1]">
                     <Property ID="Axis" Value="Core1" />
                     <Group ID="Interlocks">
                       <Property ID="Axes[0]" Value="Core2" />
                     </Group>
                   </Group>
                 </Group>*/

            }

            Logging.log(string.Format("save dynamic interlocks to {0}", path), LEVEL.INFO);
            xml.Save(path);
        }
    }
}