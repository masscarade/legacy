﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace sw_config
{

    public partial class SwConfiguration
    {
        public void save(string path)
        {
            using (TextWriter writer = new StreamWriter(path))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(SwConfiguration));
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add("", "");

                serializer.Serialize(writer, this);
            }
        }
        public static SwConfiguration load(string path)
        {
            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(sw_config.SwConfiguration));
                return (sw_config.SwConfiguration)serializer.Deserialize(fs);
            }
        }
    }
}

