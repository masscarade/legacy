﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace project_creator
{
    public class Program
    {

        static void Main(string[] args)
        {
           /* List<ProjectFeature> features = new List<ProjectFeature>();

            features.Add(FeatureFactory<CorePuller>.produceFeature());
            features.Add(FeatureFactory<TopSeparator>.produceFeature());
            features.Add(FeatureFactory<CorePuller>.produceFeature());
            features.Add(FeatureFactory<CorePuller>.produceFeature());
            features.Add(FeatureFactory<CorePuller>.produceFeature());
            features.Add(FeatureFactory<TopSeparator>.produceFeature());
            features.Add(FeatureFactory<TopSeparator>.produceFeature());
            features.Add(FeatureFactory<CorePuller>.produceFeature());*/

            /*ProjectFeature feature = ProjectFeature.FeatureFactory<TopSeparator>.produceFeature();

            XmlSerializer serializer = new XmlSerializer(typeof(ProjectFeature));
            TextWriter writer = new StreamWriter("Feature.xml");

            serializer.Serialize(writer, feature);
            writer.Close();*/


            Project project = new Project();

            project.import(new StreamReader("Project.txt"),"Products");

        }

        public class CorePuller : Axis
        {
        
            public CorePuller()
                : base()
            {
                FeatureFactory<CorePuller>.incrementObjCnt();
                componentName += FeatureFactory<CorePuller>.objCnt.ToString();
            }
        }

        public class TopSeparator : Axis
        {     
            public TopSeparator()
                : base()
            {
                FeatureFactory<TopSeparator>.incrementObjCnt();
                componentName += FeatureFactory<TopSeparator>.objCnt.ToString();     
            }
        }

        public class SPC : ProjectFeature
        {
            public SPC()
                : base()
            {          
                configurationObjects.Add(new ConfigurationObject.Task());
            }
        }

      
        public abstract class Axis : ProjectFeature
        {
            public Axis()
                : base()
            {           
                    configurationObjects.Add(new ConfigurationObject.OPC_UA());
                    configurationObjects.Add(new ConfigurationObject.Alarms());
                    configurationObjects.Add(new ConfigurationObject.Sequencer());
                    configurationObjects.Add(new ConfigurationObject.Task());

            }
        }

        [XmlInclude(typeof(CorePuller))]
        [XmlInclude(typeof(TopSeparator))]
        [XmlInclude(typeof(SPC))]
        public abstract class ProjectFeature
        {
            //i.e. clamp, core1,..needs to contain numerator
            protected string componentName;
            protected string ID;


            //list of config objects for the as project, such as OPC UA map, tasks,...
            public List<ConfigurationObject> configurationObjects { protected set; get; }

            protected ProjectFeature()
            {
                configurationObjects = new List<ConfigurationObject>();
                componentName = this.GetType().Name;
            }

            [XmlInclude(typeof(OPC_UA))]
            [XmlInclude(typeof(Task))]
            [XmlInclude(typeof(Alarms))]
            [XmlInclude(typeof(Sequencer))]
            public abstract class ConfigurationObject
            {
                [XmlRootAttribute("OPC_UA")]
                public class OPC_UA : ConfigurationObject
                {
                   public  OPC_UA()
                        : base()
                    {
                       
                    }
                }

                [XmlRootAttribute("Task")]
                public class Task : ConfigurationObject
                {
                    public string source;

                    public Task()
                        : base()
                    {
                       
                    }
                }

                [XmlRootAttribute("Alarms")]
                public class Alarms : ConfigurationObject
                {
                    public Alarms()
                        : base()
                    {
                       
                    }
                }


                [XmlRootAttribute("Sequencer")]
                public class Sequencer : ConfigurationObject
                {
                    public Sequencer()
                        : base()
                    {
                       
                    }
                }
            }

            public static class FeatureFactory<TFeature> where TFeature : ProjectFeature, new()
            {
                public static int objCnt { private set; get; }

                static FeatureFactory()
                {
                    objCnt = 0;
                }

                static public void incrementObjCnt()
                {
                    objCnt++;
                }

                static public TFeature produceFeature()
                {
                    objCnt++;
                    return new TFeature();
                }
            }
        }                      

        public class Project
        {
            //list of features in the project i.e. 2 * Corepuller, 1 * Clamp,... 
            private List<ProjectFeature> features;

            public Project()
            {
                this.features = new List<ProjectFeature>();

            }

            public void import(StreamReader src, string productSrc)
            {

                string product;

                while ((product = src.ReadLine()) != null)
                {

                    XmlSerializer serializer = new XmlSerializer(typeof(ProjectFeature));

                    string path = productSrc + @"\" + product + @"\Feature.xml";
                    FileStream fs = new FileStream(path, FileMode.Open);

                    ProjectFeature newFeature = (ProjectFeature)serializer.Deserialize(fs);
                    fs.Close();             

                    features.Add(newFeature);

                    System.Console.Out.WriteLine(newFeature.GetType());
                }
            }

            public void export(string path)
            {
                //for testing
                /*XmlSerializer serializer = new XmlSerializer(typeof(Project));
                TextWriter writer = new StreamWriter("project.xml");

                serializer.Serialize(writer, this);
                writer.Close();*/

            }
            
           
        } 
    }
}
